#! /usr/bin/env python

usage="""
Check that all the sequences in an alignment match the orientation of the first string

Requires the "Jellyfish" python module (not my name!) or blast+
  Install jellyfish with 'sudo easy_install jellyfish'
  
  Optional number after filename uses that numbered sequence (starting with zero) 
  and fuzzy match (jellyfish) instead of blasting against a named reference file
  containing a single sequence.
  
Usage:
  checkflip.py UnflippedAlignment.fta [#] > FlippedSeqs.fta
"""

import sys
import os
# needed for maketrans -- not sure if this is obsolete
import string
import mybio as mb
import jellyfish # fuzzy matching library via easy_install jellyfish

#mb.revcomp is in mybio...

def fuzzymatch(refsequence, reversedreference, querysequence):
	# better with our without gaps??
	# q = querysequence.upper()
	if RemoveGaps:
		q = querysequence.replace("-","").upper()
	else:
		q = querysequence.upper()
	# options include jaro_distance, damerau_levenshtein_distance, jaro_winkler
#	forwardscore2 = jellyfish.jaro_winkler(q,refsequence)
# 	reversescore2 = jellyfish.jaro_winkler(q,reversedreference)
	
	forwardscore = jellyfish.levenshtein_distance(q,refsequence)
 	reversescore = jellyfish.levenshtein_distance(q,reversedreference)
 	
 	# lower score (number of changes req'd) is better, but allow +- 1 without flipping?
	if (reversescore - forwardscore) >= -1: # lower score is better
#	 	sys.stderr.write("%d\t%d\t%.3f\t%.3f\n" % (forwardscore,reversescore,forwardscore2,reversescore2) )
		if (reversescore - forwardscore) <= 2:
			sys.stderr.write(greencolor)
		sys.stderr.write("%d\t%d\n" % (forwardscore,reversescore) )
		sys.stderr.write(coloroff)
		return querysequence, False
	else:
		sys.stderr.write(redcolor)
		sys.stderr.write(boldon)		
#	 	sys.stderr.write("%d\t%d\t%.3f\t%.3f\n" % (forwardscore,reversescore,forwardscore2,reversescore2) )
		sys.stderr.write("%d\t%d\n" % (forwardscore,reversescore) )
		sys.stderr.write(coloroff)
		return mb.revcomp(querysequence), True


def blastmatch(querysequence):

	blastcommand = 'echo "%s" | blastn -subject %s -task blastn-short -num_alignments 1 -outfmt "6 evalue" -strand %s'

	# When using tblastx or blastp
	blastcommand = 'echo "%s" | tblastx -subject %s  -num_alignments 1 -outfmt "6 evalue" -strand %s'
	# 
	# blastcommand = 'echo "%s" | blastp -subject %s  -num_alignments 1 -outfmt "6 evalue %s" '
	
	try:
		forwardresult =  os.popen(blastcommand % (querysequence,reffile,"plus" )).read() 
		forwardscore = float (forwardresult)
	except ValueError:
		forwardscore = 90
	try:
		reversescore = float( os.popen(blastcommand % (querysequence,reffile,"minus")).read() )
	except:
		reversescore = 900
		
	try:
		relativescore = forwardscore/reversescore
	except ZeroDivisionError:
		relativescore = 100
		

	if (relativescore < 2): # lower score is better
#	 	sys.stderr.write("%d\t%d\t%.3f\t%.3f\n" % (forwardscore,reversescore,forwardscore2,reversescore2) )
		sys.stderr.write("%.1e\t%.1e\n" % (forwardscore,reversescore) )
		sys.stderr.write(coloroff)
		return querysequence, False
	else:
		sys.stderr.write(redcolor)
		sys.stderr.write(boldon)		
#	 	sys.stderr.write("%d\t%d\t%.3f\t%.3f\n" % (forwardscore,reversescore,forwardscore2,reversescore2) )
		sys.stderr.write("%.1e\t%.1e\n" % (forwardscore,reversescore) )
		sys.stderr.write(coloroff)
		return mb.revcomp(querysequence), True
	
	
	
##########################
# START MAIN PROGRAM
skipcount=0

minsize=1
blockwidth=71
noread=False

PrintOut = True 		# print the flipped sequences.
RemoveGaps = True

sequences={}
ordernames=[]

redcolor   = '\x1b[31m'
greencolor = '\x1b[32m'
boldon     = '\x1b[1m'
boldoff    = '\x1b[22m'
coloroff   = '\x1b[0m'


reffile = "Tca.fna"
reffile = "aequorin.fna"
whichseq = 0
UseBlast = True

if len(sys.argv)<2:
	sys.stderr.write(usage)
	
	
# optional: do majority rule to find them?
# optional, use pairwise blast instead...

else:

	if len(sys.argv)>2: # second argument to tell which sequence to use as ref... 
		whichseq = int(sys.argv[2])
		UseBlast = False
	else:
		whichseq = 1
		UseBlast = False
		
	inputfilename = sys.argv[1]
		
	ordernames,sequences = mb.readfasta_list(inputfilename) # don't check dupes, and in order

	
	
	# if you want to do a comparison w/o removing gaps, change it here and in the fuzzymatch function
	refseq = ""
	origwhichseq = whichseq
	while ((len(refseq) < 2) and (whichseq < len(ordernames))): 
		refname = ordernames[whichseq]
		if RemoveGaps:
				refseq = sequences[whichseq].replace("-","").upper()
		else:
			refseq = sequences[whichseq].upper()
		if (len(refseq)<2):
			whichseq += 1
			
	assert whichseq < len(ordernames), "No non-zero lengths after the specified reference..."
		
	refrc = mb.revcomp(refseq)
	
	# assert (refseq.count("A")/float(len(refseq)) > .1), "\nIs this a DNA file or Protein? The program only works on DNA."
	
	outseq = []
	fliplist = []
	flipcount = 0
	totalcount = 0
	for name,sequence in zip(ordernames,sequences):
		newseq = sequence.upper()
		
		# do fuzzy match vs refseq here
		sys.stderr.write("%3d:\t%s\t" %(totalcount,name))
		
		# fuzzymatch function prints the relative scores to stderr
		if UseBlast:
			correctsequence,flipped = blastmatch(newseq)
		else:
			correctsequence,flipped = fuzzymatch(refseq,refrc,newseq)
		
		totalcount += 1
		
		if flipped:
			fliplist.append(name)
			flipcount += 1
		outseq.append(correctsequence)
	
	
	if PrintOut:
		for ind,name in enumerate(ordernames):
			print ">"+name
			print mb.stringblock(outseq[ind],80).rstrip()
	sys.stderr.write(redcolor)
	sys.stderr.write(".....................\n")
	sys.stderr.write(coloroff)
	if UseBlast:
		sys.stderr.write(" Using Blast against %s\n" %(reffile))
	else:
		if origwhichseq != whichseq:
			sys.stderr.write("NOTE: #%d indicated, but u" %(origwhichseq))
		else:
			sys.stderr.write("U")	
		sys.stderr.write("sed #%d <<%s>> as reference" %(whichseq,refname))
		sys.stderr.write(" WITH"+["OUT",""][RemoveGaps] + " gaps removed\n")
	sys.stderr.write("Flipped %d sequences\n" % flipcount)
	sys.stderr.write("\n".join(fliplist))
	sys.stderr.write(redcolor)
	sys.stderr.write("\n.....................\n")
	sys.stderr.write(coloroff)


"""
echo "CGTCTCCAAAGCTTCGGATGACATTTGCGCCAAACTTGGAGCAACACCAGAACAGACCAAACGTCACCAGGATGCTGTCGAAGCTTTCTTCAAAAAGATTGGTATG" \
| blastn -subject aequorin.fna -task blastn-short -outfmt 5 -num_alignments 1
 	evalue     
   	  bitscore 
   	     score 
   	    length 
		qframe

echo "CGTCTCCAAAGCTTCGGATGACATTTGCGCCAAACTTGGAGCAACACCAGAACAGACCAAACGTCACCAGGATGCTGTCGAAGCTTTCTTCAAAAAGATTGGTATG" \
| blastn -subject aequorin.fna -task blastn-short -num_alignments 1 -outfmt "6 evalue" -strand minus
"""
