#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
#####################################################

  Make a table of video attributes (in text or HTML)

Version 2.3 - got the order of ffmpeg arguments correct for 50x speed up. 
Version 2.2 - Improved file support and report file size
Version 2.1 - Tags videos < 5 sec as "short" and "Gray" color
Version 2   - uses ffprobe instead of exiftool for large file support

Requires ffmpeg (and ffprobe) and imagemagick (convert) for thumbnails installation.

  -t  : Output text table instead of html
  -i  : Extract video thumbnails (takes a while for large files)
  -e  : Use exiftool instead of ffprobe for video info
  -b  : Show debugging output

Usage:
   ## to create a HTML table with thumbnails:
   videotable.py -i /Volumes/MyVideos/SHOGUN*.MOV > seagateApreview.html
   
   ## to output a text table:
   videotable.py -t ~/MyVideos/*.MOV > videotable.txt
------------------------------------------------------------------------------

TODO: Possible to multithread the frame generation??
If parallel, then temp file names have to be unique for crop command
Need to use something other than sips (convert) for cropping?


#####################################################
"""

import os, sys
import subprocess
import argparse
import time


def writexattrs(F,TagList):
	""" writexattrs(F,TagList):
	writes the list of tags to three xattr field on a file-by file basis:
	"kMDItemFinderComment","_kMDItemUserTags","kMDItemOMUserTags
"""
	from sys import platform as _platform
	if _platform == "darwin":
		import xattr
		plistFront = '<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd"><plist version="1.0"><array>'
		plistEnd = '</array></plist>'
		plistTagString = ''
		for Tag in TagList:
			plistTagString = plistTagString + '<string>{}</string>'.format(Tag.replace("'","-"))
		TagText = plistFront + plistTagString + plistEnd

		# CommentText = "Trying again"
		# Comment = "bplist00_" + CommentText
		# xattr.setxattr(F,OptionalTag+"kMDItemFinderComment",Comment.encode('utf8'))

		OptionalTag = "com.apple.metadata:"
		XattrList = ["_kMDItemUserTags"]
		# Optional extended list of tags
		# XattrList = ["kOMUserTags","kMDItemOMUserTags","_kMDItemUserTags","kMDItemkeywords"]
		for Field in XattrList:
			# using module instead of subprocess....
			xattr.setxattr (F,OptionalTag+Field,TagText.encode('utf8'))


def printhead(Fields):
	Head = """<!DOCTYPE html>
</css>
<html lang="en">
<style type="text/css">
	body { font-family: Helvetica, Arial, sans-serif; }
	table {	border-collapse: collapse; margin: 10px; background-color: #EEE;	}
	td { border-bottom-width: 1px; border-style: solid; padding: 15px; padding-bottom: 2px; padding-top: 8px; border-color: #FFF; }
	tr.first{ font-weight: bold; }
	.red {background-color:#FAA;}
	.green {background-color:#AFA;}
	.blue {background-color:#AAF;}
	.orange {background-color:#FC6;}
	.lavender {background-color:#FAD;}
	.yellow {background-color:#FFA;}
	.gray {background-color:#888;}
</style>

<head> <meta charset="utf-8" /> <title>Video Table</title> </head>
<body>
<table>
"""
	print Head
	Headline = '<tr class=first><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>'
	print Headline % (tuple(Fields+["FileSize","Image","Name"]))
	
def printtail():
	Tail = """
</table>
</body>
</html>	"""
	print Tail
	
def frametotime(seconds):
	H = seconds//3600
	M = (seconds%3600)//60
	S = seconds % 60
	return("{:02d}:{:02d}:{:02d}").format(H,M,S)
	
def cropthumbsips(imagename,tinyimagename,height,width):
	"""sips    -c pixelsH pixelsW crop to specified size --out
	-crop to 75% of width and 85% of height
	the resample to 300 px
	--resampleWidth 300
	"""
	newh = int(0.85 * height)
	neww = int(0.75 * width)
	cropcommand = 'sips -c {} {} "{}" --out /tmp/temp.png && sips /tmp/temp.png --resampleWidth 300 --out "{}" && rm -f /tmp/temp.png'.format(newh,neww,imagename,tinyimagename)
	CropOut = subprocess.Popen(cropcommand, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True) 
	Result = CropOut.stdout.readlines()
	
	
	
def cropthumb(imagename,tinyimagename,height,width):
	"""Convert instead of SIPs
	"""
	newh = int(0.85 * height)
	neww = int(0.75 * width)
	'''convert  -gravity Center -extent 800x600 image.jpg -resize 300x300 image-crop-g.png'''
	cropcommand = 'convert -gravity Center -extent {}x{} -resize 300x300 "{}" "{}"'.format(neww,newh,imagename,tinyimagename)
	CropOut = subprocess.Popen(cropcommand, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True) 
	Result = CropOut.stdout.readlines()
	
	
def generatethumbnail(filename,durval,hei,wid):
	# is it an faster if this is one second into the video?
	frametime = int(durval/2)
	# frametime = 2
	timename = frametotime(frametime)
	
	shortname = "t_"+ filename.split("/")[-1]
	path = 'vidthumblarge'
	tinypath = 'vidthumb'
	imagename = path + "/" + shortname + ".jpg"
	
	if not os.path.exists(path):
		os.mkdir(path, 0755 )
		
	if not os.path.exists(tinypath):
		os.mkdir(tinypath, 0755 )
	
	if not os.path.exists(imagename):
		print >> sys.stderr,filename
		# This version takes the frame halfway through the movie, but it takes forever to get there
		# important! -ss has to come before -i 25 minutes vs seconds...!
		
		framecommand = 'ffmpeg -loglevel panic -nostats  -ss {} -i "{}" -f image2 -vframes 1 "{}" '.format(timename,filename,imagename)
		
		# ******* THIS VERSION TAKES FIRST FRAME
		# this version takes the frame at one second
		# framecommand = 'ffmpeg -loglevel panic -nostats -ss 00:05:00 -i "{}" -f image2 -vframes 1 "{}" '.format(filename,imagename)
		FrameOut = subprocess.Popen(framecommand, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True) 
		Result = FrameOut.stdout.readlines()
		
	tinyimagename = tinypath + "/x" + shortname + ".png"
	if not os.path.exists(tinyimagename):
		cropthumb(imagename,tinyimagename,hei,wid)
	return imagename,tinyimagename
	
def printrow(MyD,filename,MakeFrame=True):
	# 30000/1001	1920	1080
	# 24000/1001	3840	2160
	lencolors = ['gray','red','yellow','green']
	sizecolors = ['gray','green','yellow','red']
	framed = {'30000/1001':'green','24000/1001':'orange','60000/1001':'red'}
	sized  = {'1920':'lavender','3840':'blue'}
	# ADD size
	# 'duration', 'r_frame_rate', 'width', 'height', 'tag:creation_time','codec_name'

	
	RowText = '<tr><td class={}>{:.1f}</td><td class={}>{}</td><td class={}>{}</td><td>{}</td><td>{}</td><td>{}</td><td class={}>{}</td><td>{}</td><td>{}</td></tr>'
	try:
		durval=float(MyD["duration"])
		colortag=""
		colorcode = (durval > 5) + (durval > 10) + (durval > 30) 
		lencolortext = lencolors[colorcode]
		
		if not colorcode:  # duration < 5 seconds
			writexattrs(filename,["Gray","short"])
			
		fsize = os.path.getsize(filename)
		sizestr = engstr(fsize,format="%.1f",si=True)+"B"
		# sizestr = "{:3.2g}".format(fsize).replace("+0","").replace("+","")
		sizecolorcode = (fsize > 15000000) + (fsize > 150000000) + (fsize > 65000000) 
		sizecolortext = sizecolors[sizecolorcode]

		if DEBUG:
			print >> sys.stderr, "Size:", fsize, sizecolortext, type(fsize)

		url = ''
		if MakeFrame:
			imagepath,tinypath = generatethumbnail(filename,durval,int(MyD['height']), int(MyD['width']))
			url = '<a href="{}"><img src="{}" width=300></a>'.format(imagepath,tinypath)
			
		return RowText.format(
						 lencolortext,
						 durval,
						 framed.get(MyD['r_frame_rate'],'gray'),
						 MyD['r_frame_rate'], 
						 sized.get(MyD['width'],'gray'),
						 MyD['width'], 
						 MyD['height'], 
						 MyD['tag:creation_time'],
						 MyD['codec_name'],
						 sizecolortext,
						 sizestr,
						 url,
						 filename) + "\n"
		
	except (TypeError,KeyError) as e:
		# can add error report
		# repr( e ) 
		return "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td>#-- No Data --#</td><td>{}</td></tr>\n".format(filename)
			
	
def getexifattrs(F,KeywordList,CreditLine):
	# build the keyword parameters based on this format:
	# 'exiftool  -v0 -overwrite_original -keywords=one -keywords=two -keywords=four -credit={haddock at mbari dot org}'
	
	
	KeywordString = ""
	for Keyword in KeywordList:
		KeywordString += ' -keywords="{0}" -subject="{0}"'.format(Keyword[:64])
		
		
	
	# turn off backup file verbose 0  -overwrite_original 
	ExifCommand = u'exiftool -api largefilesupport=1 -v0 -overwrite_original {0} {1} "{2}"'.format(KeywordString,CreditLine,F)
	ExifCommand = ExifCommand.encode('utf8')# sys.stderr.write(ExifCommand + "\n")
	if DEBUG: sys.stderr.write("\n==>Exif: {0}\n".format(ExifCommand))
	
	ProcOut = subprocess.Popen(ExifCommand, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,shell=True) 
	Result = ProcOut.stdout.readlines()
	
	# subprocess format:
	# subprocess.Popen('ls *.jpg', stdout=subprocess.PIPE, stderr=subprocess.STDOUT,shell=True).stdout.readlines()
# =========================



def engstr(x, format='%s', si=False):
	'''
	From http://stackoverflow.com/a/19270863/1681480
	Returns float/int value <x> formatted in a simplified engineering format -
	using an exponent that is a multiple of 3.

	format: printf-style string used to format the value before the exponent.

	si: if true, use SI suffix for exponent, e.g. k instead of e3, n instead of
	e-9 etc.

	E.g. with format='%.2f':
		1.23e-08 => 12.30e-9
			 123 => 123.00
		  1230.0 => 1.23e3
	  -1230000.0 => -1.23e6

	and with si=True:
		  1230.0 => 1.23k
	  -1230000.0 => -1.23M
	'''
	import math
	x=float(x)
	sign = ''
	if x < 0:
		x = -x
		sign = '-'
	exp = int( math.floor( math.log10( x)))
	exp3 = exp - ( exp % 3)
	x3 = x / ( 10 ** exp3)
	if si and exp3 >= -24 and exp3 <= 24 and exp3 != 0:
		exp3_text = 'yzafpnum kMGTPEZY'[ ( exp3 - (-24)) / 3]
	elif exp3 == 0:
		exp3_text = ''
	else:
		exp3_text = 'e%s' % exp3
	return ( '%s'+format+'%s') % ( sign, x3, exp3_text)
	
def	getexinfo(filename,Fields):
	# use exiftool with epochsecond format
	# exifcommand = 'exiftool -v0 -S -DateTimeCreated  "%s" ' % (filename)
	exifcommand = 'exiftool -api largefilesupport=1 -tab -v0 -S  %s "%s" ' % (Fields,filename) 
	if DEBUG:
		print >> sys.stderr, exifcommand
	expipe = os.popen(exifcommand)
	exdata = expipe.readlines()
	# print "EXDATA,",exdata
	if exdata:
		return exdata[0]
	else:
		return "{}	No File".format(filename)

def	getffmpeg(filename,Fields,Text):
	# use exiftool with epochsecond format
	if DEBUG:
		print "Debugging on"
	exifcommand = 'ffprobe -v quiet -print_format compact -show_streams "%s" ' % (filename)
	if DEBUG:
		print >> sys.stderr, exifcommand
	expipe = os.popen(exifcommand)
	exdata = expipe.readlines()
	# multiple lines per file. Make a table of values?
	D = {}
	num = 0
	out = ''
	# three fields in each stream: audio, video, data
	if DEBUG:
		print >> sys.stderr, "EXDATA,",exdata
	if exdata:
		if 'codec_type=video' in exdata[0]: # Nikon movies
			usedata = exdata[0]
		else:
			usedata = exdata[1]
		for Pair in usedata.rstrip().split("|")[1:]:
			K,V = Pair.split("=")
			D[K] = V 
		if DEBUG:
			for Key in D:
				print "%30s\t%s"%(Key,D[Key])
		if Text:
			fsize = os.path.getsize(filename)
			sizestr = engstr(fsize,format="%.1f",si=True)+"B"
			# sizestr = "{:3.2e}".format(fsize).replace("+0","").replace("+","")
			
			for T in Fields:
				out = out + D[T] + '\t'
			out += sizestr +'\t'
			out += filename.split("/")[-1] + "\n"
			return out
		else:  # HTML OUT
			return D
		# want to keep codec_name,tag:encoder,duration,r_frame_rate,width,height,tag:creation_time
	else:
		return "No File\t-\t-\t-\t{}\n".format(filename)

def get_options():
	parser = argparse.ArgumentParser(usage = __doc__)
	parser.add_argument("-t", "--text",    action="store_true", dest="TEXT",  default=False, help="Print as text instead of html")
	parser.add_argument("-i", "--images",  action="store_true", dest="FRAME", default=False, help="Generate thumbnails from video")
	parser.add_argument("-e", "--exif",    action="store_true", dest="EXIF",  default=False, help="Use Exiftool")
	parser.add_argument("-b", "--debug",   action="store_true", dest="DEBUG", default=False, help="Debug output")

	parser.add_argument("Args", nargs=argparse.REMAINDER)
	options = parser.parse_args()
	return options

		

#For FFMPEG
Fields = [ 'duration', 'r_frame_rate', 'width', 'height', 'tag:creation_time','codec_name']
# Fields =    [ 'duration', 'r_frame_rate', 'width', 'height', 'tag:creation_time','tag:encoder']
Header = "\t".join(Fields) + "\tFileSize\tFileName"

if __name__ == "__main__": 
	Options = get_options()
	global DEBUG
	DEBUG = Options.DEBUG
	
	if not Options.Args:
			sys.exit(__doc__)
	else: # give a way to run on specified directories...
		starttime = time.time()
		FileList = Options.Args
		if Options.EXIF:
			Options.TEXT = True
			Options.FRAME = False
			# for exiftool 
			Fields = "-FileName -FileSize -FileType -MovieDataSize -CreateDate -Duration -ImageWidth -ImageHeight -CompressorName -VideoFrameRate "
			Header = Fields.replace("-","").replace(" ","\t")
			sys.stderr.write("Using Exiftool ... \n")
			
		outstring = ""
		# Might want to do this as a bulk operation
		if Options.TEXT:
			print Header
		else:
			printhead(Fields)
		for File in FileList:
			if Options.EXIF:
				out = getexinfo(File,Fields)
			else:
				out = getffmpeg(File,Fields,Options.TEXT)
			if DEBUG:
				print >> sys.stderr, "OUT:",out
			print >> sys.stderr, ".",
			if Options.TEXT:
				outstring = out
			else:
				outstring = printrow(out,File,Options.FRAME)
			# TODO: change to red text?
			print outstring
			
		print >> sys.stderr,"#"

		if not Options.TEXT:
			printtail()

		elapsed = time.time() - starttime
		
		if elapsed > 3660:
			timestring	= "{:.1f} hours".format(elapsed/3600.0)
		elif elapsed > 60:
			timestring	= "{:.1f} minutes".format(elapsed/60.0)
		else:
			timestring = "{:.1f} seconds".format(elapsed)

		sys.stderr.write("### Finished processing {} files in {}... \n".format(len(FileList),timestring))



"""
NOTES: 


FileName: SHOGUN_S007_S001_T055.MOV
FileSize: 3.0 MB
FileType: MOV
MovieDataSize: 3158992
CreateDate: 2015:03:16 07:52:41
Duration: 0.17 s
ImageWidth: 1920
ImageHeight: 1080
CompressorName: .Apple ProRes 422 HQ
VideoFrameRate: 29.97
GenFlags: 0 0 0
OtherFormat: tmcd



For large file annotation (>4GB) using exiftool, 
    Create a file at ~/ExifTool_config with these 3 lines in it:

%Image::ExifTool::UserDefined::Options = (
	LargeFileSupport => 1,
);

"""


