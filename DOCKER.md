# Docker

You can build this code as a docker container so that you don't need to install anything on your lcoal machine - except [Docker](https://www.docker.com/).

## Usage

### build

Note that you do not need to build this unless you've made changes to the python code. This is for developers. Users can skip on down to the run section below

```bash
docker build -t mbari/py-database .
```

The first time you build it, it will take a while. Be patient. Also, don't try to build it at the coffee shop, it's going to download a lot of stuff.

### run

Most users can just run the container. The first time you run it, it will `pull` the container from <https://hub.docker.com/r/mbari/py-database/>

```bash
docker run -it mbari/py-database
```

This will bring up a bash shell inside your shiny, new docker container. You don't need to do much except run your code.

```bash
vars_retrieve.py -s D422 R500
```

## Advanced

If you want to be able to redirect output from your script to a local directory, you can start docker with the following:

```bash
docker run -it -v $HOME/Downloads:/data mbari/py-database
```

Then in the docker container you can redirect output to the `/data` directory to save it to a local file in your `~/Downloads` directory:

```bash
vars_retrieve.py -s D422 R500 > /data/samples.txt
```
