#! /usr/bin/env python2.7
# -*- coding: utf-8 -*-

"""
v 7.23 : Removed requirement for Depth Constraint in queries (for un-merged dives)
v 7.22 : Removed requirement for TimeString in queries
v 7.21 : Added --timecode option to just grab first result matching TC
v 7.20 : added --video and --videofile options to list and get videos
          Needs concept and dive to yield a result
v 7.19 : Added mindepth, maxdepth to getconcept and getimages
v 7.18 : Started adding mindepth, just to get_concept_by_dive
v 7.17 : Moved TODO out of docstring. (TODO --maxdepth to queries not just tapes)
v 7.16 : Changed server from perseus to venus
v 7.15 : Fixed field mismatch when retrieving images of samples
v 7.14 : Updated --gettapes to get full records. Get videoID instead of VideoSet ID
v 7.13 : Adding --gettapes to retrieve video archive names for depth range
v 7.12 : Adding --simplekml to only plot one pin per concept per dive
v 7.11 : Adding --kml support to general (not only dive) concept queries
v 7.10 : Added time of day to lookup with --hb and --ha
v 7.09 : Changing CTD retrieval to save separate files (--depth still prints)
v 7.08 : Robustness in debug reporting
v 7.07 : Modified video URLs to return mezzanine mp4 instead of prores
v 7.06 : Updated sample-related image retrieval
v 7.05 : Added salinity, oxygen, transmiss to Samples query
v 7.04 : Tiny bugs
v 7.03 : Enhanced --near option to work with or without PI name
v 7.02 : Fixed bug in getimages() bounding box exclusion
v 7.01 : Made bounding box exclusion optional
v 7.0  : Implementing searching near a waypoint or lat/lon
v 6.94 : Added KML output for samples (partially tested)
v 6.93 : Typo in new code to exclude bounding boxes
v 6.92 : Specifically remove bounding box records from results
v 6.91 : Fixed EXPD server to perseus when called externally from vars_cleanup
v 6.9  : Added --notransect option to avoid results from Mission:transect
v 6.84 : Added --kml option for -p or -d
v 6.83 : Avoided some duplicate annotations by removing .png
v 6.82 : Added extended debugging output
v 6.81 : Put USEODBC into importable query function
v 6.8  : Major update to use pyodbc in addition to or instead of pymssql
v 6.73 : Fixed major bug in -p where dives < 1000 were not zero-padded 
       : Still need to fix double-entries for samples (because of jpg+png)
v 6.72 : Fixed URL for --flyer image retrieval
v 6.71 : Added image retrieval option to associations (-a) queries
v 6.7  : Updated GetImages to have ==both option for PNG and JPG.
v 6.61 : Added EpochSecs to --depthonly output
v 6.60 : Improved image retrieval for samples and naming for new URLs. TODO: batch dives
v 6.5  : Added --predator option to find X eating Y only (not Y->X)
v 6.4  : Updated for M3 compatibility!
v 6.3.2: Made CTD retrievals more robust to missing values
v 6.3.1: Fixed header line for --depthonly
v 6.3  : Added --depthonly to return just time and depth for each dives
v 6.22 : Added transmiss to --samplenum
v 6.21 : Removed error message when no results and --samplenum
v 6.2  : Added --samplenum option to get specified samples. Warning -s is more robust.
v 6.13 : Output version number when launching. Support for --help
v 6.12 : Moved EXPD to perseus
v 6.11 : Added both Analog3 and 4 to --ctd output
v 6.1  : Fixed mixup with Before and After date definitions.
v 6.0  : Changed server to perseus. Haven't tested --hurry yet.
v 5.1  : Edits by Brian for Dockerization
v 5.0  : Trying to add feeding associations to -a option
v 4.99b: Fixed -g turning on images w/o -i
v 4.991: Adding back in -g for --PNG
v 4.99 : Fixing/adding multi-dive specification for CTD retrieval -d V{4062..4067}.
v 4.98 : Putting in a --raw option for ctd
v 4.97 : Forgot to comment out debugging line
v 4.96 : Added Lat and Lon --ctd. 
v 4.95 : Added PAR sensor retrieval (analog4) to --ctd. (Hope same for vnta.)
v 4.9  : Added image URL, removed Observer from Sample queries
v 4.85 : Fixed gnatlike bug where search was repeated if --wildcards. 
v 4.84 : Fixed missing header from comment + concept search. 
v 4.83 : Fixed bug introduced with date-constrained searches and -k
v 4.82 : Added use of foghorn backup server when using the --hurry option
v 4.81 : Changed rank option to -r --rank
v 4.8  : Made it so date constraints (A,B) work with -k searches
       : TODO: prevent duplicate images with -k
v 4.71 : Changed CTD header to Transmiss for Light (660nm, 25cm)
v 4.7  : Slight change to normalization 149.9=>100, 150.0=>200
v 4.63 : Fix associations, broken by v4.53 (indexing & missing comma)
v 4.62 : Fixed (somewhat) returning images with the --flyer option 
v 4.61 : Examples plus Return transmiss from vanilla concept search
v 4.60 : Added date constraints (--after --before) to most queries
v 4.53 : Added transmissometer (Light) after Salinity (660nm, 25cm)
v 4.52 : Added (now unused) block of code for supplemening feeding
v 4.51 : Added --comma support to --effort and --normalization output 
v 4.5  : Added --effort option to generate 2D normalization matrix 
v 4.4  : Added --ctd option to get CTD profiles with -d and -p 
v 4.38 : Fixed login and remote module import issues
v 4.37 : Fixed some issues with comma-separated species in -c list
v 4.36 : Added comma delimited output option --comma (moderately tested)
v 4.35 : Fixed downloading PNGs for multiple dives
v 4.34 : Fixed frame grab downloads for changed DocRicketts URLs.
v 4.33 : Added --bin option for normalization
v 4.32 : Added wildcard support to upconcept searches.
v 4.31 : Added -r for --getrank and change -g to --png only
v 4.3  : Added --getrank option to look up a ranks or print a table of ranks
v 4.2  : Consolidated header handling, added printhead, bugfixes
v 4.11 : World's tiniest bug fixed
v 4.1  : Restructured and added simple SQL query function for import
v 4.04 : Gave the getdivesummary function an option to return the string
v 4.03 : Fixed --biolum parsing when no concept is provided
v 4.02 : Changed output of --biolum to show NA for things not in KB
v 4.01 : Fixed header when retrieving by -k and -p
v 4.0  : Added --biolum option
v 3.97 : Filtering out depths > 4500 meters. ...
v 3.96 : Remove miniROV dives, Generate SkipList
v 3.95 : Added -f1 option to -p to print dive strings
v.3.94 : Can use comments to download images... 
v 3.93 : Improvements and robustness for --normalize
v 3.91 : Adding -k to comment finder
v 3.9  : Added time at quantized depth for normalization.
v 3.83 : Bug fix in overly optimistic recursion scheme
v 3.82 : Handles strange unicode in --hurry mode better
v 3.81 : Made --hurry mode work in combo with dive and PI constraints
v 3.8  : Added --hurry mode for simple but large concept-only queries
v 3.75 : Added time for query processing to debug mode
v 3.74 : Super sample mode --super gives all samples for old dives
v 3.73 : Handles stray linefeed in Notes field
v 3.72 : Added timeout message for db connection
v 3.71 : Fixed glitch if running all feeding w/o dive
v 3.7  :  Updated to tolerate Mini ROV dives
V 3.6  : Added option to get all associations
v 3.53 : Fixed get PNG (-g) for category (-k) searches 
v 3.52 : Make the parent concept search case insensitive 
v 3.51 : Fix header names and change header separater to underscore 
v 3.5  :  Retrieve parents of concept by I (TODO: return with each record)
v 3.43 : Can constrain image retrieval by PI
v 3.42 : Made it so dive constraint returns full set of fields
v 3.4  :  Changed default use of wildcard in concept searches
v 3.32 : fixed bug from last edit for simple concept retrieval
v 3.31 : fixed retrieval of concepts hierarchy and format string
v 3.3  :  Added retrieving samples or dive annotations by KB concept group
v 3.2  :  shipboard version
v 3.1  :  Improved handling of combinations of arguments.
V 3.0  :  Refactored to use argparse with more control
v 2.2  :  Added PI searching for dive summaries and samples
v 2.1  :  Works with partial database for offsite access
V 2.0  :  Added ability to download images by concept or dive number
v 1.47 : Changed order of fields in output, added field number for cutting
v 1.46 : Cleaned up output and improved library installation instructions
v 1.45 : Added associations and reformatted output
v 1.4  :  Added extraction of species from higher taxon
v 1.3  :  Handle sample numbers in V3875-D5 format
v 1.2  :  Flag to perform different queries at run time
v 1.1  :  Parses the V2132 format. Can also take dive number w/o vehicle
  [haddock at MBARI dot org]

Will find either a concept or a list of samples from a dive.

To find a concept run with a COMMA-separated list of concept names (no space).
If there are spaces in the concept, enclose the whole thing in quotes: 
   'Aegina,Aegina citrea,Aegina rosea,Aegina sp. 1'
   
For wildcards, put % where you want wildcards and enclose in quotes: '%salp%'

To find samples from a dive, run with the appropriate flag as true,
and send in a space- or comma-delimited list of dive numbers.

NOTE: Comma-separated lists cannot have spaces too...

You can search in these ways:

 -a  : associations between groups
 -b  : print debug output
 -d  : dive summary for dives [or to specify dives with other searches]
 -f  : print as formatted report [mainly for associations and -k option]
 -c  : get all annotations of concepts for all dives
        WARNING: Genus along will not get the species. 
        Use -k option or put full species in quotes
        NOTE: You can also put % after part of the name to use wildcards
 -k  : find species included within a higher taxon
        Can be used with -p -d -i
        By itself, just lists all concepts below the given concept
        Used with a dive or PI or image and it returns results matching any
 -i  : images from a dive (specify a query with -p or -d and -c or -k)
 -g  : as above, but in PNG format same as --png
 -l  : List the images links only, don't download
 -s  : samples from a dive
 -u  : search "up" through the concepts to show hierarchy
 -w  : use wildcards in concept searches (Periphyll will match Periphyllopsis)
 -z  : pull depth information for a dive
 -r  : Get the Rank (Phylum, Class) associated with an upconcept search
        Case sensitive
        Use with -u to print a classification table
 
 -A  : Return results after the given date MM-DD-YYYY
 -B  : Return only results before the given date
       Use -p % for wildcard searches for all PIs
 
 --png       : retrieve PNGs instead of JPEGs for images
 --flyer     : search on internal server of Western Flyer
 --predator  : Like --association but only find X feeding on Y when given X+Y
 --anela     : Find all feeding associations. Can specify dive or PI
                   Two different format options: summary [default] or -f1 for full table 
                   Does not return unique records in summary table. Must use vars_unique.py
 --super     : On old dives, a sample reference is not always there. 
                   Use this to search for all "sample" Links
 --samplenum : Get samples from comma-separated list: vr --samplenum D860-SS12,D860-ss10,D959-D1
 --header    : Print the header line with numbers
 --hurry     : For concepts with many results (>20,000) will print more quickly
 --normalize : Generate normalization for each depth for series of dives
 --effort    : Generate a matrix of dive, depth, time (effort) 
                  Differs from normalizing because it does not integrate all dives together
 --bin       : Bin size in meters for normalization
 --binsize   : Bin size in meters for normalization
 --ctd       : Print CTD data for dive or dives with -d or -p
 --comment   : Search in the LinkName, LinkValue, ToConcept for terms. 
                  Requires a concept with -c or -k. Use -k Eukaryota to search all animals
 --rank      : Either get the rank of a concept (case sensitive)
                  Or used with -u get a classification table for a concept
 --biolum    : Retrieve dictionary of Concept:is-bioluminescent
                  Use with -c or -k to look up taxa
                  Format with -f1 to get python dictionary
 --comma     : Delimit output with commas
                  Do not use if piping to vars_unique
 --kml       : Return KML (Google Earth) of results rather than table (with -c -s -d or -p)
 --simplekml : put only one placemark per dive+concepth for KML
 --waypoint  : Show the locations of the waypoints containing that substring
 --near      : List dives by station name or lat,lon
 --bb        : Omit annotations with designated bounding boxes
 --hb, --ha  : Search only for records or images which happen between those hours (int)
 
For each of the main options, you can use in combination with 
   -p [PI name] or -d [dive range] and/or -c [concept name].

   -p     : get dive summaries by PI (or multiple PIs with commas)
             to limit to the most recent X dives, put ,X at the end...
   -d     : provide a range of dives for a search
          : d by itself without other flags above gets a dive summary
   -a     : get all associations between two or more groups, separated by +
             secret option: put 1 at the end for full table (nothing is summary)
   -k     : get all species for a higher taxon (takes only one name)
             secret option: put -f 1 at the end for quoted csv, -f 2 for full table
             Note: by itself this just lists sub-concepts, and doesn't retrieve
             However, used in conjunction with -s or -i, it will retrieve records
 -s -p    : get sample lists from listed PIs. Use ,X to limit to recent X dives
 -s -k    : get samples matching a broader KB concept
 -d -k XXXX -d YYYY   : get all annotations for KB group XXXS on a dive YYYY
 
Use grep, cut and sort to extract certain columns.

For associations, default is a summary table. Use -f1 to get all the records.  
Usage examples: 
	vars_retrieve.py -s D422 R500          # Get all samples from Doc Ricketts 422 and 500
	vars_retrieve.py -d D422,v360          # Get dive summaries for two dives
	vars_retrieve.py -c Aulacoctena,"Bathyctena chuni"
	vars_retrieve.py -i Paraphronima
	vars_retrieve.py -i -c Bathyctena
	vars_retrieve.py -i -d T412                    # get all images from T412
	vars_retrieve.py --png -d T412 -c Aegina       # get all Aegina images in PNG format from T412
	vars_retrieve.py -d v{2770..2880}              # get a dive summary for all dives between those two numbers
	vars_retrieve.py -k Narcomed,Scyphozo          # All species for both Narcos and Scyphos 
	vars_retrieve.py -u Aegina                     # Search Up the tree for parents of concept  
	vars_retrieve.py -a Narcome,Scyphozoa+Hyperiid # Associations between Narcos and Amphipods
	vars_retrieve.py -p Haddock,Robison,10         # Get the most recent 10 dives each by Haddock and Robison
	vars_retrieve.py -c Erenna% -p Haddock,10 -i   # Get Erenna spp images for the most recent Haddock 10 dives 
	vars_retrieve.py -s -p Haddock,2               # Get the sample list from the two most recent Haddock dives
	vars_retrieve.py -d T{840..847} -k Lobata      # Get all lobate ctenophores annotated on dives T840 through T847
	vars_retrieve.py -d T841 -c Bathocyroe         # Exact concept Bathocyroe (Genus will not find genus+sp
	vars_retrieve.py -s -d D326 -k Ctenophora      # Get all ctenophore samples from dive Doc 326
	vars_retrieve.py -d D55 -s -c "'Marrus%','Physonect%'"  # Formatting for multiple specific concepts
	vars_retrieve.py -d v{3500..4000} | sort -k 4  # Get Ventana dives between 3500 and 4000 sorted by number
	vars_retrieve.py -c Macropinna% | cut -f1,2,22 | sort -u # Get a summary of dives where Macropinna was seen
	vars_retrieve.py -u squalus -f2                # Show the parent group classification for dogfish
	for C in Squalus Bathyctena; do vr --rank -u $C | grep -v phylum; done  # give a table of ranks for multiple concepts
	vars_retrieve.py --rank squalus                # Show the rank classification
	vars_retrieve.py --predator Apolemia+Cnidaria  # Find all examples of Cnidaria feeding on Apolemia. Compare -a
	vars_retrieve.py --anela -p Haddock,10         # Return summary of feeding associations on last 10 Haddock dives
	vars_retrieve.py --anela -d D505,V829 -f1      # Return complete records of feeding associations.
	vars_retrieve.py -c Beroe -p Haddock --hurry   # Get all Beroe from Haddock dives, with performance boosting
	vars_retrieve.py --comment damaged -k Ctenophora -i  # Get all images of damaged ctenophores
	vars_retrieve.py -u Bathocyroe --rank          # Return a classification table for Bathocyroe
	vars_retrieve.py --normalize -p Haddock,10     # Create a histogram of depth bins showing effort for dives
	vars_retrieve.py --effort -p Haddock,10        # Create a table of time at depth per dive
	vars_retrieve.py --ctd -p Haddock,2 --bin 100 > ctd.dat    # Save 2 recent CTD records to a file
	vars_retrieve.py -c Solmissus -A 12-01-2015 -B 12-31-2015 # Show Solmissus seen in December 2015
	vars_retrieve.py -k Histioteuthidae -A 01-01-2015 -B 12-31-2015 -s -p %    # All histioteuthis sampled in 2015
	vr --samplenum D860-SS12,D860-ss10,D959-D1     # get samples listed. Use no spaces
	vars_retrieve.py -p Robison,10 -s --notransect # get samples, excluding transect dives
	vars_retrieve.py -p % -A 09-01-2016            # Show dive summaries for all dives after Sept 1 2016
	vars_retrieve.py --simplekml -c Rhizophysa     # Show one pin per dive per concept in KML
	vr -d D{1240..1245} --kml > FlyerSoCal.kml     # Create KML (Google Earth) file for that dive range
	vr --waypoint "Doc 2a"                         # Show location of named waypoint (substring match)
	vr --waypointname 36.2,-123.15                 # Find named waypoint near lat/lon 

New options: 
    "--after"         : "Only records after this date GMT. Format MM/DD/YYYY"
	"--before"        : "Only records before this date. Format MM/DD/YYYY"
	"--notransect"    : "Do not return transect records from PI searches"
	"--makewaypoints" : "Make waypoint file from CSV"
	"--waypoint"      : "Return lat lon for waypoint names"
	"--near"          : "Find dives or samples near a waypoint (named or Lat,Lon)"
	"--epsilon"       : "Degrees away from waypoint defined with --near"
	"--waypointname"  : "given a lat/lon, find the name of the closest waypoint"
	"--bb"            : "Omit records with a bounding box"
	"--hourbefore"    : "Hour of day GMT to start search"
	"--hourafter"     : "Hour of day GMT to end search"
	"--gettapes"      : "Get tape IDs for specified PI or Dives"
	"--mindepth"      : "Minimum depth for get tapes"
	"--maxdepth"      : "Maximum depth for get tapes"

Requires: pymssql or pyodbc module, freetds-dev, and unixODBC 

Installation instructions are a comment in the source code directly below...

CTD Units described here: http://mww.mbari.org/expd/UserDocs/FormatCtdDer.htm

"""

'''
######### TODO BLOCK ###########
# TODO:  Fix it so multi-dive image retrievals are done in one query, not per dive
# 
# TODO: Transect filter with topo", "transect", "diel transect", "starttransect", or "endtransect"
# TODO:  Add summary fields for Temp and Salin at 200 (x?) meters and O2 at Y meters 
# 
# TODO: Add --count to return only the number of records. The problem is that redundant records will dominate w/o vars_unique
# TODO: Change comment implementation to use a CommentString that gets 
#       inserted into existing functions (image, etc)
# 		
# TODO: Restrict PI dive search by minlat maxlat minlon maxlon or avglat avglon and ± --navprecision
#
# TODO: Look up station name for -d and -s
# 
# TODO: Allow search constraint by station name (box around lat/lon ± 0.x degrees)
# 
# TODO: LOOK UP BOTTOM DEPTHS for RECORD
# Can potentially use elevation API but wouldnt want to query every record:
#    https://github.com/googlemaps/google-maps-services-python
# 
# TODO: add --exemplar tag to search by -c or -k with --comment good 
#        and retrieve a random (most recent?) image
#
# TODO: Retrieve frame grabs de-novo
# # Installation with poetry
# # pip install poetry
# # git clone https://github.com/mbari-org/beholder-py.git
# # cd beholder-py && poetry install
# ###########################################################################
# # This code below not work with master video files due to issues with the LTO tape recall
# ###########################################################################
# from beholder_client import BeholderClient

# api_key = 'RmF5b3NIZWlsbWFuR2FybG9ja1NjaGVua1NnYWllckppbWluZXo='  # this comes from Raziel
# base_url = 'http://singularity.shore.mbari.org:8088/'  # this is also available in Raziel

# client = BeholderClient(base_url, api_key)

# video_url = 'http://m3.shore.mbari.org/videos/M3/mezzanine/DocRicketts/2022/03/1438/D1438_20220323T164756Z_h265.mp4'
# elapsed_time_ms = 42340

# pil_image = client.capture(video_url, 42340)  # returns a parsed PIL image

# # Save the pil image to disk
# pil_image.save('D1442_20220528T155201Z_h265_42340.png')
# 
######### END TODO BLOCK ###########
# '''
# main() code is at the bottom

# TODO: Fix multi-dive image download: Search for dives with matches and then loop through those?
# TODO: Add option to output latitude of waypoints in degrees minutes
# TODO: make multi-dive option work with --comment. 
#     Might be good to refactor the whole query into one function with options added 
# TODO: add LatLon range to queries?
# TODO: Get ctd data from EXPD: select * from Rovctd
# Options: 
# SELECT divestartdtg, diveenddtg FROM Dive WHERE rovname = ? AND divenumber = ?` 
# SELECT DatetimeGMT, t, s, o2, o2Flag, o2alt, o2altFlag, light, p FROM <rovname>RovctdData WHERE DatetimeGMT BETWEEN <divestartdtg> AND <diveenddtg>
# <rovname>CleanNavData

# TODO: Long one - reformat combined queries like samples to use OR LIKE statements — otherwise wildcards don't work...
'''	SELECT DISTINCT
	      CONVERT(varchar(22), RecordedDate, 120) as DateTime24,
	      RovName, DiveNumber, ConceptName, 
	      Depth, Temperature, Oxygen, Salinity, Light as Transmiss, Latitude, Longitude, TapeTimeCode, 
	      Image, LinkValue 
	FROM  dbo.annotations_legacy
	WHERE ((RovName like 'Doc Ricketts') AND (DiveNumber IN ('0027','0340') )
	AND ((LinkName LIKE '%sample-reference%') ) AND (
	(ConceptName LIKE 'Marrus%') OR 
	(ConceptName LIKE 'Platycten%')
	ORDER BY DateTime24,TapeTimeCode ;'''
	


InstallString="""
# IF YOU ARE SEEING THIS, THEN REQUIRED LIBRARIES WERE NOT FOUND.
# INSTALLATION: Do all these steps and commands to install the 
# pre-requisites for OSX, in this order. Commands refer to the Terminal
#
# If it asks you for a password, it will be your normal login password
#     1. If you don't have gcc (type "which gcc" and see if it is found) then you will need
#        the Apple Command Line Tools
         You should be able to get these by typing
           gcc
         in a terminal window, and if you don't have them, it will pop up a prompt 
         asking if you want to install them. 
#     2. Install HomeBrew with these exact commands: 
#         ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
#         sudo chown -R $USER /usr/local
#         brew doctor
#     3. Install pip with this exact command:
#         sudo easy_install pip
#     4. Now you can install the packages:
#          brew install unixodbc
#          brew install --with-unixodbc homebrew/versions/freetds091
#          brew link --force freetds@0.91
#          see Linux instructions below
#          python -m pip install pymssql  # may need sudo
# 
# You can run via Docker following these instructions (thx to Brian S)
#      https://bitbucket.org/mbari/database/src/master/DOCKER.md
#
# INTALLATION FOR UBUNTU / LINUX:
 sudo apt install unixodbc
 sudo apt install unixodbc-dev
 sudo pip install pyodbc
 install MS drivers with https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server
 # sudo apt install freetds-dev   # Necessary?
 # sudo apt install python-pip    # May not be necessary on some systems
 # sudo apt install python-dev    # May not be necessary on some systems
 # sudo apt install net-tools     # Necessary for ubuntu minimal install
"""



def get_options():
	parser = argparse.ArgumentParser(usage = __doc__)
	parser.add_argument("-b", "--debug",    action="store_true", dest="DEBUG", default=False, help="Print debug info")
	parser.add_argument("-c", "--concept",    action="store", dest="Concept", help="Search for concept in comma-separated list")
	parser.add_argument("-l", "--listimages",    action="store_true", dest="ListImages", default=False, help="Search for images of listed concepts")
	parser.add_argument("-i", "--images",    action="store_true", dest="GetImages", default=False, help="Search for images of listed concepts")
	parser.add_argument("-g", "--png",    action="store_true", dest="GetPNG", default=False, help="Download PNG instead of JPEG")	
	parser.add_argument(      "--both",    action="store_true", dest="GetBoth", default=False, help="Download both PNG and JPEG")	
#	parser.add_argument("-h", "--help",    action="store_true", dest="Help", default=False, help="Print help")	
	parser.add_argument("-d", "--dives",    action="store", dest="Dives", help="Provide dive numbers for other searches -i -c -r")
	parser.add_argument("-p", "--pi",    action="store", dest="Pis", help="Search for dives by PI")
	parser.add_argument("--kml",    action="store_true", dest="Kml", default=False, help="Output dive summaries as KML, given PI or dive numbers")
	parser.add_argument("--simplekml",    action="store_true", dest="Simple", default=False, help="Only show one pin per concept per dive in KML")
	parser.add_argument("-s", "--samples",    action="store_true", dest="Samples", default=False, help="Retrieve samples from dive or PI (with -p)")
	parser.add_argument("-k", "--knowledge",    action="store", dest="Knowledge", help="Get concepts from Knowledgebase")
	parser.add_argument("-u", "--upladder",    action="store", dest="UpConcept", help="Get Parent Hierarchy from Knowledgebase")
	parser.add_argument("-a", "--association",    action="store", dest="Association", default=False, help="Get associations between Concept+Concept")
	parser.add_argument("-f", "--format",    action="store", dest="Format", default=0, help="Format for printed reports")
	parser.add_argument("-w", "--wildcard",    action="store_true", dest="Wildcard", default=False, help="Use Wildcards in Searches")
	parser.add_argument("-v", "--version",    action="store_true", dest="Version", default=False, help="print version on launch")
	parser.add_argument("-z", "--getdepth",    action="store_true", dest="GetDepth", default=False, help="Get dive depth profile")
	parser.add_argument("--super",    action="store_true", dest="Supersample", default=False, help="All 'sampled' annotations, for old records")
	parser.add_argument("--anela",     action="store_true", dest="Anela", default=False, help="Find all feeding associations (per dive)")
	parser.add_argument("--predator",    action="store", dest="Predator", default=False, help="Get only Predator of associations Concept+Concept")
	parser.add_argument("--hurry",     action="store_true", dest="Hurry", default=False, help="Print Rows as they come...")
	parser.add_argument("--comment",     action="store", dest="Comment", help="Find records for concept with specified comment")
	parser.add_argument("--normalize",    action="store_true", dest="Normalize", default=False, help="Generate column of seconds per depth for dives or PI")
	parser.add_argument("--samplenums",    action="store_true", dest="SampleNums", default=False, help="Search only for comma-separated sample numbers")
	parser.add_argument("--effort",    action="store_true", dest="GetEffort", default=False, help="Generate 2D matrix of seconds per depth per dives or PI")
	parser.add_argument("--flyer",    action="store_true", dest="Flyer", default=False, help="Run on Flyer internal database")
	parser.add_argument("--biolum",    action="store_true", dest="Biolum", default=False, help="Find Bioluminescence")
	parser.add_argument("--useship",    action="store_true", default=False, help="Use ship nav for ROV summaries")
	parser.add_argument("-r","--rank",    action="store_true", dest="GetRank", help="Return Rank for Concept Searches")
	parser.add_argument("--header",    action="store_true", default=False, help="Print the main header line with numbers")
	parser.add_argument("--bin","--binsize",    action="store", type=int, dest="BinSize", default=10, help="Size of bins for --normalize. Default 10")
	parser.add_argument("--comma",    action="store_true", default=False, help="Use comma as delimiter on [certain] output")
	parser.add_argument("--ctd",    action="store_true", dest="CTD", default=False, help="Retrieve CTD data for dives or PI")
	parser.add_argument("--depthonly",    action="store_true", dest="DepthOnly", default=False, help="Retrieve CTD depth only for dives or PI")
	parser.add_argument("--raw",    action="store_true", dest="Raw", default=False, help="Don't calibrate light sensor data for CTD")
	parser.add_argument("-A","--after",  action="store", dest="After",default="12/31/1980", help="Only records after this date GMT. Format MM/DD/YYYY")
	parser.add_argument("-B","--before", action="store", dest="Before",default="12/31/2500", help="Only records before this date. Format MM/DD/YYYY")
	parser.add_argument("--notransect", action="store_true", dest="NoTransect",default=False, help="Do not return transect records from PI searches")
	parser.add_argument("--makewaypoints", action="store", dest="Waypointfile",default="", help="Make waypoint file from CSV")
	parser.add_argument("--waypoint", action="store",default="", help="Return lat lon for waypoint names")
	parser.add_argument("--near", action="store",default="", help="Find dives or samples near a waypoint (named or Lat,Lon)")
	parser.add_argument("--epsilon", action="store",default=0.015, help="Degrees away from waypoint defined with --near")
	parser.add_argument("--waypointname", action="store",default=False, help="given a lat/lon, find the name of the closest waypoint")
	parser.add_argument("--bb", action="store_true",dest="BoundingBox",default=False, help="Omit records with a bounding box")
	parser.add_argument("--hb","--hourbefore",    action="store", type=int, dest="Stime", default=-1, help="Hour of day GMT to start search")
	parser.add_argument("--ha","--hourafter",    action="store", type=int, dest="Etime", default=25, help="Hour of day GMT to end search")
	parser.add_argument("--gettapes",    action="store_true", help="Get tape IDs for specified PI or Dives")
	parser.add_argument("--mindepth",    action="store", type=int, default=-1, help="Minimum depth for get tapes")
	parser.add_argument("--maxdepth",    action="store", type=int, default=5000, help="Maximum depth for get tapes")
	parser.add_argument("--video",    action="store_true", help="Get video for queries")
	parser.add_argument("--videofile",    action="store_true", help="Download video files")
	parser.add_argument("--timecode", action="store",default="", help="Get videos matching timecode only")



	parser.add_argument("Args", nargs='*')
	try:
		options = parser.parse_args()
	except ValueError:
		sys.exit(__doc__)
		# parser.print_help(sys.stderr)
		# ("## Unrecognized Argument given...\n" + "\n ".join(sys.argv[1:]))
	# print >> sys.stderr, __doc__
	return options

def execquery(query, mydatabase="M3_ANNOTATIONS",hurry=False,keeplist=False,delimit="\t"):
	from sys import exit
	from base64 import b64decode
	guest='==AdzxEdzFzVXVmT'[::-1]
	guest2='xADMxUjOnJ3bukmchJWbuUmbvlGZ'
	"changing from perseus to venus"
	serverlookup = {
		"EXPD"	 : "perseus.shore.mbari.org",
		"VARS"	 : "perseus.shore.mbari.org",
		"VARS_KB": "venus.shore.mbari.org",
		"M3_ANNOTATIONS" : "venus.shore.mbari.org",
		"M3_VIDEO_ASSETS" : "venus.shore.mbari.org"
	}
	
	if hurry:
		serverlookup["VARS"]="foggy.shore.mbari.org"
	
	if Opt.Flyer:
		servername = 'alaskanwind.wf.mbari.org'
		print >> sys.stderr, "# Using FLYER server..."
		# Note: Now there are tables M3_ANNOTATIONS with view dbo.annotations
		# Also: M3_VIDEO_ASSETS, DOCR_DiveLog
	
	elif Onsite:
		servername = serverlookup[mydatabase]
	else:
		if mydatabase == "EXPD":
			servername = serverlookup[mydatabase]
		else:
			servername = serverlookup[mydatabase]
			# servername = b64decode(guest2[::-1])
	if Opt.DEBUG:
		print >> sys.stderr, "# Using server {} and database {}".format(servername,mydatabase)
	if USEODBC:
		try:
			conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+servername+';DATABASE='+mydatabase+';UID='+loc_red()[0]+';PWD='+ loc_red()[1])
			cur = conn.cursor()
		except pyodbc.Error as f:
			print>>sys.stderr, f
			exit("\n >>> Error: pyodbc failed")
	else:
		try:
			conn = pymssql.connect(servername, loc_red()[0], loc_red()[1], database=mydatabase, login_timeout=7, as_dict=False)
			cur = conn.cursor()
		except pymssql.OperationalError as e:
			print >> sys.stderr, e, "\nPymssql not working."
			exit("\n   >>> Database connection timed out. May require VPN.<<<\n")

	NumRecords=0

	if Opt.DEBUG:
		print >> sys.stderr, "# Starting query...",time.asctime()
		print >> sys.stderr, "QUERY:",query
	cur.execute(query)
	if Opt.DEBUG:
		print >> sys.stderr, "# Finished query...",time.asctime()
	outstr=""
	
	
	# CAREFUL WITH THIS. Reading the cur clears it, so you need to use OutL (loss of efficiency) if you list() it
	# if Opt.DEBUG:
	# 	OutL = list(cur)
	# 	print "# RESULT LIST", OutL
        #
        #  --hurry option does not return the same delimited list
		
	LoL = []
	
	if Opt.DEBUG:
		print >> sys.stderr, "# Parsing query....",time.asctime()
        if Opt.Kml:
            Opt.Hurry = False
		
	for row in cur:
		# if Opt.DEBUG:
		# 	print >> sys.stderr,"#ROW",row
		#### THIS BLOCK ADDED JUNE 2020 to remove duplicate image entries
		# CAUTION: Position-sensitive result	
		if len(row)>8 and not Opt.GetBoth and Opt.Samples:
			if DEBUG:
				print >> sys.stderr, "ROW:",row
			if row[12] and not Opt.GetPNG and '.png' in row[12]:
				continue
			elif row[12] and Opt.GetPNG and '.jpg' in row[12]:
				continue
		if keeplist:
			LoL.append([x for x in row])
			outstr=LoL
		elif not hurry:
			NumRecords += 1
			strlist = ["%s"%x for x in row]
			outstr += delimit.join(strlist)
			outstr += "\n"
		else:
			NumRecords += 1
			try:
				strlist = ["{}".format(x) for x in row]
				# changing this from print to outstr building 
				print delimit.join(strlist)
			except UnicodeEncodeError:
				print >> sys.stderr, "# Warning: Unicode error in parsing results\n# ", row
				# maybe try this instead unicode(x,'utf-8') instead of unicode_escape
#				strlist = [unicode(x).encode('unicode_escape') for x in row]
				strlist = [unicode(x,'utf-8') for x in row]
				print delimit.join(strlist)
			# outstr = ""

	if Opt.DEBUG:
		print >> sys.stderr, "# Formatted query..",time.asctime()

	conn.close()
	return NumRecords,outstr

def simpleexecquery(query, mydatabase="M3_ANNOTATIONS",delimit='\t'):
	'''no overhead, returns a raw list of the results'''
	try:
		import pymssql
		USEODBC=False
	except ImportError:
		import pyodbc
		USEODBC=True

	from base64 import b64decode
	serverlookup = {
	# CHANGING FROM SOLSTICE to PERSEUS
	# Changing from perseus to venus # June 2024
		"EXPD"	 : "perseus.shore.mbari.org",
		"VARS"	 : "perseus.shore.mbari.org",
		"VARS_KB": "venus.shore.mbari.org",
		"M3_ANNOTATIONS" : "venus.shore.mbari.org"
	}	
	
	# grab the right server name, depending on what database is being used...
	un6='ZXZlcnlvbmU='
	pw6='Z3Vlc3Q='
	servername = serverlookup[mydatabase]
	uloc=b64decode(un6)
	ploc=b64decode(pw6)

	if USEODBC:
		try:
			cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+servername+';DATABASE='+mydatabase+';UID='+loc_red()[0]+';PWD='+ loc_red()[1])
       			cur = cnxn.cursor()
		except pyodbc.Error as f:
			print>>sys.stderr, f
			exit("\n >>> Error: pyodbc failed")
	else:
		try:
			conn = pymssql.connect(servername, loc_red()[0], loc_red()[1], database=mydatabase, login_timeout=7, as_dict=False)
			cur = conn.cursor()
		except pymssql.OperationalError as e:
			exit("\n   >>> Database connection timed out. May require VPN.<<<\n")

#	try:	
#		conn = pymssql.connect(servername, uloc, ploc, database=mydatabase, login_timeout=7, as_dict=False)
#	except pymssql.OperationalError as e:
#		print >> sys.stderr, e
#		sys.exit("\n   >>> Database connection timed out. May require VPN.<<<\n")
#	cur = conn.cursor()

	cur.execute(query)
	LoL = []
	for row in cur:
		strlist = ["{}".format(x) for x in row]
		LoL.append( delimit.join(strlist))		
		# LoL.append([x for x in row])
		# outstr=LoL
	conn.close()
	return LoL
	
def loadwaypointsfromfile(): 
	try: 
		from Waypoints import WaypointDict
	except:
		print >> sys.stderr, "Waypoint file not found. Generate with --makewaypoints\nFor example:  vr --makewaypoints Waypoints.csv > ~/python/Waypoints.py"
	return WaypointDict
	
def volttopar(vstring, Raw=False,Dark=0.012,wetcal=3.51e12):
	'''Process PAR sensor data:
	Conversion from voltage to irradiance is # Irradiance = Calibration factor * (10^Light Signal Voltage - 10^Dark Voltage)
## wet calibration factor = 3.51E+12 quanta/cm^2*sec per volt
## dark voltage = 0.012
## Baseline typically 0.0117, but reported only to 3 decimals?
'''
	# disconnected analog channels report 0.005 or 0.006
	if vstring and Raw:
		return "{:.3f}".format(vstring)
	elif vstring and vstring > 0.008:
		# if True:
		# 	print >> sys.stderr, "VSTRING", vstring
		
		Irradiance = wetcal * (10**float(vstring) - 10**Dark)
		return "{:.5e}".format(Irradiance)
	else:
		return "None"

def ptoz(pstring,lat=35):
	'''Convert pressure to depth. 
	From http://www.seabird.com/document/an69-conversion-pressure-depth'''
	import math
	try:
	    p = float(pstring)
	    x = math.sin(lat/57.29578)**2
	    g =  9.780318 * ( 1.0 + ( 5.2788e-3  + 2.36e-5  * x) * x ) + 1.092e-6  * p
	    z = ((((-1.82e-15  * p + 2.279e-10 ) * p - 2.2512e-5 ) * p + 9.72659) * p) / g
	    return "{:.1f}".format(z)
	except TypeError:
		return "NA"


def processKML(StringsFile,IsConcept=0,simple=True):
	'''IsConcept 0 = dives; 1=Concepts; 2=Samples
	TODO: Embed image links 
	TODO: option to only plot one dive pin per organism
	'''

	header = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<kml xmlns=\"http://earth.google.com/kml/2.2\">\n<Document>'
	footer = '</Document>\n</kml>'

	placestring = '''<Placemark>\n  <name>{3}</name>
    <Point>\n <coordinates>{0},{1},-{2}</coordinates>
	</Point>\n</Placemark>'''

	OutPlaces = 0
	lastlabel = ""
	lastps = ""
	StringsList = StringsFile.split("\n")

	print header
	ConceptString = ""
	if Opt.DEBUG:
		print >> sys.stderr, "### Processing {} lines for KML\n".format(len(StringsList))

	for line in StringsList:
		'''split out year?'''

		# for concepts: 
		if (IsConcept==1):
			if Opt.DEBUG:
				print >> sys.stderr, "SampleLine\n",">>",line,"<<"
			if len(line)> 10:
				RovName,DiveNum,Elevation,ConceptName,Lat,Lon,Temperature,Oxygen,Salinity,Transmiss,DiveStartTime,Image,TapeTimeCode,Zoom,ObservationID,LinkValue,LinkName,VideoArchiveSetID,ShipName,Associations,videoArchiveName,CameraDirection,ChiefScientist,FieldWidth,Notes,Observer,ToConcept = line.rstrip("\n").split("\t")
				ConceptString = ConceptName + " - "
			else:
				#print >> sys.stderr, "Skipping line: >>",line,"<<"
				continue
			# for samples:
		elif (IsConcept==2):
			if len(line)> 20:
				DiveStartTime,RovName,DiveNum,ConceptName,Elevation,Temperature,Lat,Lon,TapeTimeCode,ImageLink,SampleRefName = line.rstrip("\n").split("\t")
				ConceptString = ConceptName + "[s] - "
		elif (IsConcept==0):
			ChiefScientist,ShipName,RovName,DiveNum,Lat,Lon,DiveStartTime,Elevation = line.rstrip("\n").split("\t")
		Label = ConceptString + RovName[0].upper()+DiveNum
		Label += ": " + DiveStartTime.split(" ")[0]
		
		ps = placestring.format(Lon,Lat,Elevation,Label)
		if simple:
			if Label != lastlabel:
				OutPlaces += 1				
				print ps
				lastlabel = Label
		elif ps != lastps:
			OutPlaces += 1
			print ps
			lastps = ps

	# except:
	# 	pass
		#print >> sys.stderr, "# Unparsable KML input:",line
	print footer
	print >> sys.stderr, "\n## Output {} KML records...".format(OutPlaces)		

	
def getrovdepth(ROV,ROVnumber):
	ROVletter = ROV[0].upper()
	LongDict = dict(zip(list("TVRDM"),["Tiburon","Ventana","DocRicketts","DocRicketts","MiniROV"]))
	ShortDict = dict(zip(list("TVRDM"),["tibr","vnta","docr","docr","mini"]))
	shortrov = ShortDict[ROVletter]
	longrov  = LongDict[ROVletter]
	
	# NOTE: Some ctds report every 2 seconds and some 2 second...!
	# Syntax for doing it as one query
	# SELECT  EpochSecs,[Depth] FROM VentanaCleanNavData,
	#   (SELECT divestartdtg AS startDate, diveenddtg AS endDateFROM Dive
	#    WHERE rovname = 'vnta' AND divenumber = 3903) AS TimeBounds
	# WHERE DatetimeGMT BETWEEN TimeBounds.startDate AND TimeBounds.endDate
	
		
	TQ = '''SELECT divestartdtg, diveenddtg FROM Dive 
	     WHERE rovname = '{shortrov}' AND divenumber = '{divenum:04d}' '''.format(shortrov=shortrov,divenum=int(ROVnumber))

	if Opt.DEBUG:
		print >> sys.stderr, TQ
		 
	# RUN AN INITIAL QUERY TO GET THE TIMES
	n,tvals = execquery(TQ,mydatabase="EXPD",keeplist=True)
	# Returns time tuple. Need string
	if not tvals:
		print "# NO ENTRY for start times: {}".format(ROV[0].upper()+ROVnumber)
		return 0,[]

	qstart,qend = tvals[0]
	
	# SHOULDN"T BE FASTER BUT FOR SOME REASON IT IS IN THE GUI



	SQ="""SELECT EpochSecs,[Depth] FROM {longrov}{clean}NavData where DatetimeGMT BETWEEN
	'{tstart}' AND '{tend}'"""

	Query = SQ.format(longrov=longrov,clean="Clean",tstart=qstart.strftime(r'%D %r'),tend=qend.strftime(r'%D %r'))
	if Opt.DEBUG:
		print >> sys.stderr, Query
	
	n,ctd = execquery(Query,mydatabase="EXPD",keeplist=True)

	if not ctd:
		Query = SQ.format(longrov=longrov,clean="Raw",tstart=qstart.strftime(r'%D %r'),tend=qend.strftime(r'%D %r'))
		print >> sys.stderr, "## Trying Raw data for {}".format(ROV[0].upper()+ROVnumber)
	n,ctd = execquery(Query,mydatabase="EXPD",keeplist=True)
		
	# raw query return is a tuple like (datetime.datetime(2016, 2, 18, 22, 17, 56), 1455833876, 3.4700000286102295),
	if Opt.DEBUG:
		print >> sys.stderr,  "CTD0  ",ctd[0]
	
	return n,ctd
	
	
def texthistogram(dlist=[],tlist=[],DepthD = {},WindowWidth=50,DiveDict={}):
	"""Given data dictionary or parallel lists of Depth: Count, make a histogram"""
	if DepthD:
		Dtuple = zip(DepthD.values(),DepthD.keys(),)
		Dtuple.sort()
		tlist,dlist = zip(*Dtuple)
	Multiplier = float(WindowWidth) / max(tlist) ## FIXME
	for d,t in sorted(zip(dlist,tlist)):
		print >> sys.stderr,  "{:>6d} : {}|{}".format(d,"=" * int(round(t* Multiplier)),t)
	if DiveDict:
		MasterList = [item for sublist in DiveDict.values() for item in sublist]
		print >> sys.stderr,  "# Collated {} dives with {} values on {}".format(len(MasterList),sum(tlist),time.ctime())

def distance(latlon1,latlon2):
	'''pass a list of [lat,lon],[lat,lon]'''
	return ((latlon1[0] - latlon2[0])**2 + (latlon1[1] - latlon2[1])**2)**0.5

def getwaypointforlatlon(latlon,notes=False):
	wayd = loadwaypointsfromfile()  # do this outside the loop so it is once, and send in?
	distdict = {}
	# use distance as the key?? 
	# get the key of the minimum value of a dict
	#   min(d, key=d.get)
	# Dont worry about ties, or report them all?
	pfields = latlon.split(",")
	if Opt.DEBUG:
		print >> sys.stderr,  "# latlon = opt.waypointname",latlon
		print >> sys.stderr,  "# positionfields",pfields

	position = [float(pfields[0]),float(pfields[1])]
	for wp in wayd.keys():
		distdict[wp] = distance(position,wayd.get(wp))
	minval = min(distdict.values())
	closest = [k for k, v in distdict.items() if v==minval]
	# can return multiples or just one closest
	
	if Opt.DEBUG:
		for kk in sorted(distdict, key=distdict.get):
			print kk, distdict.get(kk)

	return closest  # returns multiple matches
	#return min(distdict, key=distdict.get)   #returns single best match


######################################################################
# TODO FOR WAYPOINTS:
# Output column of waypoint name for dives and concepts and samples
# Create a LUT of waypoint name for dive number
# Constrain concept searches by waypoint name or lat/lon (already?)
#    I think now just does dive table constrained.
######################################################################
def getwaypointbyname(waypointName="",PrintIt=True):
	''' Use about +/- .015 degrees for nearness. Set with epsilon parameter'''
	wayd = loadwaypointsfromfile()  # imports WaypointDict
	matchd = {}
	matchlist = [x for x in wayd.keys() if waypointName in x]
	if Opt.DEBUG:
		print >> sys.stderr,  "WaypointMatches",matchlist
	if (matchlist):
		for wname in matchlist:
			# print "{}\t{}\t{}".format(wname,wayd[wname])
			if PrintIt:
				print "{}\t{}".format(wname,wayd[wname])
			matchd[wname]=wayd[wname]
	else:
		print >> sys.stderr,  "No waypoints found matching",waypointName
	return matchd

def makewaypointfile(Filename=""):
	wd={}
	with open(Filename) as fn:
		for wl in fn:
			wfields=wl.split(",")
			if Opt.DEBUG:
				print >> sys.stderr,  "SplitFields",wfields
			if len(wfields)>2:
				la = float(wfields[1][:-1]) * [1,-1][wfields[1].endswith("S")]
				lo = float(wfields[2][:-1]) * [1,-1][wfields[2].endswith("W")]
				wd[wfields[0]]=[la,lo]
	print "WaypointDict =" , wd
	return

	
def getctdfordive(ROV,ROVnumber,DepthOnly=False,DepthNoTime=False):
	'''Need to add dive info to the front of the data records'''
	
	ROVletter = ROV[0].upper()
	LongDict = dict(zip(list("TVRDM"),["Tiburon","Ventana","DocRicketts","DocRicketts","MiniROV"]))
	ShortDict = dict(zip(list("TVRDM"),["tibr","vnta","docr","docr","mini"]))
	shortrov = ShortDict[ROVletter]
	longrov  = LongDict[ROVletter]
	
	'''Header is RovDive\tDepth\tTimeCTD\tLatitude\tLongitude\tTemp\tSalin\tOxygen\tOxyQual\tTransmiss\tPAR4\tPAR3'''
	# NOTE: Some ctds report every 2 seconds and some 2 second...!
	# Syntax for doing it as one query
	# SELECT  EpochSecs,[Depth] FROM VentanaCleanNavData,
	#   (SELECT divestartdtg AS startDate, diveenddtg AS endDateFROM Dive
	#    WHERE rovname = 'vnta' AND divenumber = 3903) AS TimeBounds
	# WHERE DatetimeGMT BETWEEN TimeBounds.startDate AND TimeBounds.endDate
	
		
	TQ = '''SELECT divestartdtg, diveenddtg FROM Dive 
	     WHERE rovname = '{shortrov}' AND divenumber = '{divenum:04d}' '''.format(shortrov=shortrov,divenum=int(ROVnumber))

	if Opt.DEBUG:
		print >> sys.stderr, TQ
		 
	# RUN AN INITIAL QUERY TO GET THE TIMES
	n,tvals = execquery(TQ,mydatabase="EXPD",keeplist=True)
	# Returns time tuple. Need string
	if not tvals:
		print "# NO ENTRY for start times: {}".format(ROV[0].upper()+ROVnumber)
		return 0,[]

	qstart,qend = tvals[0]
	
	# SHOULDN"T BE FASTER BUT FOR SOME REASON IT IS IN THE GUI
    # Light is really Transmiss. Par is light. Analog 4 may change
	if DepthNoTime:
		SQ = """SELECT p as Pressure 
          FROM {longrov}RovctdData where DatetimeGMT BETWEEN '{tstart}' AND '{tend}' """
	if DepthOnly:
		SQ = """SELECT DatetimeGMT as TimeCTD,  p as Pressure, EpochSecs
          FROM {longrov}RovctdData where DatetimeGMT BETWEEN '{tstart}' AND '{tend}' """
	else:	  
	    SQ = """SELECT DatetimeGMT as TimeCTD, rlat as Latitude, rlon as Longitude, 
          t as Temp, s as Salin, o2 as Oxygen, o2Flag as OxyQual, 
          light as Light, p as Pressure,analog4 as PAR4,analog3 as PAR3 
          FROM {longrov}RovctdData where DatetimeGMT BETWEEN '{tstart}' AND '{tend}' """

	Query = SQ.format(longrov=longrov,tstart=qstart.strftime(r'%D %r'),tend=qend.strftime(r'%D %r'))
	if Opt.DEBUG:
		print >> sys.stderr, Query
	
	
	
	
	'''add dive info here, or with another function'''
	if Opt.DepthOnly or DepthNoTime:
		ctd = execquery(Query,mydatabase="EXPD",keeplist=True)
		# if Opt.DEBUG:
		# 	print >> sys.stderr, ctd[:10][:10]
		return ctd
	else:
		n,ctd = execquery(Query,mydatabase="EXPD",keeplist=True)
		return n,ctd
	
	# END getctdfordive


def getctds(DiveList='',PIList='',Raw=False,DepthOnly=False,delimit="\t"):
	'''take a list of dives or PIs and get the corresponding CTD records'''
	
	SkipList = []
	GoodList = []
	masterCTD = []

	try:
		DiveDict = parsediveoptions(DiveList=DiveList,PIList=PIList)
	except TypeError:
		sys.exit("To generate a normalization matrix you must provide a dive list or PI list with -d or -p")

	try:
		del DiveDict['mini']
	except KeyError:
		pass
		
	starttime=time.ctime()
	print >> sys.stderr, "# ------- Starting queries       {:>25}".format(starttime)

	if DepthOnly:
		headlist = ['RovDive','Depth','TimeCTD','EpochSecs']
	else:
	    headlist = ['RovDive','Depth','TimeCTD','Latitude','Longitude','Temp','Salin','Oxygen','OxyQual','Transmiss','PAR4','PAR3']
	header = delimit.join(headlist)+'\n'
	
	
	for ROV in DiveDict:	
		for DiveNum in DiveDict[ROV]:
			n,CTDdata = getctdfordive(ROV,DiveNum,DepthOnly)
			if CTDdata:
				GoodList.append(ROV[0].upper()+DiveNum)
			else:
				SkipList.append(ROV[0].upper()+DiveNum)
			DiveName = ROV[0].upper() + DiveNum
			# if Opt.DEBUG:
			# 	print >> sys.stderr, CTDdata
			# try option of zip(*ctd)
			# This should only return Date and Pressure from ctd database...
			if DepthOnly:
				newCTD = [ [DiveName] + [ptoz(Line[1])] + [Line[0].strftime('%Y-%m-%d %H:%M:%S')] + ["{}".format(Line[2])]  for Line in CTDdata ]
				print "\n".join([delimit.join(Row) for Row in newCTD])
			else:
				print >> sys.stderr, "# Saving {} ...".format(DiveName)
				# This is sketchy because it is position dependent, but pressure is 2nd to last, and PAR is last
				newCTD = [ [DiveName] + [ptoz(Line[-3])] + ["%s" % Sub for Sub in Line[:-3]]+ [volttopar(Line[-2],Raw)] + [volttopar(Line[-1],Raw)] for Line in CTDdata ]
				# replace with saving to a TSV file. 
				CTDname = "CTD_{}.tsv".format(DiveName)
				with open(CTDname,"w") as ctdfile:
					ctdfile.write(header)
					ctdfile.write("\n".join([delimit.join(Row) for Row in newCTD]))
					ctdfile.write("\n")
			
			

	print >> sys.stderr, "# ------- Wrote {:02} records to files {:>25} using {} Light Values".format(len(GoodList),time.ctime(),["Calibrated","Raw"][Raw])
	if len(SkipList)>0:
		print >> sys.stderr, "# SKIPPED:", ",".join(SkipList)
	
	# END getctdfordive
	
def quantizedepths(ROV,DiveNum,binsize=50):
	""" THIS FUNCTION could be made testable"""
	
	DepthD = {}
	n,ctd = getrovdepth(ROV,DiveNum)
	
	# Round to integer, but do we want to quantize to 10 (50?) meters?
	if len(ctd) < 10:
		print >> sys.stderr,  "# BAD CTD {}".format(ROV+DiveNum)
		if Opt.DEBUG:
			print ctd
		return {None,0,1}
	try:
		Times,Depths = zip(*ctd)
	except ValueError:
		print "# CTD VALUEERROR {}".format(ROV+DiveNum)
		return {None,0,1}
		
	starttime = Times[0] # Is this ever NaN??
	endtime = Times[-1]
	
	if Opt.DEBUG:
		print >> sys.stderr, "DEPTHS",min(Depths),max(Depths)
		print >> sys.stderr, "TIMES", Times[0],'\n',Times[-1]
	if Opt.DEBUG:
		print >> sys.stderr, "Binning data..",time.asctime()

	# Bins so interval is in the middle of the range (100 is 51 to 150)
	# [0, 10, 49, 50, 51, 99, 100, 101, 125, 149, 150, 151]
	# bins
	# [0, 0, 0, 0, 100, 100, 100, 100, 100, 100, 100, 200]
	
	# original
	# binned = [int(y - ((y - 1+ binsize/2.0) % binsize) + binsize/2.0 -1) for y in Depths if (y < 4200)]
	
	# New: 49.9=0, 50.1 = 100, 150.0 = 200
	# [0, 10, 49.9, 50.1, 50.6, 99, 100, 101, 125, 149.9, 150.0, 151]
	# [0, 0, 0, 100, 100, 100, 100, 100, 100, 100, 200, 200]
	# offset by 1
	binned =[int(y - ((y - binsize/2.0) % binsize) + binsize/2.0 ) for y in Depths if (y < 4200)]	
	interval = int(round((endtime - starttime)/float(len(ctd))))
	checkinterval = int(Times[3] - Times[2])
	if interval > 11 or abs(interval - checkinterval)>1:
		print >> sys.stderr,  "Collated Total {}. Diff {} | {}".format(interval,checkinterval,ROV+DiveNum)
		return {None,0,1}	
	
	maxd = max(binned)
	if Opt.DEBUG:
                          # Fri Mar 18 22:22:47 2016  | vnta 2665  766 remain...
		print >> sys.stderr, "#      Interval {}          | {}".format(interval,ROV[0].upper()+DiveNum)

	for d in range(0,maxd+1,binsize):
		DepthD[d] = binned.count(d) * interval
	if Opt.DEBUG:
		print >> sys.stderr,  DepthD
	return DepthD,starttime,endtime
	
def generatenormalization(DiveList='',PIList='',BinSize=10,GetEffort=False,delimit="\t"):
	import time
	SkipList = []
	GoodList = []
	try:
		if Opt.DEBUG:
			print >> sys.stderr, "DiveList in Normalize...\n",DiveList
			print >> sys.stderr, "PI List in Normalize...\n",PIList
		DiveDict = parsediveoptions(DiveList=DiveList,PIList=PIList)
		if Opt.DEBUG:
			print >> sys.stderr, "DiveDict...\n",DiveDict
	except TypeError:
		sys.exit("To generate a normalization matrix you must provide a dive list or PI list with -d or -p")

	try:
		del DiveDict['mini']
	except KeyError:
		pass
		
	starttime=time.ctime()
	if not GetEffort:
		print >> sys.stderr, "# ------- Starting query {}".format(starttime)
	MasterD = {}
	MasterList = [item for sublist in DiveDict.values() for item in sublist]
	LenD = len(MasterList)
	if GetEffort:
		print "DiveNum{0}DiveStart{0}DiveEnd{0}DepthBin{0}Seconds".format(delimit)
	for ROV in DiveDict:	
		for DiveNum in DiveDict[ROV]:
			LenD -= 1
			sys.stderr.write("#  {2}  | {0} {1}  {3} remain...\n".format(ROV,DiveNum,time.ctime(),LenD))
			DepthD,DiveStart,DiveEnd = quantizedepths(ROV,DiveNum,BinSize)
			StartString = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(DiveStart))
			EndString = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(DiveEnd))
			Dname = ROV[0].upper()+DiveNum
			if DepthD:
				GoodList.append(Dname)
				DepthList = sorted(DepthD.keys())
				for D in DepthD:
					MasterD[D] = MasterD.get(D,0) + DepthD[D]
				if GetEffort:
					for D in DepthList:
						print "{1}{0}{2}{0}{3}{0}{4}{0}{5}".format(delimit,Dname,StartString,EndString,D,MasterD.get(D))
					MasterD = {}
			else:
				SkipList.append(Dname)
	if MasterD and not GetEffort:
		texthistogram(DepthD=MasterD,DiveDict = DiveDict)
	if SkipList:
		print >> sys.stderr, "# SKIPLIST:", ",".join(SkipList)
	
	return [MasterD,None][GetEffort]
		
def getmyip(Debug=False):
	'''Unused approach'''
	import socket
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	try:
		s.connect(("8.8.8.8",80))
		ipl = s.getsockname()
		if Debug:
			print >> sys.stderr, ("IP LIST:"),ipl
		ip = ipl[0]
		s.close()
	except socket.error:
		sys.stderr.write("--> No Network, Debug mode <--\n")
		ip = '127.0.0.1'
	if Debug:
		sys.stderr.write("IP ADDRESS: %s\n" % ip)
	return ip,ip.startswith('134.89.')

def getmyip2(Debug=False):
	import os
	import socket
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	try:
		s.connect(("8.8.8.8",80))
		ipl = s.getsockname()
		if Debug:
			print >> sys.stderr, "IP LIST:",ipl
		ip = ipl[0]
		s.close()
	except socket.error:
		sys.stderr.write("--> No Network, Debug mode <--\n")
		ip = '127.0.0.1'
	if Debug:
		sys.stderr.write("IP ADDRESS: %s\n" % ip)
	if os.name == "posix":
		Subp = subprocess.Popen(["ifconfig"], stdout=subprocess.PIPE)
		Result = Subp.stdout.readlines()
		# if Debug:
		# 	print >> sys.stderr,"IFCONFIG:",Result
		if "ppp0" in Result[-4] and ("134.89" in Result[-2] or "134.89" in Result[-3]):
			if Debug:
				sys.stderr.write("VPN Suspected... Trying OnSite\n")
				print >> sys.stderr, "IP", Result[-3].rstrip(),Result[-2].rstrip()
			ip = "134.89.1.1"		
	return ip,ip.startswith('134.89.')
	
def loc_red():
	from base64 import b64decode
	e_lou = 'ZXZlcnlvbmU='
	e_lop = 'Z3Vlc3Q='
		
	return (b64decode(e_lou),b64decode(e_lop))

def querydivesummary(Dive,DEBUG=False):
	'''Importable version: Called from vars_cleanup'''
	import sys
	if DEBUG:
		print >> sys.stderr, "Parsing dive summary for #",Dive,"#"
	shortname,longname,number = parsedive(Dive)

	SQuery = """
	SELECT DISTINCT
		  chiefscientist, shipname, rovname,  divenumber, avgshiplat, avgshiplon, divestartdtg, maxpressure
	FROM  dbo.DiveSummary
	WHERE (RovName like '{0}') AND (DiveNumber IN ('{1:04d}') )
	""".format(shortname,int(number))
	if DEBUG:
		print >> sys.stderr, SQuery
	Result = simpleexecquery(SQuery,mydatabase='EXPD')
	if Result:
		return Result[0]
	else:
		print >> sys.stderr, "No DiveFix: Dive not found", Dive
		return ""

def parsedive(ROV):  # Expects pure sample like D733
	ROVletter = ROV[0].upper()
	number = ROV[1:]
	LongDict = dict(zip(list("TVRDM"),["Tiburon","Ventana","DocRicketts","DocRicketts","MiniROV"]))
	ShortDict = dict(zip(list("TVRDM"),["tibr","vnta","docr","docr","mini"]))
	shortrov = ShortDict[ROVletter]
	longrov  = LongDict[ROVletter]
	return shortrov,longrov,number
	
def parsedivenumbers(Dive,shortname=False):
	Dive = Dive.strip()
	DiveDict = dict(zip(list("TVRDM"),["Tiburon","Ventana","Doc Ricketts","Doc Ricketts","MiniROV"]))
	if shortname:
		DiveDict = dict(zip(list("TVRDM"),["tibr","vnta","docr","docr","mini"]))
	if "-" in Dive:
		DFields = Dive.split("-")
		Dive = DFields[0]
		Sample = DFields[1]
	else:
		Sample = ""
	if Dive[0].isdigit():
		Veh= "%"
		Num = '{:04d}'.format(int(Dive.strip()))
	else:
		Veh = DiveDict[Dive[0].upper()]
		Num = '{:04d}'.format(int(Dive[1:].strip()))
	return Veh,Num,Sample
	
def parsediveoptions(DiveList='',PIList='',Before='12-31-2500',After='1-1-1980',NearLat=30.0, NearLon=-120.0,Epsilon=90.0):
	PreList = False
	if PIList:
		DiveList = getdivebypi(PIList,ReturnThem=True,Before=Before,After=After,NearLat=NearLat,NearLon=NearLon,Epsilon=Epsilon)
		if Opt.DEBUG:
			print >> sys.stderr, "DiveList in parsediveptions...",DiveList
		PreList = True
	elif "," in DiveList:
		if Opt.DEBUG:
			print >> sys.stderr, "DiveList in parsediveptions...",DiveList
		DiveList = DiveList.split(",") 
		# added by SH Apr 2019   Untested
		# DiveList = ['{}{:04d}'.format(x[0],int(x[1:])) for x in PreList]
	else:
		DiveList = [DiveList]
	DiveDict = divelisttodict(DiveList=DiveList,PreList=PreList,Short=True)
	return DiveDict
	

def getdivebypi(PIList,ReturnThem=False,Before='12-31-2500',After='1-1-1980',NearLat=30.0, NearLon=-120.0,Epsilon=90.0):
	""" Num = '{:04d}'.format(int(Dive[1:].strip()))
	    Optional last argument as a digit limits the number of returns"
		To get all dives, but constrained by date, use -p %
		
		TODO: add WHERE (ts.Mission NOT LIKE '%transect%')  to SQuery
		also add filtering by list generated searching on Camera Direction.
		
		Potential Mission edited query: 
		SELECT DISTINCT 
			chiefscientist, shipname, ds.rovname,  ds.divenumber, avgrovlat, avgrovlon, ts.Mission, divestartdtg, maxpressure
			FROM  dbo.DiveSummary as ds, dbo.TapeSummary as ts 
			WHERE (chiefscientist like '%Robison') AND 
			(divestartdtg BETWEEN '12/31/1980' AND '12/31/2500')  
    		AND (ds.rovname = ts.rovname) 
    		AND (ds.divenumber = ts.divenumber) 
			AND (ts.Mission NOT LIKE '%transect%') 
ORDER BY divestartdtg DESC;
			
			SELECT ds.rovname, ds.divenumber from dbo.DiveSummary as ds, dbo.TapeSummary as ts 
				      WHERE (ts.Mission LIKE '%transect%') 
			          and (chiefscientist like '%Robison%')
					  and (ds.rovname = ts.rovname) 
					  and (ds.divenumber = ts.divenumber)
					  ORDER BY ds.rovname, ds.divenumber;
			
		"""
	SQuery = """
SELECT DISTINCT {1}
  chiefscientist, shipname, ds.rovname,  ds.divenumber, avgrovlat, avgrovlon, divestartdtg, maxpressure
  FROM  dbo.DiveSummary as ds, dbo.TapeSummary as ts
  WHERE (chiefscientist like '%{0}') {3} {2}
"""
	#TODO: Add date info?
	
	SHead = "ChiefScientist	ShipName	RovName	DiveNumber	AvgROVLatitude	AvgROVLongitude	DiveStartTime	MaxPressure"
	delimit="\t"
	
	if Opt.Format=='1':
		ReturnThem = True

	if not ReturnThem:
		print SHead
		delimit=DELIMITER
	if "," in PIList:
		PIList = PIList.split(",") 
	else:
		PIList = [PIList]
	Limit = ''
	Tail = ''
	if (Opt.NoTransect):
		Tail += " AND (ds.rovname = ts.rovname) \n AND (ds.divenumber = ts.divenumber)\n AND (ts.Mission NOT LIKE '%transect%') "

	if PIList[-1].isdigit():
		Limit = "TOP " + PIList[-1]
		PIList = PIList[:-1]
		Tail += 'ORDER BY divestartdtg DESC'
		
	DateString = "AND (divestartdtg BETWEEN '{}' AND '{}')".format(After,Before)
	LocString = "AND (avgrovlat BETWEEN '{}' AND '{}') AND (avgrovlon BETWEEN '{}' AND '{}') ".format(NearLat-Epsilon,NearLat+Epsilon,NearLon-Epsilon,NearLon+Epsilon)	
	NumFound = 0
	TotalFound = 0
	DList = []


	for PI in PIList:
		SQ = SQuery.format(PI,Limit,Tail,DateString+LocString)
		if DEBUG:
			print >> sys.stderr, "SQUERY:", SQ
		NumFound,Outstr = execquery(query = SQ, mydatabase="EXPD",delimit=delimit)
		TotalFound += NumFound
		if ReturnThem:
			for Line in Outstr.split('\n'):
				Fields = Line.split("\t")
				if len(Fields)>3:
					if DEBUG:
						print >> sys.stderr, "DiveFields 2:4:",Fields[2:4]
					Divenum = '{:04d}'.format(int(Fields[3].strip()))
					DList.append([Fields[2],Divenum])
		else:
			print Outstr.rstrip("\n")

	if Opt.Format=='1':
		Dout = [ x[0].upper() + y for x,y in DList ]
		print >> sys.stderr, ",".join(Dout)
	if DEBUG:
		print >> sys.stderr, "DLIST:",DList
	if ReturnThem:
		return DList
	sys.stderr.write("## Found %s dives by PI...\n" % (TotalFound))

def gettapebydepth(DiveList='',PIList='',Debug=False,UpperDepth=0,LowerDepth=4000):
	'''return tape uuid, tape name, lat/lon, depth, dive number, date'''
	tapelist=''''''

        if DiveList:
		if "," in DiveList:
			DiveList = DiveList.split(",") 
		else:
		 	DiveList = [DiveList]
		DiveDict = divelisttodict(DiveList=DiveList,PreList=False)
	elif PIList:
		DiveList = getdivebypi(PIList,ReturnThem=True)
		DiveDict = divelisttodict(DiveList=DiveList,PreList=True)
        HeaderQuery = '''select COLUMN_NAME from INFORMATION_SCHEMA.Columns WHERE TABLE_NAME='unique_videos';'''
        Num,HeaderResult = execquery(HeaderQuery,mydatabase='M3_VIDEO_ASSETS')
        Header = "\t".join(HeaderResult.strip().split('\n'))

	if DiveDict:
		for ROV in DiveDict:
			DiveNums =  "','".join(DiveDict[ROV])			
			sys.stderr.write("## Getting tapeIDs from {0} {1}...\n".format(ROV,DiveNums))

                        # round(Depth, -2) as Depth,
                        #(Depth in (SELECT max(Depth) FROM annotations_legacy GROUP BY VideoArchiveSetID_FK))

			SQuery = '''SELECT DISTINCT RovName, DiveNumber, 
			CAST(RecordedDate AS DATE) as MyDate, 
			VideoArchiveID_FK AS videoid,
			VideoArchiveID_FK AS videoID,
			videoArchiveName
			FROM annotations_legacy
			WHERE ( Depth BETWEEN {0} AND {1} ) AND (RovName like '{2}') AND (DiveNumber IN ('{3}')) 
				ORDER BY MyDate ; ''' .format(UpperDepth,LowerDepth,ROV,DiveNums)

                        VQuery = '''SELECT DISTINCT VideoArchiveID_FK FROM annotations_legacy
                        WHERE ( Depth BETWEEN {0} AND {1} ) AND (RovName like '{2}') AND (DiveNumber IN ('{3}')) ;
                                '''.format(UpperDepth,LowerDepth,ROV,DiveNums)

			if DEBUG:
				print >> sys.stderr, VQuery
			Num,Result = execquery(VQuery,mydatabase='M3_ANNOTATIONS')
			if DEBUG:
				print >> sys.stderr, "## Result:", Result
			if Result:
                            uList = ''
                            uList = "','".join(Result.strip().split('\n'))
                            if DEBUG:
                                print >> sys.stderr,"ULIST  ", uList
                            SecondQuery = '''SELECT * from unique_videos WHERE video_reference_uuid IN ('{}') ORDER BY video_sequence_name; '''.format(uList)
                            if DEBUG:
                                print >> sys.stderr,"SQuery",SecondQuery
                            Num,SecondResult = execquery(SecondQuery,mydatabase='M3_VIDEO_ASSETS')
			    tapelist += SecondResult
	else:
		sys.stderr.write("## Provide a PI or divelist to retrieve video archive names.\n")
	print Header
        print tapelist



def distance(latlon1,latlon2):
	'''pass a list of [lat,lon],[lat,lon]'''
	return ((latlon1[0] - latlon2[0])**2 + (latlon1[1] - latlon2[1])**2)**0.5


def divelisttodict(DiveList=[],PreList=False,Short=False):
	DiveDict={}
	RovDict = dict(zip(["tibr","vnta","docr","mini"],["Tiburon","Ventana","Doc Ricketts","MiniROV"]))
	for Dive in DiveList:
		if PreList:
			ShortName,DiveNum = Dive
			if Short:
				ROVName = ShortName
			else:
				ROVName = RovDict[ShortName]
		else:
			ROVName,DiveNum,_ = parsedivenumbers(Dive,shortname=Short)
		DiveDict[ROVName] = DiveDict.get(ROVName,[])+[DiveNum]
	return DiveDict

def getdepthfromctd(Dive, datefield, DEBUG=False):
	'''Importable version: Called from vars_cleanup'''
	import sys
	# from vars_retrieve import simpleexecquery, ptoz
	
	shortname,rovname,number = parsedive(Dive)
	# NOTE: Do we have to use non-binned data? Ventana ends in 2015?
	
	CQ = """SELECT TOP 1 DatetimeGMT, p from {longname}RovctdBinData 
	where DatetimeGMT BETWEEN '{datefield}' AND 
	DATEADD(ss,30,'{datefield}') ORDER BY DatetimeGMT ASC""".format(longname=rovname,datefield=datefield)

	# FOR DEBUGGING:
	# CQ = """SELECT TOP 2 DatetimeGMT, p from
	# {longname}RovctdBinData
	# where DatetimeGMT >= '{datefield}' ORDER BY DatetimeGMT ASC""".format(longname=rovname,datefield=datefield)
		
	resultstr = simpleexecquery(CQ,mydatabase='EXPD')
	if resultstr:
		datestr,depthstr = resultstr[0].split("\t")
	else:
		if DEBUG:
			print >> sys.stderr, "# Cannot Fix depth :\t{}\t{}".format(Dive,datefield)
		return "NaN"
	if DEBUG:
		print >> sys.stderr, "Pressure: {} Depth   : {} \nReturn: {} | Request: {}".format(depthstr,ptoz(depthstr),datestr,datefield)
	return ptoz(depthstr)	

def getparentrank(ranknumber):
	'''Return the name, ID, rank, parentID for an ID number or concept name
	Result looks like this [u'Tentaculata', u'2250', u'class', u'2249']'''
	if ranknumber.isdigit():
		QC = '''SELECT ConceptName from ConceptName where ConceptID_FK = {}'''.format(ranknumber)
		NumRecs, ConName = execquery(QC,mydatabase="VARS_KB")
		QS = '''SELECT id,RankName,ParentConceptID_FK from Concept where id = {}'''.format(ranknumber)
		# RankString has RankName (type) and the parent concept ID. Need name...
	else:
		QS = '''SELECT id,RankName,ParentConceptID_FK from Concept 
		where id  = (Select ConceptID_FK from ConceptName where 
		ConceptName like '{}')'''.format(ranknumber)
		ConName = ranknumber		
	NumRecs, RankString = execquery(QS, mydatabase="VARS_KB")
	RankList = [ConName.strip('\n')] + RankString.strip('\n').split("\t")
	return RankList
	
def getparentconcepts(conceptname,GetRanks=False,style=1):
	QueryString = """WITH org_name AS 
    (
        SELECT DISTINCT parent.id AS parent_id, parentname.ConceptName as parent_name,
            child.id AS child_id, childname.ConceptName as child_name
        FROM
            Concept parent RIGHT OUTER JOIN 
            Concept child ON child.ParentConceptID_FK = parent.id LEFT OUTER JOIN
            ConceptName childname ON childname.ConceptID_FK = child.id LEFT OUTER JOIN
            ConceptName parentname ON parentname.ConceptID_FK = parent.id
        WHERE
            childname.NameType = 'Primary' AND
            parentname.NameType = 'Primary'
    ), jn AS (   
        SELECT parent_id, parent_name, child_id, child_name
        FROM org_name 
        WHERE UPPER(child_name) LIKE UPPER('{}')
        UNION ALL 
            SELECT C.parent_id, C.parent_name, C.child_id,C.child_name 
            FROM jn AS p JOIN org_name AS C ON C.child_id = p.parent_id
    ) 
    SELECT DISTINCT 
        jn.parent_id,
        jn.parent_name
    FROM jn ORDER BY 1;
    """.format(conceptname)
	NumRecs, ParentString = execquery(QueryString, mydatabase="VARS_KB")
	if NumRecs < 1:
		return "# Not found: "
	if Opt.DEBUG:
		print >> sys.stderr, QueryString, "Number", NumRecs
	if GetRanks:
		WholeList = [z.split("\t") for z in ParentString.split("\n") if len(z)>=2]
		Ranks = [[getparentrank(x)[2],y] for x,y in WholeList if x not in ["1","90","91"]]
		return Ranks
	elif style !=1:
		WholeList = [z.split("\t") for z in ParentString.split("\n") if len(z)>=2]
		Parents = [y for x,y in WholeList if x not in ["1","90","91"]]
		if Parents:
			ParentString = ",".join(Parents)
	return ParentString

def getconceptfromKB(higher_taxa,style=1):
	""" Retrieve all subordinate taxonomic concepts from the knowledgebase
 Style 1 (default) is quoted and comma delimited. 
 Style 0 is a list with one concept per line
 Style 2 is the full table with concept numbers and higher-order classes
 """
	ConString = """ parent_name LIKE '%{}%' """
	Splits = higher_taxa.split(",")
	
	if len(Splits) > 1:
		ConList = Splits
	else:
		ConList = [higher_taxa]
	outstr=""
	NumCons = 0
	delimit=["\n",", ",""][style]  # zero gets newline
	for Con in ConList:
		# Capitalize concept search
		# if searching for species, this could be problematic
		Con = Con[0].upper() + Con[1:]
		Constraint = ConString.format(Con.strip())
		# print "CONSTRAINT: ", Constraint
		# Using knowledgebase (VARS_KB), select all the species for a group
				
		Cquery = """
   WITH org_name AS (
        SELECT DISTINCT
            parent.id AS parent_id, parentname.ConceptName as parent_name,
            child.id AS child_id, childname.ConceptName as child_name
        FROM
            Concept parent RIGHT OUTER JOIN 
            Concept child ON child.ParentConceptID_FK = parent.id LEFT OUTER JOIN
            ConceptName childname ON childname.ConceptID_FK = child.id LEFT OUTER JOIN
            ConceptName parentname ON parentname.ConceptID_FK = parent.id
        WHERE
            childname.NameType = 'Primary' AND
            parentname.NameType = 'Primary' ), 
   jn AS (   SELECT            parent_id, parent_name, child_id, child_name FROM org_name 
       WHERE ({}) 
       UNION ALL SELECT C.parent_id, C.parent_name, C.child_id, C.child_name FROM jn AS p 
       JOIN org_name AS C ON C.parent_id = p.child_id ) 
   SELECT DISTINCT jn.parent_id, jn.parent_name, jn.child_id, jn.child_name 
    FROM jn ORDER BY 1;
   """.format(Constraint)
		if Opt.DEBUG:
			print >> sys.stderr, Cquery
		NumRecs, SpeciesList = execquery(Cquery, mydatabase="VARS_KB")
		NumCons += NumRecs
		CheckQuery = "SELECT ConceptID_FK from ConceptName where ConceptName like '{}%';".format(Con)
		NumCheck, IDCheck = execquery(CheckQuery, mydatabase="VARS_KB")
		if NumCheck == 0:
			NumRecs = -1
		sys.stderr.write("## Found %d concepts in hierarchy for query of %s\n"%(NumRecs+1,Con))
		if style == 2:
			outstr = SpeciesList
			break
		try:
			# Get the full name of the query to include it too, e.g. Chrysaora and not just speciesprint
			if style == 1:
				FirstName = "'{}'".format(SpeciesList.split("\t")[1])
				SpNames = [ "'{}'".format(F.split("\t")[3]) for F in SpeciesList.rstrip().split("\n")]
			else:
				FirstName = SpeciesList.split("\t")[1]
				SpNames = [ F.split("\t")[3] for F in SpeciesList.rstrip().split("\n")]
		except IndexError:
			# sys.stderr.write("No lower concepts found for {}\n".format(higher_taxa))
			if style == 1:
				FirstName = "'{}'".format(Con)
			else:
				FirstName = Con
			SpNames = []
			
		if outstr:
			head=delimit
		else:
			head=""
		outstr += head + delimit.join([ FirstName ] + SpNames)

	return NumCons,outstr

# NOTE: EDITED TO GET INSIDE STUFF   CHANGE BACK

def allassociations_INSIDE(DiveList='',PIList='',PreList=False,Style=0,Debug=False):
	'''For supplementing the feeding records, this finds within|contain|containing   '''
	
	SQuery = '''SELECT DISTINCT RovName, DiveNumber, Depth, ConceptName,Latitude, Longitude, 
	Temperature, Oxygen, Salinity, Light as Transmiss,
	RecordedDate AS DateTime24, Image, TapeTimeCode, Zoom,
	ObservationID_FK AS obsid, LinkValue, linkName,
	VideoArchiveID_FK AS videoID, ShipName, Associations,
	videoArchiveName, CameraDirection,
	ChiefScientist, FieldWidth, Replace(Notes,CHAR(10),' '), Observer,ToConcept
	FROM annotations_legacy
	WHERE (ObservationID_FK IN (SELECT ObservationID_FK FROM annotations_legacy WHERE (  
          (( LinkName = 'within' ) OR ( LinkName = 'contains' )  OR ( LinkName = 'containing')) ) )
         {}  )
           ORDER BY DateTime24,TapeTimeCode ; '''

	
	if PIList:
		DiveList = getdivebypi(PIList,ReturnThem=True)
		PreList = True
	elif DiveList and not PreList:
		if "," in DiveList:
			DiveList = DiveList.split(",") 
		else:
		 	DiveList = [DiveList]
	if Debug:
		print >> sys.stderr, "## DiveList:", DiveList
	NumFound = 0
	TotalFound = 0
	AllOut = ""
	if DiveList:
		DiveDict = divelisttodict(DiveList=DiveList,PreList=PreList)
		for ROV in DiveDict:
			DiveNums =  "','".join(DiveDict[ROV])			
			sys.stderr.write("## Getting associations from {0} {1}...\n".format(ROV,DiveNums))
			if not Style:
				print >> sys.stderr, "## SUMMARY MODE. For full output, use -f1 for formatting."
			ExtraString = "AND (RovName like '{}') AND (DiveNumber IN ('{}') ) ".format(ROV,DiveNums)
			SQ = SQuery.format(ExtraString)
			if Debug:
				print >> sys.stderr, "## DIVE Query:\n", SQ
			NumFound, Outstr = execquery(SQ, mydatabase="M3_ANNOTATIONS")
			TotalFound += NumFound
			AllOut += Outstr.replace("/master/","/mezzanine/").replace("_prores.mov","_h264.mp4")
		if TotalFound == 0 and not ('%' in SampleString):
			sys.stderr.write("If unexpected zero results, make sure to use -k for genus only searches\n")
	else:
		SQ = SQuery.format("")
		if Debug:
			print >> sys.stderr, "## NO DIVE Query:", SQ
		TotalFound, AllOut = execquery(SQ, mydatabase="M3_ANNOTATIONS")

	if __name__== "__main__" and Style:
		 print handleheader()

	return TotalFound,AllOut

def allassociations(DiveList='',PIList='',PreList=False,Style=0,Debug=False,Concept=''):
	'''This is the --anela option, to get ALL feeding associations between concepts '''
	
	SQuery = '''SELECT DISTINCT RovName, DiveNumber, Depth, ConceptName,Latitude, 
	Longitude, Temperature, Oxygen, Salinity,Light as Transmiss,
	RecordedDate AS DateTime24, Image, TapeTimeCode, Zoom,
	ObservationID_FK AS obsid, LinkValue, linkName,
	VideoArchiveID_FK AS videoID, ShipName, Associations,
	videoArchiveName, CameraDirection,
	ChiefScientist, FieldWidth, Replace(Notes,CHAR(10),' '), Observer,ToConcept
	FROM annotations_legacy
	WHERE (ObservationID_FK IN (SELECT ObservationID_FK FROM annotations_legacy WHERE (  
          (( LinkName = 'eating' )  OR ( LinkName = 'feeding' ) 
           OR ( LinkName = 'feeding-association-predator-of') 
           OR ( LinkName = 'feeding-association-prey-of')) ) )
         {}  )
           ORDER BY DateTime24,TapeTimeCode ; '''

	
	if PIList:
		DiveList = getdivebypi(PIList,ReturnThem=True)
		PreList = True
	elif DiveList and not PreList:
		if "," in DiveList:
			DiveList = DiveList.split(",") 
		else:
		 	DiveList = [DiveList]
	if Debug:
		print >> sys.stderr, "## DiveList:", DiveList
	NumFound = 0
	TotalFound = 0
	AllOut = ""
	if DiveList:
		DiveDict = divelisttodict(DiveList=DiveList,PreList=PreList)
		for ROV in DiveDict:
			DiveNums =  "','".join(DiveDict[ROV])			
			sys.stderr.write("## Getting associations from {0} {1}...\n".format(ROV,DiveNums))
			if not Style:
				print >> sys.stderr, "## SUMMARY MODE. For full output, use -f1 for formatting."
			ExtraString = "AND (RovName like '{}') AND (DiveNumber IN ('{}') ) ".format(ROV,DiveNums)
			SQ = SQuery.format(ExtraString)
			if Debug:
				print >> sys.stderr, "## DIVE Query:\n", SQ
			NumFound, Outstr = execquery(SQ, mydatabase="M3_ANNOTATIONS")
			TotalFound += NumFound
			AllOut += Outstr
		if TotalFound == 0 and not ('%' in SampleString):
			sys.stderr.write("If unexpected zero results, make sure to use -k for genus only searches\n")
	else:
		SQ = SQuery.format("")
		if Debug:
			print >> sys.stderr, "## NO DIVE Query:", SQ
		TotalFound, AllOut = execquery(SQ, mydatabase="M3_ANNOTATIONS")

	if __name__== "__main__" and Style:
		 print handleheader()

	return TotalFound,AllOut
	
def handleheader(WithNums = False):
	Fields = """RovName\tDiveNumber\tDepth\tConceptName\tLatitude\tLongitude\tTemperature\tOxygen\tSalinity\tTransmiss\tRecordedDate\tImage\tTapeTimeCode\tZoom\tObservationID\tLinkValue\tLinkName\tVideoArchiveSetID\tShipName\tAssociations\tvideoArchiveName\tCameraDirection\tChiefScientist\tFieldWidth\tNotes\tObserver\tToConcept"""
	
	L = Fields.split("\t")
	while '' in L:
		L.remove('')
	if WithNums:
		NumFields = DELIMITER.join(["".join([j,"_",str(i+1)]) for i,j in enumerate(L)])		
	else:
		NumFields = Fields
	return NumFields
		
def findwithcomment(Comment='',Concept = '',DiveList='',PIList='',PreList=False,
           Style=0,Debug=False,Before='12-31-2500',After='1-1-1980',NearLat=30.0, NearLon=-120.0,Epsilon=90.0,BBString=""):
	''' Look for concepts with a particular linked value. To search on all concepts, use -k Eukaryota '''
	
	SQuery = '''SELECT DISTINCT RovName, DiveNumber, Depth, ConceptName,Latitude, Longitude, Temperature, Oxygen, Salinity,Light as Transmiss,
	RecordedDate AS DateTime24, Image, TapeTimeCode, Zoom,
	ObservationID_FK AS obsid, LinkValue, linkName,
	VideoArchiveID_FK AS videoID, ShipName, Associations,
	videoArchiveName, CameraDirection,
	ChiefScientist, FieldWidth, Replace(Notes,CHAR(10),' '),
	Observer,ToConcept
	FROM annotations_legacy
	WHERE ( {concept} {bb} AND (linkName NOT LIKE '%identity-certainty%') AND (ObservationID_FK IN (SELECT ObservationID_FK FROM annotations_legacy WHERE (  
           ((linkName Like '%{comment}%' ) OR ( LinkValue LIKE '%{comment}%' ) or (ToConcept LIKE '%{comment}%') )
         {extra}    ))))
           ORDER BY DateTime24,TapeTimeCode ; '''
	if "," in Concept:
		ConceptString = "(ConceptName IN ({}))".format(Concept)
	else:
		ConceptString = "(ConceptName LIKE '{}')".format(Concept.lower())	
		
	if PIList:
		DiveList = getdivebypi(PIList,ReturnThem=True,Before=Before,After=After,NearLat=NearLat,NearLon=NearLon,Epsilon=Epsilon)
		PreList = True
	elif DiveList and not PreList:
		if "," in DiveList:
			DiveList = DiveList.split(",") 
		else:
		 	DiveList = [DiveList]
	if Debug:
		print >> sys.stderr, "## DiveList:", DiveList
	NumFound = 0
	TotalFound = 0
	AllOut = ""
	if DiveList:
		DiveDict = divelisttodict(DiveList=DiveList,PreList=PreList)
		for ROV in DiveDict:
			DiveNums =  "','".join(DiveDict[ROV])			
			sys.stderr.write("## Getting commented from {0} {1}...\n".format(ROV,DiveNums))
			ExtraString = "AND (RovName like '{}') AND (DiveNumber IN ('{}') ) ".format(ROV,DiveNums)
			SQ = SQuery.format(concept=ConceptString,comment=Comment,extra=ExtraString,bb=BBString)
			if Debug:
				print >> sys.stderr, "## DIVE Query:", SQ
			NumFound, Outstr = execquery(SQ, mydatabase="M3_ANNOTATIONS")
			TotalFound += NumFound
			AllOut += Outstr.replace("/master/","/mezzanine/").replace("_prores.mov","_h264.mp4")
		if TotalFound == 0 and not ('%' in ConceptString):
			sys.stderr.write("If unexpected zero results, make sure to include both a concept and comment\n")
	else:
		SQ = SQuery.format(concept=ConceptString,comment=Comment,extra="",bb=BBString)
		if Debug:
			print >> sys.stderr, "## NO DIVE Query:", SQ
		TotalFound, AllOut = execquery(SQ, mydatabase="M3_ANNOTATIONS")
	# Is this a mistake? Needs a print or to be returned.
	handleheader()
	
	return TotalFound,AllOut
	
def findassociation(conceptstrings,Debug=False,PredatorOnly=False):
	''' Specify two concepts or groups joined with + to see their associations'''
	
	
	if Debug:
		sys.stderr.write("Associating conceptstrings {}\n".format(conceptstrings))
	try:
		HostString,AssocString = conceptstrings.split("+")
		# HostList = HostString.split()
		# AssocList = AssocString.split()
	except ValueError:
		sys.exit("** To search for associations, provide two or more concept lists separated by a plus.\n** Group concepts (e.g., Cnidaria) are OK too")
	
	""" Make your own custom query """
	# print "HostString",  (getconceptfromKB([HostString]))
	# print "AssocString", AssocList
	Outstr=""
	TotalNum=0
	
	
	# This returns a tuple with number and string...
	HostCons = getconceptfromKB(HostString)[1]
	AssocCons = getconceptfromKB(AssocString)[1]
	sys.stderr.write("Finding associations for {0}... \n                    WITH {1}...\n".format(HostCons[:100],AssocCons[:100]))
	
	# Need to search both ways: fish annotated with jelly and jelly annotated with fish...
	# TODO: do "prey of" search on the flipped concepts
	if PredatorOnly:
		AssociationText = """AND ( 
             ( LinkName = 'eating' )  
          OR ( LinkName = 'feeding' ) 
          OR ( LinkName = 'feeding-association-predator-of')
         ) """
		FlipList = [[HostCons,AssocCons]]
	else: 
		# removed          OR ( LinkName = 'feeding-association-predator-of') 
      #   OR ( LinkName = 'feeding-association-prey-of')) 

		AssociationText = """AND ( 
         ann.linkName LIKE '%association%' OR
         (( LinkName = 'eating' )  OR ( LinkName = 'feeding' )  OR ( LinkName = 'feeding-association-predator-of') 
         OR ( LinkName = 'feeding-association-prey-of'))  
         )"""
			
		FlipList = ([HostCons,AssocCons],[AssocCons,HostCons])
			
	for Flipper in FlipList:
		FromCon = """ ann.ConceptName IN ({}) """.format(Flipper[0])
		# ToCon = """ ann.ToConcept IN ({},'Marine organism') """.format(Flipper[1])
		ToCon = """ ( (ann.ToConcept IN ({0}) ) OR (REPLACE(ann.Associations,'-',' ' ) IN ({0}) ) ) """.format(Flipper[1])
		# FromCon = """ ann.ConceptName IN ({}) """.format("'Chrysaora'")
		# ToCon = """ ann.ToConcept IN ({}) """.format("'Doryteuthis opalescens'")
		# Take all args as a string, split on commas (genus species have spaces)
	
		# ADDING DISTINCT AND ORDER BY  Replace png with jpg
		Aquery = """ SELECT DISTINCT
		ann.RovName, ann.DiveNumber,ann.DEPTH, ann.ConceptName,ann.Latitude, 
		ann.Longitude, ann.Temperature, ann.Oxygen, ann.Salinity, ann.Light,
		ann.RecordedDate, ann.Image, ann.TapeTimeCode, ann.Zoom,
		ann.ObservationID_FK AS obsid, ann.LinkValue, ann.linkName,
		ann.VideoArchiveID_FK AS videoID, ann.ShipName,
		ann.Associations,ann.videoArchiveName, ann.CameraDirection,
		ann.ChiefScientist, ann.FieldWidth, REPLACE(ann.Notes,CHAR(10),' '),
		ann.Observer, ann.ToConcept
		FROM annotations_legacy AS ann
		WHERE ( (ann.Image NOT LIKE '%png') AND
		( ({0}) AND ({1}) ) 
		{2}   
		) ;""".format(FromCon,ToCon,AssociationText)
		if Debug:
			# print >>sys.stderr, "QUERY: ",Aquery
		# 	print >> sys.stderr, "FromCon",FromCon
				print >> sys.stderr, "\n## In Flipper...."
		# AND (( ann.linkName LIKE '%association%') OR (ann.linkName LIKE '%commensal%'))  

		NumFound,ResultStr = execquery(Aquery)

		TotalNum += NumFound 
		Outstr += ResultStr.replace("/master/","/mezzanine/").replace("_prores.mov","_h264.mp4")


	NumFields = handleheader() + "\n"
	
	# head = """RovName	DiveNumber	Depth	ConceptName	Latitude	Longitude	Temperature	Oxygen	EpochSecs	Salinity	TTransmiss	RecordedDate	Image	TapeTimeCode	Zoom	ObservationID	LinkValue	linkName	VideoArchiveSetID	ShipName	Associations	videoArchiveName	CameraDirection	ChiefScientist	FieldWidth	Light	Notes	Observer	AssocConcept"""
	# print Fields
	# sys.stderr.write("## Found %d associations for %s...\n" % (TotalNum,conceptstrings))

	return TotalNum, NumFields + Outstr

	
def getsamples(DiveList='',PIList='',Sample='',PreList=False,Debug=False,Super=False,Before='12-31-2500',After='1-1-1980',NearLat=30.0, NearLon=-120.0,Epsilon=90.0):
	# PreList is whether you are passing a list of [veh,name] strings or a string like v3762
	# for epoch secs add this back in: DateDiff(ss, '01/01/70', RecordedDate) AS Esecs,
	# DiveList = DiveListAsString.split(",")
	SQuery = """ SELECT DISTINCT
	      CONVERT(varchar(22), RecordedDate, 120) as DateTime24,
	      RovName, DiveNumber, ConceptName, 
	      Depth, Temperature, Oxygen, Salinity, Light as Transmiss, Latitude, Longitude, TapeTimeCode, 
	      Image, LinkValue 
	FROM  dbo.annotations_legacy
	WHERE ((RovName like '{0}') AND (DiveNumber IN ('{1}') )
	AND ((LinkName LIKE '%sample-reference%') {3}) {2})
	ORDER BY DateTime24,TapeTimeCode ; """
	#sys.stderr.write("Finding samples...\n")
	if PIList:
		DiveList = getdivebypi(PIList,ReturnThem=True,Before=Before,After=After,NearLat=NearLat,NearLon=NearLon,Epsilon=Epsilon)
		
		PreList = True
	elif not PreList:
		if Debug:
			print >> sys.stderr,"Before DiveList",DiveList
		if "," in DiveList:
			DiveList = DiveList.split(",") 
		else:
		 	DiveList = [DiveList]
	NumFound = 0
	TotalFound = 0
	AllOut = ""
	if Debug:
		print >> sys.stderr, "## DiveList:", DiveList

	DiveDict = divelisttodict(DiveList=DiveList,PreList=PreList)
	
	if Debug:
		print >> sys.stderr, DiveDict	
	if Sample:
		if "," in Sample:
			SampleString = "AND ConceptName IN ({})".format(Sample)
		else:
			SampleString = "AND (ConceptName LIKE '{}') ".format(Sample.lower())
	else:
		SampleString = ""
	SuperString = ""
	if Super:
		SuperString = " OR (LinkName LIKE '%%sampled%')"
	SHead = "DateTime24	RovName	DiveNumber	ConceptName	Depth	Temperature	Oxygen	Salinity	Transmiss	Latitude	Longitude	TapeTimeCode	ImageLink	SampleRefName"
	print SHead
	
	for ROV in DiveDict:
		DiveNums =  "','".join(DiveDict[ROV])			
		sys.stderr.write("## Getting samples from {0} '{1}'...\n".format(ROV,DiveNums.replace("'","")))
		SQ = SQuery.format(ROV, DiveNums, SampleString, SuperString)
		if Debug:
			print >> sys.stderr, "## Query:", SQ
		NumFound, Outstr = execquery(SQ, mydatabase="M3_ANNOTATIONS",delimit=DELIMITER)
		TotalFound += NumFound
		AllOut += Outstr
	sys.stderr.write("## Found %d samples for the query...\n" % (TotalFound))
	
	if TotalFound == 0 and not ('%' in SampleString):
		sys.stderr.write("If unexpected zero results, make sure to use -k for genus only searches\n")
	return AllOut

def getsamplesbynumber(DiveList='',Debug=False):
	# PreList is whether you are passing a list of [veh,name] strings or a string like v3762
	# for epoch secs add this back in: DateDiff(ss, '01/01/70', RecordedDate) AS Esecs,
	# DiveList = DiveListAsString.split(",")
	SQuery = """ SELECT DISTINCT
	      CONVERT(varchar(22), RecordedDate, 120) as DateTime24,
	      RovName, DiveNumber, ConceptName, 
	      Depth, Temperature, Oxygen, Salinity, Light, Latitude, Longitude, TapeTimeCode, 
		  Image, LinkValue 
	FROM  dbo.annotations_legacy
	WHERE ((Image NOT LIKE '%png') AND (UPPER(LinkValue) like '{0}') AND (LinkName LIKE '%sample-reference%')) 
	ORDER BY DateTime24,TapeTimeCode ; """
	#sys.stderr.write("Finding samples...\n")
	DiveList = DiveList.replace(" ","")
	if Debug:
		print >> sys.stderr,"Before DiveList",DiveList
	if "," in DiveList:
		DiveList = DiveList.split(",") 
	else:
	 	DiveList = [DiveList]
	NumFound = 0
	TotalFound = 0
	AllOut = ""
	if Debug:
		print >> sys.stderr, "## DiveList:", DiveList
	SHead = "DateTime24	RovName	DiveNumber	ConceptName	Depth	Temperature	Oxygen	Salinity	Transmiss	Latitude	Longitude	TapeTimeCode	ImageLink	SampleRefName"
	print SHead
	print >> sys.stderr, "## Looking for specfic sample numbers:",DiveList
	for SampleNum in DiveList:
		if len(SampleNum) > 4:
			SQ = SQuery.format(SampleNum.upper())
			if Debug:
				print >> sys.stderr, "## Query:", SQ
			NumFound, Outstr = execquery(SQ, mydatabase="M3_ANNOTATIONS",delimit=DELIMITER)
			TotalFound += NumFound
			AllOut += Outstr
	sys.stderr.write("## Found %d samples for the query...\n" % (TotalFound))
	
	return AllOut
	
def getconceptbydive(DiveList='',PIList='',Sample='',PreList=False,Debug=False,Before='12-31-2500',After='1-1-1980',NearLat=30.0, NearLon=-120.0,Epsilon=90.0,BBString="",Stime=-1,Etime=25,minz=-1,maxz=5000):
	# PreList is whether you are passing a list of [veh,name] strings or a string like v3762
	# removed Image, but add back in if you want Image URL
	# for epoch secs add this back in: DateDiff(ss, '01/01/70', RecordedDate) AS Esecs,
	# DiveList = DiveListAsString.split(",")
	
	#  Investigating ann.AnnotationMode, 
	# changing ann.TapeTimeCode to ann.index_recorded_timestamp
	SQuery = """ SELECT DISTINCT ann.RovName, ann.DiveNumber,ann.Depth, ann.ConceptName,
	ann.Latitude, ann.Longitude, ann.Temperature, ann.Oxygen, 
	ann.Salinity,ann.Light as Transmiss,
	ann.RecordedDate AS DateTime24, ann.Image, ann.TapeTimeCode, ann.Zoom,
	ann.ObservationID_FK AS obsid, ann.LinkValue, ann.linkName,
	ann.VideoArchiveID_FK AS videoID, ann.ShipName, ann.Associations,
	ann.videoArchiveName, ann.CameraDirection,
	ann.ChiefScientist, ann.FieldWidth, Replace(Notes,CHAR(10),' '),
	ann.Observer,ann.ToConcept
	FROM annotations_legacy as ann
	WHERE (RovName like '{0}') AND (DiveNumber IN ('{1}') )
	AND {5} {3}
	{2}  {4}
	ORDER BY DateTime24,TapeTimeCode ; """

	## Old query
	# SQuery = """
	# SELECT DISTINCT
	#       CONVERT(varchar(22), RecordedDate, 120) as DateTime24,
	#       RovName, DiveNumber, ConceptName,
	#       Depth, Latitude, Longitude, TapeTimeCode, ISNULL(Observer, '') AS Observer,
	#       LinkValue
	# FROM  dbo.Annotations
	# WHERE (RovName like '{0}') AND (DiveNumber IN ('{1}') )
	# {2}
	# ORDER BY DateTime24,TapeTimeCode ; """

	if PIList:
		DiveList = getdivebypi(PIList,ReturnThem=True,Before=Before,After=After,NearLat=NearLat,NearLon=NearLon,Epsilon=Epsilon)
		PreList = True
	elif not PreList:
		if "," in DiveList:
			DiveList = DiveList.split(",") 
		else:
		 	DiveList = [DiveList]
	NumFound = 0
	TotalFound = 0
	AllOut = ""
	if Debug:
		print >> sys.stderr, "## DiveList:", DiveList

	DiveDict = divelisttodict(DiveList=DiveList,PreList=PreList)
	
	if Debug:
		print >> sys.stderr, DiveDict	
	if Sample:
		if "," in Sample:
			if "'" not in Sample:
				SList = Sample.split(",")
				SampleTextList=["'"+x+"'" for x in SList]
				SampleString = " ConceptName IN ({})".format(",".join(SampleTextList))
			else:
				SampleString = " ConceptName IN ({})".format(Sample)
		else:
			SampleString = " (ConceptName LIKE '{}') ".format(Sample.strip("'").lower())
	else:
		SampleString = ""
	TimeQuery = ""
	if (Stime>0) or (Etime<25):
		TimeQuery = '''(DATEPART(hh,ann.RecordedDate) BETWEEN {} AND {}) AND '''.format(Stime,Etime)

	DepthQuery=""
	if (minz > -1) or (maxz < 5000):
		DepthQuery="(ann.Depth BETWEEN {} AND {}) AND ".format(minz,maxz)

	if not Opt.Kml:
		print handleheader()
	
	for ROV in DiveDict:
		DiveNums =  "','".join(DiveDict[ROV])			
		sys.stderr.write("## Getting concepts from {0} '{1}'...\n".format(ROV,DiveNums.replace("'","")))
		SQ = SQuery.format(ROV, DiveNums, SampleString, TimeQuery, BBString,DepthQuery)
		if Opt.DEBUG:
			print >> sys.stderr, "## Query ROV in DiveDict:", SQ
		NumFound, Outstr = execquery(SQ, mydatabase="M3_ANNOTATIONS",hurry=Opt.Hurry,delimit=DELIMITER)
		TotalFound += NumFound
		AllOut += Outstr.replace("/master/","/mezzanine/").replace("_prores.mov","_h264.mp4")
	sys.stderr.write("## Found %d concepts for the query...\n" % (TotalFound))
	if TotalFound == 0 and not ('%' in SampleString):
		sys.stderr.write("If unexpected zero results, make sure to use -k for genus only searches\n")
	return AllOut
	
	
def getdivesummary(DiveList='',PIList='',Debug=False, PrintResult=True,UseShip=False):
	# removed Image, but add back in if you want Image URL
	# for epoch secs add this back in: DateDiff(ss, '01/01/70', RecordedDate) AS Esecs,
	# requires shortnames?
	
	# THIS VERSION USES AVGROVLAT -- NOT RELIABLE

	SQuery = """
	SELECT DISTINCT
		  chiefscientist, shipname, rovname,  divenumber, avgrovlat, avgrovlon, divestartdtg, maxpressure
	FROM  dbo.DiveSummary
	WHERE (RovName like '{0}') AND (DiveNumber IN ('{1}') )
	"""
	if UseShip:
		SQuery = """
		SELECT DISTINCT
			  chiefscientist, shipname, rovname,  divenumber, avgshiplat, avgshiplon, divestartdtg, maxpressure
		FROM  dbo.DiveSummary
		WHERE (RovName like '{0}') AND (DiveNumber IN ('{1}') )
		"""
	SHead = "ChiefScientist	ShipName	RovName	DiveNumber	AvgROVLatitude	AvgROVLongitude	DiveStartTime	MaxPressure"
	
	DiveDict = parsediveoptions(DiveList=DiveList,PIList=PIList)
	
	NumFound = 0
	TotalFound = 0
	AllOut = ""
	if Debug:
		print >> sys.stderr, "## DiveList:", DiveList


	if PrintResult and not Opt.Kml:
		print SHead
	NumFound = 0
	TotalFound = 0
	AllOut = ''
	for ROV in DiveDict:
		DiveNums =  "','".join(DiveDict[ROV])		
		SQ = SQuery.format(ROV,DiveNums)	
		if Debug:
			print SQ
		NumFound,Outstr = execquery(query = SQ, mydatabase="EXPD")
		TotalFound +=NumFound
		AllOut += Outstr
	if PrintResult:
		sys.stderr.write("## Found %s dive summaries...\n" % (TotalFound))
	return AllOut


def findconcept(conceptstrings,wild=False,hurry=False,Before='12-31-2500',After='1-1-1980',BBString=""):

	if wild:
		ConString = """ ann.ConceptName like '%%%s%%' """
	else:
		ConString = """ ann.ConceptName like '%s' """
	# Take all args as a string, split on commas (genus species have spaces)
	if "," in conceptstrings:
		Splits = conceptstrings.split(",")
		ConList = [ConString % (Con.strip()) for Con in Splits]
		Constraint = "( %s )" % (" OR ".join(ConList))
	else:
		ConList = []
		if conceptstrings:
			Constraint = ConString % (conceptstrings)
		else:
			Constraint = ''
	# Concept = " ".join(sys.argv[1:])
	DateString = "AND (ann.RecordedDate BETWEEN '{}' AND '{}')".format(After,Before)

	if Opt.DEBUG:
		print >> sys.stderr, Constraint
	query = """ SELECT
	ann.RovName, ann.DiveNumber,ann.DEPTH, ann.ConceptName,ann.Latitude, ann.Longitude, 
	ann.Temperature, ann.Oxygen, ann.Salinity, ann.Light,
	ann.RecordedDate, ann.Image, ann.TapeTimeCode, ann.Zoom,
	ann.ObservationID_FK AS obsid, ann.LinkValue, ann.linkName,
	ann.VideoArchiveID_FK AS videoID, ann.ShipName, ann.Associations,
	ann.videoArchiveName, ann.CameraDirection,
	ann.ChiefScientist, ann.FieldWidth, Replace(Notes,CHAR(10),' '),
	ann.Observer,ann.ToConcept
	FROM
	    annotations_legacy AS ann where
	{0} {1} {2} """.format(Constraint,DateString,BBString)
	if not Opt.Kml:
		NumFields = handleheader()	
		print NumFields
	# NumFields = "\t".join(["".join([str(i+1),j]) for i,j in enumerate(L)])
	sys.stderr.write("Finding all annotations for %s...\n" % conceptstrings)
	# head = """RovName	DiveNumber	Depth	ConceptName	Latitude	Longitude	Temperature	Oxygen	EpochSecs	Salinity	Transmiss	RecordedDate	Image	TapeTimeCode	Zoom	ObservationID_FK	LinkValue	linkName	VideoArchiveSetID_FK	ShipName	Associations	videoArchiveName	CameraDirection	ChiefScientist	FieldWidth	Light	Notes	Observer	ToConcept"""
	if Opt.DEBUG:
		sys.stderr.write("QUERY:\n%s\n" % query)
		
	NumFound,Outstr = execquery(query,hurry=hurry,delimit=DELIMITER)
	sys.stderr.write("## Found %d annotations for %s...\n" % (NumFound,conceptstrings))
	if NumFound == 0 and not ('%' in conceptstrings):
		sys.stderr.write("If unexpected zero results, make sure to use -k for genus only searches\n")
		return None
	else:
		return Outstr.replace("/master/","/mezzanine/").replace("_prores.mov","_h264.mp4")
	# if ConList:
	# 	return ConList

def getvideofromresults(Results):
	'''Called from getconceptbydive if Opt.video'''
	import urllib2
	import json
	
	RList = Results.split("\n")
	mediaList = []
	mediaDict = {}
	Break=False
	for L in RList:
		TString=''
		Fields = L.split("\t")
		if len(Fields)>9:
			Platform = Fields[0].replace(" ","%20")
			TimeString = Fields[10]
			if Opt.timecode:
				ot=Opt.timecode
				if ":" in ot:
					InsertTime=ot
				else:
					nt=list(ot)
					nt.insert(4,':')
					nt.insert(2,':')
					InsertTime = "".join(nt)
					
				TimeFields=TimeString.split(" ")
				TimeString = TimeFields[0]+" "+InsertTime
				Break=True
				if Opt.DEBUG:
					print >> sys.stderr, "Got timecode", TimeString
				
				
			if ":" in TimeString:
				TString = TimeString.replace(" ","T")+"Z"
				if Opt.DEBUG:
					print >> sys.stderr, "Tstring\t",TString
				shortTime=TString.split("T")[-1].replace(":","").replace("Z","")

			else:
				shortTime=''
				TString=''
				Break=True
			mediaURL = 'https://m3.shore.mbari.org/vam/v1/media/camera/{}/{}'.format(Platform,TString)
			mediaList.append(mediaURL)
			# Save seek-time for each URL. Multiples if non-unique
			mediaDict[mediaURL]=mediaDict.get(mediaURL,[]) + [shortTime]
		if Break:
			break # out of looping all records

	for k in mediaDict:
		# Flatten redundant timecodes
		mediaDict[k]= list(set(mediaDict[k]))
		if Opt.DEBUG:
			print >> sys.stderr, k,"\t",mediaDict[k]
	masterLinkList = []
	masterLinkD = {}
	if (len(set(mediaList))<55):
		print >> sys.stderr,"Looking up",TString
		for url in mediaList:
			req = urllib2.urlopen(url)
			response = json.loads(req.read())
			# print response
			linklist = []
			for stuff in response:
				pURL = stuff.get('uri','')
				if '.mp4' in pURL:
					masterLinkList.append(pURL)
					masterLinkD[pURL] = masterLinkD.get(pURL,[]) + mediaDict.get(url)
			
	for Item in set(masterLinkList):
		#D421_20120930T192110.699Z_t6s2_hd_tc05160705_h264.mp4
		FileName = Item.split("/")[-1]
		FileStart = FileName.split(".")[0]
		TCodes = sorted(set(masterLinkD.get(Item)))
		AddTC = "-".join(TCodes)
		OutName = FileStart + "+" + AddTC + ".mp4"
		if Opt.DEBUG:
			print >> sys.stderr,Item,OutName
		if Opt.videofile:
			URLtext = "curl -s '{0}' > '{1}'".format(Item,OutName)
			if len(OutName) > 7:
				if Opt.DEBUG:
					print >> sys.stderr,"Getting file",OutName
				ProcOut = subprocess.Popen(URLtext, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,shell=True)
				ProcOut.wait()
				sys.stderr.write(".")


def findconceptvideo(conceptstrings,wild=False,hurry=False,Before='12-31-2500',After='1-1-1980',BBString=""):

	if wild:
		ConString = """ ann.ConceptName like '%%%s%%' """
	else:
		ConString = """ ann.ConceptName like '%s' """
	# Take all args as a string, split on commas (genus species have spaces)
	if "," in conceptstrings:
		Splits = conceptstrings.split(",")
		ConList = [ConString % (Con.strip()) for Con in Splits]
		Constraint = "( %s )" % (" OR ".join(ConList))
	else:
		ConList = []
		if conceptstrings:
			Constraint = ConString % (conceptstrings)
		else:
			Constraint = ''
	# Concept = " ".join(sys.argv[1:])
	DateString = "AND (ann.RecordedDate BETWEEN '{}' AND '{}')".format(After,Before)

	if Opt.DEBUG:
		print >> sys.stderr, Constraint
	query = """ SELECT
	ann.RovName, ann.DiveNumber,ann.DEPTH, ann.ConceptName,ann.Latitude, ann.Longitude, 
	ann.Temperature, ann.Oxygen, ann.Salinity, ann.Light,
	ann.RecordedDate, ann.Image, ann.TapeTimeCode, ann.Zoom,
	ann.ObservationID_FK AS obsid, ann.LinkValue, ann.linkName,
	ann.VideoArchiveID_FK AS videoID, ann.ShipName, ann.Associations,
	ann.videoArchiveName, ann.CameraDirection,
	ann.ChiefScientist, ann.FieldWidth, Replace(Notes,CHAR(10),' '),
	ann.Observer,ann.ToConcept,ann.preview_media_index_seconds
	FROM
	    annotations_legacy AS ann where
	{0} {1} {2} """.format(Constraint,DateString,BBString)
	if not Opt.Kml:
		NumFields = handleheader()	
		print NumFields
	# NumFields = "\t".join(["".join([str(i+1),j]) for i,j in enumerate(L)])
	sys.stderr.write("Finding all annotations for %s...\n" % conceptstrings)
	# head = """RovName	DiveNumber	Depth	ConceptName	Latitude	Longitude	Temperature	Oxygen	EpochSecs	Salinity	Transmiss	RecordedDate	Image	TapeTimeCode	Zoom	ObservationID_FK	LinkValue	linkName	VideoArchiveSetID_FK	ShipName	Associations	videoArchiveName	CameraDirection	ChiefScientist	FieldWidth	Light	Notes	Observer	ToConcept"""
	if Opt.DEBUG:
		sys.stderr.write("QUERY:\n%s\n" % query)
		
	NumFound,Outstr = execquery(query,hurry=hurry,delimit=DELIMITER)
	sys.stderr.write("## Found %d annotations for %s...\n" % (NumFound,conceptstrings))
	if NumFound == 0 and not ('%' in conceptstrings):
		sys.stderr.write("If unexpected zero results, make sure to use -k for genus only searches\n")
		return None
	else:
		return Outstr.replace("/master/","/mezzanine/").replace("_prores.mov","_h264.mp4")
	# if ConList:
	# 	return ConList

def checktransect():
	'''TBD: either check if a list of dive numbers is transects, 
	or return transect status for a list of dives'''	
	
	'''Probably good to have this take a divedict as input and return the sanitized list w/o transect dives'''

	# TWO possible queries to retrieve (or test?) transcripts.
	
	# DList is in the format:  [[u'docr', u'1138'], [u'docr', u'1137'], [u'vnta', u'4186']]
	TQuery = """SELECT DISTINCT RovName, DiveNumber, CameraDirection, ChiefScientist
	FROM annotations_legacy WHERE (  (CameraDirection LIKE 'transect%' )  AND (ChiefScientist LIKE '%Robison%') )
	ORDER BY RovName, DiveNumber;"""
	
	# ADD Robison below
	DBQuery = """SELECT ds.rovname, ds.divenumber from dbo.DiveSummary as ds, dbo.TapeSummary as ts 
	      WHERE (ts.Mission LIKE '%transect%') 
		  and (ds.rovname = ts.rovname) 
		  and (ds.divenumber = ts.divenumber)
		  ORDER BY ds.rovname, ds.divenumber;"""
	return None
   # return one list merging these dives 

def getimages(listOnly=False,minz=-1,maxz=5000,getConcept=False,dive=False,png=False,both=False,wild=False,
comment='',Before='12-31-2500',After='1-1-1980',SampleString='',Assoc=False,flyer=False,BBString="",Stime=-1,Etime=25):
	imagecount = 0
	bb=BBString
	Con = ""
	if SampleString:
		NewLines = []
		Lines = SampleString.split("\n")
		Extensions = ['jpg','JPG']
		if png:
			Extensions = ['png','PNG']
		if both:
			Extensions = ['jpg','JPG','png','PNG']
		
		if Assoc:
			# Associations
			# Veh,Dive,Con,URL,Feed2
			# 0,1,3,11,26
			for L in Lines:
				Fields = L.split("\t")
				if 30 > len(Fields) > 25:
					if DEBUG:
						print >> sys.stderr, len(Fields)
				# CAUTION This uses a position-based entry
					Veh,Dive,Con,URL,Prey = [ Fields [ind] for ind in [0,1,3,11,26]]
					Con2 = Con + "+" + Prey
					Combined = [Veh,Dive,Con2,URL]
					#target Vehicle,Dive,Con2,URL
					if URL[-3:] in Extensions:
						NewLines.append("\t".join(Combined))
						imagecount +=1
			Outstr = "\n".join(NewLines)	
		else:  # Sample records
		# CAUTION This uses a position-based entry
			for L in Lines:
				Fields = L.split("\t")
				# if DEBUG:
				# 	print >> sys.stderr, "Before Fields",Fields
				# THIS should be only Veh,Dive,Concept,Url
				del Fields[4:12]
				if DEBUG:
					print >> sys.stderr, "After Fields",Fields
				NewLines.append("\t".join(Fields[1:-1]))
			Outstr = "\n".join(NewLines)
			if DEBUG:
				print >> sys.stderr, "OutStr for samples",Outstr

		
	
	else:
		if comment:
			CommentString = "{bb} AND ((LinkName Like '%{comment}%' ) OR ( LinkValue LIKE '%{comment}%' ) or (ToConcept LIKE '%{comment}%') )".format(comment=comment,bb=BBString)
		else:
			CommentString = "{bb}".format(bb=BBString)
		
		DiveJoin = ""
	
		# TODO  Need to fix for multiple dives
		# if dive:
		# 	if  "," in dive:
		# 		DiveList = dive.split(",")
		# 	else:
		# 		DiveList = [dive]
		Constraint = ""
		if dive:
			Veh,Num,Sam = parsedivenumbers(dive)
			Constraint = """ (ann.RovName like '%%{0}%%') AND (ann.DiveNumber = {1:04d}) {2}""".format(Veh,int(Num),CommentString)
			DiveJoin = "AND "
	
		Splits = []

		SuffixString = "((ann.Image LIKE '%%.jpg') OR (ann.Image LIKE '%%.JPG') ) AND "
		if png:
			SuffixString = "((ann.Image LIKE '%%.png') OR (ann.Image LIKE '%%.PNG') ) AND "
		if both:
			SuffixString = ""
		TimeQuery = ""	
		if (Etime < 25) or (Stime > -1):
			TimeQuery = '''AND  (DATEPART(hh,ann.RecordedDate) BETWEEN {} AND {})'''.format(Stime,Etime)

		DepthQuery=""
  		if (minz > -1) or (maxz < 5000):
			DepthQuery="(ann.Depth BETWEEN {} AND {}) AND ".format(minz,maxz)

		if getConcept:
			if wild:
				ConString = """ ann.ConceptName like '%%%s%%' """
			else:
				ConString = """ ann.ConceptName like '%s' """
			if "," in getConcept:
				Splits = getConcept.split(",")
				ConList = [ConString % (Con.strip().replace("'","")) for Con in Splits]
				Con = "( %s )" % (" OR ".join(ConList))
			else:
				Con = ConString % (getConcept)

			Constraint += DiveJoin + Con
			# Concept = " ".join(sys.argv[1:])
			if Opt.DEBUG: 
				print >> sys.stderr, "Constraint: ",Constraint
			# instead of limit use TOP 100
	
	  # REORDERING FIELDS 
		
		query = """ SELECT DISTINCT
		ann.RovName,ann.DiveNumber,ann.Depth,ann.RecordedDate,ann.ConceptName,ann.Image
		FROM
		annotations_legacy AS ann
		WHERE {0} {4}
		{1} {2} {3}""".format(SuffixString,Constraint,TimeQuery,CommentString,DepthQuery)
	
		if Opt.DEBUG:
			print >> sys.stderr, query
		Fields = """Vehicle\tDiveNumber\tDepth\tDate\tConcept\tImage"""

		NumFound,Outstr = execquery(query)

		if not listOnly:
			sys.stderr.write("Finding all images for Concept:%s and Dives:%s...\n" % (getConcept,dive))
		else:
			print >> sys.stderr, "# Only listing image URLs"
	
	
	# TODO: Retrieve from associations — provide Outstr
	
	for S in Outstr.split('\n'):
		if '\t' in S and not ((S.split('\t')[-2] == 'None') or (S.split('\t')[-2] == '')):
			imagecount+=1
			if listOnly and not flyer:
				print S.replace('framegrabs','frameGrabs').replace('%20','')
			else:
				if Opt.DEBUG:
					print >> sys.stderr, S
                                try:
                                    Vehicle,DiveNum,Depth,DateTime,Con2,URL = S.split("\t")
                                except ValueError:
                                    Vehicle,DiveNum,Depth,Con2,URL = S.split("\t")
				# This line is obsolete. New database lists both PNG and JPEG Urls...				
				# if png:
				# 	URL = URL.replace(".jpg",'.png').replace('.JPG','.PNG')
				
				FileTag = Vehicle[0]+DiveNum
				Fname = URL.split('/')[-1]
				if "-" in Fname:
					Fname = URL.split('/')[-1].split("-")[0] + "." + URL.split(".")[-1]
				
				# URL MOVED. Now: 
				# http://search.mbari.org/ARCHIVE/frameGrabs/DocRicketts/images/0536/04_18_58_14.jpg
				# OLD:
				# http://search.mbari.org/ARCHIVE/framegrabs/Doc%20Ricketts/images/0536/04_19_02_05.jpg
				if not flyer:
					URL = URL.replace('framegrabs','frameGrabs').replace('%20','')
				if "file:" in URL:
					print >> sys.stderr, "## local image URL found on Flyer. Check script for proper folder"
					'''file:/Users/docricketts/.vars/images/DocRicketts/images/0914/06_39_50_12.jpg
					http://fww.wf.mbari.org/DocRicketts/images/'''
					URL = URL.replace('file:/Users/docricketts/.vars/images/DocRicketts','http://fww.wf.mbari.org/DocRicketts')
				URLtext = "curl -s '{0}' > '{1}-{2}-{3}'".format(URL,Con2.replace(" ","_"),FileTag,Fname)
				if len(Fname) > 7:
					if Opt.DEBUG:
						print >> sys.stderr, URLtext
					ProcOut = subprocess.Popen(URLtext, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,shell=True)
					ProcOut.wait()
					sys.stderr.write(".")
	
	# if not listOnly and not SampleString:
	# 	try:
	# 		divestr = "on "+ Veh[0]+"-"+Num
	# 	except NameError:
	# 		divestr = ""
	if not listOnly:
		imageformat= "JPEG "
		if png:
			imageformat="PNG "
		if both:
			imageformat=""
		sys.stderr.write("\n## Found %d %simages\n" % (imagecount,imageformat))
		
	if imagecount == 0 and not listOnly and not ('%' in Con):
		sys.stderr.write("If unexpected zero results, make sure to use -k for genus only searches\n")

def printranktable(ranklist,Head=True):
	RankD = dict(ranklist)
	PrintRanks = ['phylum','class','order','family','genus']
	if Head:
		print DELIMITER.join(PrintRanks)
	RankFields = [RankD.get(PR,'None') for PR in PrintRanks]
	print DELIMITER.join(RankFields)
		
def getbiolum(getConcept='',style=0):
	
	ConString = "cn.ConceptName like '%s'"
	Splits = []
	if getConcept:
		if "," in getConcept:
			Splits = getConcept.split(",")
			ConList = [ConString % (Con.strip().replace("'","")) for Con in Splits]
			Con = "AND ( %s )" % (" OR ".join(ConList))
		else:
			Con = "AND " + ConString % (getConcept.strip().replace("'",""))
			Splits = [getConcept]
	else:   # GET FULL LIST
		Con = ''
	
	
	BQ = '''SELECT cn.ConceptName, lr.LinkValue AS link
	FROM
	 Concept AS c LEFT JOIN
	 ConceptName AS cn ON cn.ConceptID_FK = c.id LEFT JOIN
	 ConceptDelegate AS cd ON cd.ConceptID_FK = c.id LEFT JOIN
	 LinkRealization AS lr ON lr.ConceptDelegateID_FK = cd.id 
	WHERE LinkName = 'is-bioluminescent' {constring}
	ORDER BY cn.ConceptName'''.format(constring=Con)

	if Opt.DEBUG:
		print >> sys.stderr, BQ
	n,outlist = execquery(BQ,mydatabase="VARS_KB",keeplist=True)
	
	
	if style:
		print "BiolumDict = {"
		for con,bio in outlist:
			print "'{}' : '{}',".format(con.replace("'","\\'"),bio)
		print "'NA':'uncertain' }  # End BiolumDict"
		
	else:
		if Splits:  # Concepts were provided to search on
			cd = dict(outlist)
			for rawcon in Splits:
				con = rawcon.replace("'","").strip()
				print "{}\t{}".format(con,cd.get(con,"NA"))
		else:# No concepts, so returning whole database
			for con,bio in outlist:
				print "{}\t{}".format(con,bio)


def summarizeassoc(instring):
	""" Split the associations to a subset of pairs
	    This is fragile. It uses positional indices for values.
	    If something is wrong in findassociation check here.
	"""

	allstr = instring.split("\n")[1:]
	OutSet = {}
	for Line in allstr:
		Values = Line.split("\t")
		if len(Values)>25:
			OutKey = Values[3].replace("Hyperiidea","Hyperiidae") + " + " + Values[26].replace("Hyperiidea","Hyperiidae")
			OutSet[OutKey] = OutSet.get(OutKey,0) + 1
	OK = sorted(OutSet.keys())
	for k in OK:
		print "%8d  %s" % (OutSet[k],k)



### START OF PROGRAM
	
def main():
	
	CarryOn = True
	CommentString = ""
	SampleString = ""
	if len(sys.argv) <2:
		sys.exit(__doc__)
	if Opt.Version or Opt.DEBUG:
		versionnum = __doc__.split('\n')[1].split(":")[0].strip().replace("v","vars_retrieve.py Version:")
		print >> sys.stderr, "%%%%%%%%%%\n","% "+ versionnum, "\n%%%%%%%%%%%\n"
	if Opt.header:
		sys.exit(handleheader(True))

	if Opt.Waypointfile:
		sys.exit(makewaypointfile(Opt.Waypointfile))

	if Opt.waypointname:
		sys.exit(getwaypointforlatlon(Opt.waypointname))
	
	# Only get video archive names
	if Opt.gettapes:
		print gettapebydepth(DiveList=Opt.Dives,PIList=Opt.Pis,Debug=Opt.DEBUG,UpperDepth=Opt.mindepth,LowerDepth=Opt.maxdepth)
                sys.exit()

	if Opt.waypoint:
		'''matches substring'''
		getwaypointbyname(Opt.waypoint)
	
	if Opt.Simple:
		Opt.Kml = True

	# Default values
	NearLat = 30
	NearLon = -120
	Epsilon = 90
	if Opt.Stime > Opt.Etime:
		Opt.Stime,Opt.Etime = Opt.Etime,Opt.Stime

	if Opt.near:
		'''Work in progress'''
		if not Opt.Pis:
			sys.stderr.write("\n## No PI specified for --near: searching all PIs...\n")
			Opt.Pis = "%"
		if "," in Opt.near:
			try:
				fields=Opt.near.split(",")
				NearLat = float(fields[0])
				NearLon = float(fields[1])
				Epsilon = float(Opt.epsilon)
				print >> sys.stderr,"# Searching near {} {} ± {}. Specify distance (degrees) with --epsilon\n".format(NearLat,NearLon,Epsilon)

			except:
				sys.exit("To supply lat/lon for --near, use decimal values separated by commas")
		else:
			wd = getwaypointbyname(Opt.near,PrintIt=False)
			if len(wd.keys())>1:
				print >> sys.stderr,"Too many matches to station string. Specify one\n",wd.keys()
			elif len(wd.keys()) ==1:
				station = wd.keys()[0]
				NearLat = wd.get(station)[0]
				NearLon = wd.get(station)[1]
				Epsilon = float(Opt.epsilon)
				print >> sys.stderr,"Searching near station {} ± {}. Specify distance with --epsilon".format(station,Epsilon)
		if Opt.DEBUG:
			print >> sys.stderr,"Lat,Lon,Epsilon:",NearLat,NearLat,Epsilon

	if Opt.BoundingBox:
		BBString = "AND (LinkName NOT LIKE 'bounding box') "
	else:
		BBString = ""

	if Opt.DepthOnly:
		Opt.CTD = True		
	if Opt.Hurry:
		print >> sys.stderr, "# Using fog backup server for M3_ANNOTATIONS. Not sure if this makes any difference"		
	if Opt.Biolum:
		if Opt.Knowledge:
			Opt.Concept = getconceptfromKB(Opt.Knowledge)[1]
		getbiolum(Opt.Concept,style=Opt.Format)
		CarryOn=False
	if Opt.GetPNG or Opt.GetBoth:
		Opt.GetImages = True
	if Opt.Kml:

		if not Opt.Dives:
			if Opt.Args:
				Opt.Dives = ",".join(Opt.Args)
		elif Opt.Args:
			Opt.Dives = Opt.Dives + ","+",".join(Opt.Args)
		if Opt.DEBUG:
			print >> sys.stderr, "Found dives for KML", Opt.Dives


	if Opt.Anela:
		style = Opt.Format
		assocnum, assocstr = allassociations(DiveList=Opt.Dives,PIList=Opt.Pis,PreList=False,Style=Opt.Format,Debug=Opt.DEBUG)
		if style:
			print assocstr
		else:
			summarizeassoc(assocstr)
		sys.stderr.write("\n## Found %d feeding annotations...\n" % (assocnum ))
		CarryOn=False

	if Opt.Normalize or Opt.GetEffort or Opt.BinSize != 10:
		if not Opt.Dives:
			if Opt.Args:
				Opt.Dives = ",".join(Opt.Args)
				print >> sys.stderr, "DiveArgs",Opt.Dives
		elif Opt.Args:
			Opt.Dives = Opt.Dives + ","+",".join(Opt.Args)
			if Opt.DEBUG:
				print >> sys.stderr, "Found dives as Args", Opt.Dives
		DepthDictionary = generatenormalization(DiveList=Opt.Dives,PIList=Opt.Pis,
		      BinSize=Opt.BinSize,GetEffort=Opt.GetEffort,delimit=DELIMITER)
		if not Opt.GetEffort:
			Dkeys = DepthDictionary.keys()
			Dkeys.sort()
			print"Depth{}Seconds".format(DELIMITER)
			for D in Dkeys:
				print "{}{}{}".format(D,DELIMITER,DepthDictionary.get(D))
		CarryOn=False
	if Opt.CTD or Opt.Raw:
		if not Opt.Dives:
			if Opt.Args:
				Opt.Dives = ",".join(Opt.Args)
				print >> sys.stderr, "DiveArgs",Opt.Dives
		elif Opt.Args:
			Opt.Dives = Opt.Dives + ","+",".join(Opt.Args)
			if Opt.DEBUG:
				print >> sys.stderr, "Found dives as Args", Opt.Dives
		getctds(DiveList=Opt.Dives,PIList=Opt.Pis,Raw=Opt.Raw,DepthOnly=Opt.DepthOnly,delimit=DELIMITER)
		CarryOn=False

	elif Opt.Comment and not Opt.GetImages:
		if not (Opt.Concept) and not(Opt.Knowledge):
			sys.exit("Comment searches require a concept -c or Category -k also.")
	
		if Opt.Knowledge:
			Opt.Concept = getconceptfromKB(Opt.Knowledge)[1]
		if Opt.DEBUG:
			print >> sys.stderr, "CONCEPT",Opt.Concept
		if not Opt.GetImages:
			assocnum, assocstr = findwithcomment(Comment=Opt.Comment,Concept=Opt.Concept,DiveList=Opt.Dives, PIList=Opt.Pis,PreList=False,Style=Opt.Format,Debug=Opt.DEBUG,Before=Opt.Before,After=Opt.After,BBString=BBString)
			if not Opt.Kml:
				print handleheader()
				print assocstr
			else:
				processKML(assocstr,IsConcept=1,simple=Opt.Simple)
			sys.stderr.write("\n## Found %d annotations for '%s' with comment '%s'...\n" % (assocnum,Opt.Concept,Opt.Comment))
			CarryOn=False
	elif Opt.UpConcept:
		style=int(Opt.Format)
		if Opt.GetRank:
			RankList = getparentconcepts(Opt.UpConcept,Opt.GetRank,style=style)
			LastRank = getparentrank(Opt.UpConcept)
			if len(LastRank)>2:
				RankList.append([LastRank[2],LastRank[0]])  # add the rank and value of the orig search
				printranktable(RankList)
			
		else:
			# use test and don't append original query if it contained %
			print getparentconcepts(Opt.UpConcept,Opt.GetRank,style=style) + ["," + Opt.UpConcept,""]["%" in Opt.UpConcept]
		CarryOn=False
	elif Opt.GetRank:
		# Fixme later to search up on -k ...
		# if Opt.Knowledge or Opt.Concept:
		# 	Opt.Args = [Opt.Knowledge] + [Opt.Concept]
		# 	Opt.Args.remove(None)
		# 	print Opt.Args
		print "Name\tID\tRank\tParent"
		for Con in Opt.Args:
			print "\t".join(getparentrank(Con))
		CarryOn=False
	elif Opt.GetDepth: # flawed?
		print getrovdepth(Opt.Args[0])
		CarryOn=False
	elif Opt.Samples or Opt.Supersample or Opt.SampleNums:
		if not Opt.Dives:
			if Opt.Args:
				Opt.Dives = ",".join(Opt.Args)
				print >> sys.stderr, "DiveArgs",Opt.Dives
		elif Opt.Args:
			Opt.Dives = Opt.Dives + ","+",".join(Opt.Args)
			if Opt.DEBUG:
				print >> sys.stderr, "Found dives as Args", Opt.Dives
		if Opt.SampleNums:
			print(getsamplesbynumber(DiveList=Opt.Dives,Debug=Opt.DEBUG))
			CarryOn=False
		else:	
			if Opt.Knowledge:
				Opt.Concept = getconceptfromKB(Opt.Knowledge)[1]
				if Opt.DEBUG:
					print >> sys.stderr, Opt.Concept
			if Opt.Kml:
				processKML(getsamples(DiveList=Opt.Dives, PIList=Opt.Pis, Sample=Opt.Concept, PreList=False, 
						Debug=Opt.DEBUG,Super=Opt.Supersample,Before=Opt.Before,After=Opt.After),IsConcept=2,simple=Opt.Simple)
			else:
				SampleString = (getsamples(DiveList=Opt.Dives, PIList=Opt.Pis, Sample=Opt.Concept, PreList=False, 
						Debug=Opt.DEBUG,Super=Opt.Supersample,Before=Opt.Before,After=Opt.After))
				print SampleString
			CarryOn = False
			
	if (Opt.ListImages or Opt.GetImages) and not Opt.Association:
		# FIX THIS wrt IMAGES...!!!!!!
		# First retrieve dives and then get the images after
		Strings=""
		if SampleString:
			getimages(listOnly=Opt.ListImages,minz=Opt.mindepth,maxz=Opt.maxdepth,SampleString=SampleString,flyer=Opt.Flyer,BBString=BBString,Stime=Opt.Stime,Etime=Opt.Etime)
		else:
			if Opt.Knowledge:
				Opt.Concept = getconceptfromKB(Opt.Knowledge)[1]
			if Opt.Dives or Opt.Pis:
				if Opt.Pis:
					DiveList = getdivebypi(Opt.Pis,ReturnThem=True,Before=Opt.Before,After=Opt.After)
					DiveStringList = [A[0].upper() + B for A,B in DiveList]
					if Opt.DEBUG:
						print >> sys.stderr, "DiveListFromPi",DiveStringList
					Opt.Dives = ",".join(DiveStringList)
				if Opt.Args:
					Strings = Opt.Dives + ","+",".join(Opt.Args)
				else:
					Strings = Opt.Dives
				if Opt.DEBUG:
					print >> sys.stderr, "\n## Divestring:",Strings
				for D in Strings.split(","):
					getimages(listOnly=Opt.ListImages,minz=Opt.mindepth,maxz=Opt.maxdepth,getConcept=Opt.Concept,dive=D,png=Opt.GetPNG,
 both=Opt.GetBoth,wild=Opt.Wildcard,comment=Opt.Comment,
 Before=Opt.Before,After=Opt.After,flyer=Opt.Flyer,BBString=BBString,
 Stime=Opt.Stime,Etime=Opt.Etime)
			else:
				if Opt.DEBUG:
					print >> sys.stderr, "# No Dive or PI List"
				getimages(listOnly=Opt.ListImages,minz=Opt.mindepth,maxz=Opt.maxdepth,getConcept=Opt.Concept,dive=Opt.Dives,png=Opt.GetPNG,
 both=Opt.GetBoth,wild=Opt.Wildcard,comment=Opt.Comment,Before=Opt.Before,After=Opt.After,flyer=Opt.Flyer,
 BBString=BBString,Stime=Opt.Stime,Etime=Opt.Etime)
		CarryOn = False
	elif Opt.Pis and CarryOn:
		if not (Opt.Knowledge or Opt.Concept or Opt.Kml):
			if Opt.DEBUG:
				print >> sys.stderr, "# NearLat,NearLon", NearLat,NearLon,Epsilon
			getdivebypi(Opt.Pis,ReturnThem=False,Before=Opt.Before,After=Opt.After,NearLat=NearLat,NearLon=NearLon,Epsilon=Epsilon) # return for internal script use, or print (default)
		else:
			DiveList = getdivebypi(Opt.Pis,ReturnThem=True,Before=Opt.Before,After=Opt.After,NearLat=NearLat,NearLon=NearLon,Epsilon=Epsilon)
			DiveStringList = [A[0].upper() + B for A,B in DiveList]
			if Opt.DEBUG:
				print >> sys.stderr, "DiveListFromPi:",DiveStringList
			Opt.Dives = ",".join(DiveStringList)
			CarryOn = True
	if CarryOn and Opt.Dives:
		if Opt.Args:
			Strings = Opt.Dives + ","+",".join(Opt.Args)  # this might lead to double entries - check
		else:
			Strings = Opt.Dives
		if Opt.Knowledge:
			Opt.Concept = getconceptfromKB(Opt.Knowledge)[1]
		if Opt.ListImages or Opt.GetImages:
			getimages(listOnly=Opt.ListImages,minz=Opt.mindepth,maxz=Opt.maxdepth,getConcept=Opt.Concept,dive=Opt.Dives,
			     png=Opt.GetPNG,both=Opt.GetBoth,Before=Opt.Before,After=Opt.After,flyer=Opt.Flyer,
				 BBString=BBString,Stime=Opt.Stime,Etime=Opt.Etime)	
		elif (Opt.Concept):
			print >> sys.stderr, "## Getting Concepts by Dives"
			if Opt.Kml:
				processKML(getconceptbydive(DiveList=Strings, PIList=Opt.Pis, Sample=Opt.Concept, 
				     PreList=False, Debug=Opt.DEBUG,Before=Opt.Before,After=Opt.After,BBString=BBString,minz=Opt.mindepth,maxz=Opt.maxdepth),IsConcept=1)
			else:
				if Opt.video or Opt.videofile or Opt.timecode:
					Results = getconceptbydive(DiveList=Strings, PIList=Opt.Pis, Sample=Opt.Concept, 
				     PreList=False, Debug=Opt.DEBUG,Before=Opt.Before,After=Opt.After,
					 BBString=BBString,Stime=Opt.Stime,Etime=Opt.Etime,minz=Opt.mindepth,maxz=Opt.maxdepth)
					getvideofromresults(Results)
				else:
					print(getconceptbydive(DiveList=Strings, PIList=Opt.Pis, Sample=Opt.Concept, 
				     PreList=False, Debug=Opt.DEBUG,Before=Opt.Before,After=Opt.After,
					 BBString=BBString,Stime=Opt.Stime,Etime=Opt.Etime,minz=Opt.mindepth,maxz=Opt.maxdepth))
					
			CarryOn=False
		else:
			if Opt.Kml:
				processKML(getdivesummary(Strings,UseShip = Opt.useship).rstrip("\n"),IsConcept=0,simple=True)
			else:
				print getdivesummary(Strings,UseShip = Opt.useship).rstrip("\n")
		if Opt.DEBUG:
			print >> sys.stderr, "D Strings:",Strings
			print >> sys.stderr, "Opt.Args:",Opt.Args
		CarryOn = False
	elif Opt.Knowledge and (Opt.After != "12/31/1980" or Opt.Before != "12/31/2500"):
		Opt.Concept = getconceptfromKB(Opt.Knowledge)[1].replace("'","")
		CarryOn=True
	elif CarryOn and Opt.Knowledge:
		style=int(Opt.Format)
		if Opt.Args:
			Strings = " ".join([Opt.Knowledge,Opt.Args])
		else:
			Strings = Opt.Knowledge
		if Opt.DEBUG:
			print >> sys.stderr, "K Strings:",Strings
		print( getconceptfromKB(Strings, style=style)[1])
		CarryOn=False
		
	if Opt.Predator:
		Opt.Association=Opt.Predator
	
	if CarryOn and (Opt.Concept or Opt.Wildcard):
		if Opt.Wildcard:
			sys.stderr.write("** Wildcards turned ON. Searching inclusively.\n")
			if not Opt.Concept:
				Opt.Concept = Opt.Args[0]
		else:
			sys.stderr.write("** Wildcards turned OFF. Finding concepts strictly. Use %% to override.\n")

		if len(Opt.Args) > 1:
			sys.stderr.write("** NOTE: Concepts should be comma-separated or surrounded in quotes.\n Space-separated lists will not work as expected\n")
		
		if Opt.Kml:
			processKML(findconcept(conceptstrings=Opt.Concept,wild=Opt.Wildcard,hurry=Opt.Hurry,Before=Opt.Before,After=Opt.After,BBString=BBString),IsConcept=1,simple=Opt.Simple)
		else:
			if Opt.video:
				print(findconceptvideo(conceptstrings=Opt.Concept,wild=Opt.Wildcard,hurry=Opt.Hurry,Before=Opt.Before,After=Opt.After,BBString=BBString))
			else:
				print(findconcept(conceptstrings=Opt.Concept,wild=Opt.Wildcard,hurry=Opt.Hurry,Before=Opt.Before,After=Opt.After,BBString=BBString))
		
	elif Opt.Association:
		style = Opt.Format
		assocnum, assocstr = findassociation(Opt.Association,Opt.DEBUG,Opt.Predator) 
		if Opt.Predator:
			PredString = '(Predation only, not vice versa)'
		else:
			PredString = '(and vice versa)'
		sys.stderr.write("\n## Found %d annotations for %s %s...\n" % (assocnum,Opt.Association,PredString ))
		if Opt.GetImages:				 
			getimages(listOnly=Opt.ListImages,minz=Opt.mindepth,maxz=Opt.maxdepth,png=Opt.GetPNG,both=Opt.GetBoth,SampleString=assocstr,Assoc=True,
			 flyer=Opt.Flyer,BBString=BBString,Stime=Opt.Stime,Etime=Opt.Etime)
		elif style:
			print assocstr
		else:
			summarizeassoc(assocstr)
        if Opt.Kml and Opt.Hurry:
            sys.stderr.write("\n## WARNING: --hurry is not compatible with --kml  \n")




if __name__ == "__main__":
	try:
		import argparse
	except ImportError:
		sys.exit("Argparse (standard in python 2.7+) is required.\n Consider updating python to 2.7?")
	import sys
	import subprocess
	import os
	import time
	global USEODBC
	USEODBC=False

	try:
		import pymssql
	except ImportError:
		try:
			import pyodbc
			USEODBC=True
		except:
			print >> sys.stderr,"ODBC Failed"
			sys.exit(InstallString)

		
	Opt = get_options()
	global DELIMITER
	DELIMITER = ["\t",","][Opt.comma]

	_,Onsite = getmyip2(Opt.DEBUG)


	if not Onsite:
		sys.stderr.write("# Offsite detected. Limited options\n")
	
	global DEBUG
	DEBUG=Opt.DEBUG
	
	if Opt.DEBUG:
		print >> sys.stderr, "OPTIONS: ",Opt
	
	main()


"""
DiveSummary in EXPD has the following fields:
COLUMN_NAME,avgrovlat,avgrovlon,avgshiplat,avgshiplon,chiefscientist,
ctdconfigid,ctdlastupdate,ctdlastwho,ctdlastwhy,ctdo2optodecount,ctdo2sbecount,
ctdpcount,ctdplotsent,ctdscount,ctdtcount,ctdxmisscount,diveenddtg,diveid,
divenumber,divestartdtg,expdid,id,maxpressure,maxrovlat,maxrovlon,maxshiplat,
maxshiplon,minrovlat,minrovlon,minshiplat,minshiplon,rovid,rovname,
shipid,shipname
"""


