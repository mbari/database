#! /usr/bin/env python
#
# v3.61- with trimming, can leave one parameter unspecified to go from start or to end
# v3.6 - changed indexing in -t trim option to match position, so -t 1,2 is first two
# v3.5 - n90 should have been n10 2015-07-22
#               also turned on -p print_seq option when trimming  2015-11-22
# v3.4 - added break for -d option for long sequence files 
#               added option for output format and changed -f to -i 2014-06-09
# v3.3 - added cut option to cut into equal size pieces 2014-04-24
# v3.2 - functionalized n50 and histogram 2014-04-17
# v3.1 - corrected usage info and histogram bug 2014-03-13
# v3.0 - created more general histogram using numpy 2014-02-27
# v2.4 - added option to print first N sequences 2013-11-14
# v2.3 - changed output of N50 to float 2013-04-04
# v2.2 - changed repeat to allow for multi-character repeats 2013-04-03
# v2.1 - corrected error from GC content of unequal length sequences 19/oct/2012
#               changed GC counter to use set(), to avoid repeated letters
# v2.0 - added feature to generate histogram from 0 to 100 17/jul/2012
# v1.9 - changed return options 26/jun/2012
# v1.8 - added trimming function, to only output first N letters 26/may/2012
# v1.7 - moved all parameters to 'outvalue' to enable cutting of all outputs 11/may/2012
# v1.6 - added option to count longest polyN 9/may/2012
# v1.5 - minor cleanups of format 23/apr/2012
# v1.4 - added gc counter 11/jan/2012
# v1.3 - added n50 calculator with -n, and only-count option 9/jan/2012
# v1.2 - converted inputs as argparse, rather than sys.argv 31/dec/2011
# v1.1 - added options of size cutoff and print sequence output
#               also uses Bio fasta parser, rather than previous
#               dictionary-based approach 13/dec/2011
# v1.0 - first version
#
# email wrf at lrz dot uni-muenchen dot de with questions or comments

"""SIZECUTTER.PY v3.6 2015-11-22
Requires Bio library (biopython) and numpy for histograms

A simple program to output the length of fasta sequences to stdout
or can be used to filter by size, repeat content, base content, and print
those sequences to stdout.

It can be integrated into pipelines for both input and output
cat file1.fasta file2.fasta | sizecutter.py - > combined_sizes.txt

For example commands:
sizecutter.py -X -

sizecutter.py -t 4, myseq.fasta   # cut from the 4th to the end.
"""

import sys
import os.path
from Bio import SeqIO
from Bio.Seq import Seq
import argparse
import time

try:
	from numpy import histogram
	from numpy import arange
except ImportError:
	print >> sys.stderr, "## Numpy not found. Histograms unavailable"
	
def make_poly_regex(repeat):
	import re
	# generates repolya as "(A)+"
	regex = re.compile("({0})+".format(repeat) )
	return regex

def get_longest_repeat(sequence, regexp):
	# this will allow for multi-character repeats, but takes 2x as long (v2.2)
	polyresults = regexp.finditer(str(sequence))
	# for multicharacter repeats, length with be number of repeats * length, so 'ACACAC' would return 6 rather than 3
	try:
		longestrepeat = max([l.end()-l.start() for l in polyresults])
		return longestrepeat
	# for cases where repeat does not occur once, and max of list fails
	except ValueError:
		return 0

def get_gc_content(sequence, letters):
	# count each letter in the set of letters, and sum them
	gcsum = sum([str(sequence).count(i) for i in set(letters)])
	return gcsum

def get_by_length(value, seqlength):
	by_length = 100.0 * value/float(seqlength)
	return by_length

def trim_seq_ends(seq_record,trimstring):
	# trim some sequence to arbitrary positions as specified in argv
	a,b = trimstring.split(",")
	if not b:
		b=len(seq_record)
	if not a:
		a=1
	return seq_record.seq[int(a)-1:int(b)]

def chop_in_blocks(seq_record,blocksize,wayout,formattype):
	# break up a fasta sequence into fragments the size of blocksize
	seqlength = len(seq_record.seq)
	seqseq = str(seq_record.seq)
	seqname = str(seq_record.id)
	seq_record.description = ""
	for block in xrange(0,seqlength,blocksize):
		outseq = seqseq[block:block+blocksize]
		seq_record.seq = Seq(outseq)
		# rename the sequence by the blocksize, or the seqlength for the last block
		if block+blocksize > seqlength:
			seq_record.id = seqname+"_{}:{}".format(block,seqlength)
		else:
			seq_record.id = seqname+"_{}:{}".format(block,block+blocksize)
		wayout.write(seq_record.format(formattype))
	#THIS IS A FUNCTION OF NO RETURN

def calc_n50(seqsizes, seqsum, writtenseqs):
	if seqsum:
		n50, n10, ncountsum = 0,0,0
		print >> sys.stderr,"# Sorting sequences: ", time.asctime()
		seqsizes.sort()
		halfsum = seqsum//2 ### why are these ints and not floats?
		ninesum = int(seqsum*.9)
		seqmean = float(seqsum)/writtenseqs
		seqmedian = seqsizes[(writtenseqs//2)]
		seqmin, seqmax = min(seqsizes), max(seqsizes)
		print >> sys.stderr,"# Average length is: %.2f" % (seqmean)
		print >> sys.stderr,"# Median length is:", seqmedian
		print >> sys.stderr,"# Shortest sequence is:", seqmin
		print >> sys.stderr,"# Longest sequence is:", seqmax
		for i in seqsizes:
			# changed to output float (%.2f, not %d) for v2.3
			ncountsum += i
			if ncountsum >= halfsum and not n50:
				n50 = i
				print >> sys.stderr,"# n50 length is: %d and n50 is: %.2f" % (ncountsum, n50)
			if ncountsum >= ninesum and not n10:
				n10 = i
				print >> sys.stderr,"# n10 length is: %d and n10 is: %.2f" % (ncountsum, n10)
		return seqmean, seqmedian, n50, n10
	else:
		print >> sys.stderr,"# All lengths and parameters are: 0"
		return 0,0,0,0

def make_histogram(wayout, minval, maxval, bin_size, seqsizes, ignore_max):
	bins = arange(minval, maxval+bin_size, bin_size)
	# unless using -I, values above the max will be changed to the max and put into the last bin
	if not ignore_max:
		for i,x in enumerate(seqsizes):
			if x >= maxval:
				seqsizes[i] = maxval
	# histogram function generates a tuple of arrays, which must be zipped to iterate
	sizehist = histogram(seqsizes,bins,(minval, maxval+bin_size))
	for x,y in zip(sizehist[0],sizehist[1]):
		print >> wayout, "%d\t%d" % (x,y)

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")
	# this handles the system arguments
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input_file', type = argparse.FileType('rU'), default = '-', help="fasta format file")
	parser.add_argument('-X', '--examples', action='store_true', help="display examples for usage and then exit")
	parser.add_argument('-a', '--above', type=float, metavar='N', default=0.0, help="only count sequences longer than N")
	parser.add_argument('-b', '--below', type=float, metavar='N', default=1000000000.0, help="only count sequences shorter than N")
	parser.add_argument('-c', '--cut', type=int, metavar='N', help="cut sequences into chunks of N bases")
	parser.add_argument('-d', '--head', type=int, metavar='N', help="only count the first N sequences")
	parser.add_argument('-e', '--exclude-gaps', action='store_true', help="exclude alignment gaps from length count")
	parser.add_argument('-f', '--print-fasta', action='store_true', help="print fasta header with length")
	parser.add_argument('-g', '--gc-content', metavar='GC', help="instead of size, print the count of N or N...")
	parser.add_argument('-i', '--input-format', nargs='?', const='fastq', default='fasta', help="import fastq format sequences")
	parser.add_argument('-l', '--by-length', action='store_true', help="for -g or -r, output percent of length, rather than count; this option will affect most calculations")
	parser.add_argument('-n', '--n50', action='store_true', help="calculate the mean, median, n50 and n10 for the set")
	parser.add_argument('-o', '--output-format', nargs='?', const='fastq', default='fasta', help="print fastq format sequences")
	parser.add_argument('-p', '--print-seq', action='store_true', help="print sequences instead of sizes, such as when using cutoffs")
	parser.add_argument('-q', '--quiet', action='store_true', help="only count, do not print output")
	parser.add_argument('-r', '--repeats', metavar='A', help="instead of size, print the length of the longest polyN, NX repeat, etc.")
	parser.add_argument('-t', '--trim', metavar='N,N', help="trim sequences from N to N, as 10,20")
	parser.add_argument('-H', '--histogram', action='store_true', help="generate histogram of output, must use -q")
	parser.add_argument('-z', '--bin-size', type=float, metavar='N', default=1.0, help="bin size for histogram output [1.0]")
	parser.add_argument('-m', '--minimum', type=float, metavar='N', default=0.0, help="minimum cutoff for histogram [0.0]")
	parser.add_argument('-M', '--maximum', type=float, metavar='N', default=100.0, help="maximum cutoff for histogram [100.0]")
	parser.add_argument('-I', '--ignore_max', action='store_true', help="ignore values in histogram above maximum")
	args = parser.parse_args(argv)

	if args.examples:
		print >> sys.stderr, examples
		sys.exit()

	# all integers initialized
	seqcount = 0
	rawsum = 0
	# additional parameters
	formattype = args.input_format
	# this may need to be "fastq-sanger" or "fastq-illumina"

	inputfilename = args.input_file

	# alert user of non-unique letters in gc-content string
	getgc = args.gc_content
	if getgc:
		if len(getgc) != len(set(getgc)):
			print >> sys.stderr, "# WARNING: Redundant letters in list", time.asctime()
			getgc = "".join(set(getgc))
			print >> sys.stderr, "# Making non-redundant list for: %s" % (getgc), time.asctime()
	# form regular expression
	getpolya = args.repeats
	if getpolya:
		repoly = make_poly_regex(getpolya)
		print >> sys.stderr, "# Finding repeats of %s:" % (getpolya), time.asctime()
	# list for tracking total number of written bases
	seqsizes = []

	# start main loop to iterate through sequence file
	print >> sys.stderr, "# Parsing sequences on %s:" % (inputfilename.name), time.asctime()
	for seq_record in SeqIO.parse(inputfilename, args.input_format):
		# count of current number of sequences, and if head count is specified then only process N sequences
		if not (args.head) or (args.head and seqcount < args.head):
			seqcount += 1
		# for '-e' option
			if args.exclude_gaps:
				seqlength = len(str(seq_record.seq).replace('-',''))
			else:
				seqlength = len(seq_record.seq)
			# track total number of read bases
			rawsum += seqlength
		# for '-r' mode
			# uses regular expression for finding longest repeat out of list of all repeats
			if getpolya:
				outvalue = get_longest_repeat(seq_record.seq, repoly)
		# for '-g' mode
			# for each unique letter in string of args.gc_content, count it, and sum all of the results
			elif getgc:
				outvalue = get_gc_content(seq_record.seq, args.gc_content)
			else:
				outvalue = seqlength
		# for '-l' option
			# if using by-length parameter, get percentage by length
			# this is meant for viewing individual transcripts
			# summary will not be accurate unless all sequences are the same length
			if args.by_length:
				outvalue = get_by_length(outvalue, seqlength)
		# for '-a' and '-b'
			# if sizes are within high and low cutoffs
			if (args.below > outvalue >= args.above):
			# for '-t' option
				if args.trim:
					args.print_seq = True
					seq_record.seq = trim_seq_ends(seq_record, args.trim)
					# update written value with trimmed sequence length
					outvalue = len(seq_record.seq)
				# make list of all sequences that match above criteria
				seqsizes.append(outvalue)
			# for '-q' option
				# if this is changed, then do all calculations up to this point but print nothing
				if not args.quiet:
					if not args.print_seq:
						if args.print_fasta:
							print >> wayout, seq_record.id, outvalue
						else:
							print >> wayout, outvalue
					else:
					# for '-c' option
						if args.cut:
							chop_in_blocks(seq_record,args.cut,wayout,args.output_format)
						# for '-p' option
						else:
							wayout.write(seq_record.format(args.output_format))
		# must have break, otherwise it will still try to parse through very large files when using -d option
		else:
			break
	# determine number of sequences within cutoff parameters, and output
	# this is the only part that is printed with option '-s'
	writecount = len(seqsizes)
	seqsum = sum(seqsizes)
	try:
		sumratio = float(seqsum)/rawsum
	# for cases where the file is empty
	except ZeroDivisionError:
		sumratio = 0
	print >> sys.stderr, "# Counted %d sequences: " % (seqcount), time.asctime()
	print >> sys.stderr, "# Wrote %d sequences: " % (writecount), time.asctime()
	print >> sys.stderr,"# Total input letters is:", rawsum
	print >> sys.stderr,"# Total output count is:", seqsum
	print >> sys.stderr,"# Ratio of out/in is:", sumratio

	# for '-n' option
		# if finding the n50 and other stats, and if seqsizes list is not empty
	if args.n50 and writecount:
		seqmean, seqmedian, n50, n10 = calc_n50(seqsizes, seqsum, writecount)

	# for '-H' option
		# if generating frequency histogram, and if seqsizes list is not empty
		# histogram is printed to stdout, so it can be written directly to file
		# -q option should be used
	if args.histogram and writecount:
		make_histogram(wayout, args.minimum, args.maximum, args.bin_size, seqsizes, args.ignore_max)

	print >> sys.stderr, "# Done: ", time.asctime()

	# for integration with other programs, return these stats
	if args.n50 and writecount:
		return seqcount, seqsum, seqmean, seqmedian, n50, n10
	else:
		return seqcount

examples="""  For simple length output for each sequence:
sizecutter.py sequences.fasta

  To only display summary statistics, and not write out lengths:
sizecutter.py -q sequences.fasta

  For sequences longer or shorter, use -a or -b, respectively:
sizecutter.py -a 1000 sequences.fasta > longsizes.txt
sizecutter.py -b 1000 sequences.fasta > shortsizes.txt

  To print out the fasta format sequences in use with the size filter:
sizecutter.py -a 1000 -p sequences.fasta > longsequences.fasta

  To cut out a specific piece of a sequence, like from a genomic scaffold:
sizecutter.py -p -t 1500,3000 scaffold_01.fasta > scaffold_01_1500_3000.fasta

  To cut a large sequence to many sequences of 1000bp:
sizecutter.py -p -c 1000 long_sequence.fasta > split_sequences.fasta

  To additionally calculate the n50 and other values:
sizecutter.py -n sequences.fasta

  Use fastq sequences with:
sizecutter.py -i short_reads.fastq

  Convert from fastq to fasta:
sizecutter.py -i -o -p reads.fastq > reads.fasta

  To print GC content for each sequence:
sizecutter.py -g GC -l sequences.fasta

  To only print sequences with greater than 50% GC:
sizecutter.py -g GC -l -a 50 -p sequences.fasta

  To print the length of the longest AT repeat in each sequence:
sizecutter.py -r AT sequences.fasta

  To make a histogram of sizes, ranging from 0 to 10000:
sizecutter.py -q -H -m 0 -M 10000 -z 500 sequences.fasta

  To output the GC content in histogram form:
sizecutter.py -q -l -g GC -H -m 0 -M 100 sequences.fasta
"""

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
