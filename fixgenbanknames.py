#! /usr/bin/env python
"""
{0}

edit genbank style names to be genus_species|sample__gene

Sample is provided as a second argument after the file name...

To save to a file, put this after the command name:
	> outputname.txt 

<REQUIRES mybio.py from http://www.mbari.org/~haddock/scripts/>


version 1.0 - first version 

Usage: 
	{0} filename.fasta myseqs.fasta "|Genbank"
	
"""
import sys
import mybio as mb

blockwidth=71 # optional argument for mb.blockprint
slicecount=0
removing=True
StyleNum=0


def replacenames(FullName,Extra="",Style=0):
	import re
	# >gi|4099886|gb|U91492.1|LPU91492 Limulus polyphemus 28S ribosomal RNA gene, partial sequence
	# >gi|300828338|gb|HM244865.1| Limulus polyphemus isolate REDT 10F cytochrome b (cytb) gene, partial cds; mitochondrial
	
	RepList = [
		[r'A.nidulans' , 'Emericella nidulans'],
		[r'cytochrome oxidase subunit I' , 'COI'],
		[r'cytochrome c oxidase subunit I' , 'COI'],
		[r'mitochondrion chromosome 2', '16S'],
		[r'\(COI\)' , 'COI'],
		[r'\(cox1\)' , 'COI'],
		[r'\(?cytb\)?' , 'cytb'],
		[r'\(CYB\)'	 , 'cytb'],
		[r'cytochrome b', 'cytb'],
		[r'histone', 'H3'],
		[r'[ _]*consensus_sequence',''],
		[r' (reversed).*',''],
		[r' Concatenation.*',''],
		[r'[ _]*consensus.*','']
		]
	
	GeneList = "(18S|18s|28s|28S|cytb|16S|16s|H3|COI)"
	
	ReSearch = r'gi\|\d+\|(gb|dbj|emb)\|[\w\.]+\|\w* (\w+) ([\w\.]+) .*{0},? .*'.format(GeneList)
#	print >> sys.stderr, ReSearch
	for Find,Replace in RepList:
		FullName = re.sub(Find,Replace,FullName)
	#		print >> sys.stderr, FullName

		# if re.search(ReArrange,FullName):
	if Style == 0:
		ReArrange = r'\2_\3{0}___\4'.format(Extra)
		# 	print >> sys.stderr, FullName, "Found"
		Outputname = re.sub(ReSearch,ReArrange,FullName)
	if Style == 1:
		ReMove = r'(.*)[ _]*{0}.*'.format(GeneList)
		# print >> sys.stderr, ReMove
		ReArrange = r'\1'
		Outputname = re.sub(ReMove,ReArrange,FullName)
		# print >> sys.stderr, Outputname
		Outputname += "___" + Extra
		
	return Outputname
	
	
if len(sys.argv)<2:
	print __doc__.format(sys.argv[0].split("/")[-1])	

else:
	# second parameter for printing all seqs
	inputfilename = sys.argv[1]
	ExtraText = ""
	if len(sys.argv)>2:
		ExtraText = sys.argv[2]	
	if len(sys.argv)>3:
		StyleNum = int(sys.argv[3])
		
		
	# This function is in the mybio module
	names,sequences = mb.readfasta_list(inputfilename)

	for name,sequence in zip(names,sequences):
			# These two lines print the sequence
#			print >> sys.stderr, name
			print ">" + replacenames(name,Extra=ExtraText,Style=StyleNum) 
			# can add a blockwidth parameter, but defaults to 71
			mb.printblock(sequence)
	print >> sys.stderr, "Reformatted {0} sequences in style # {2}'>{1}'".format(len(names),replacenames(name,Extra=ExtraText,Style=StyleNum),StyleNum)
