#! /usr/bin/env python
"""Concatenate sequences of equal length from many files,
based on seq names.
FILES SHOULD BE -- padded to same length
TODO:
Preserve file names in mask or something?

Version 1.2 - added skip taxa option
Version 1.1 - added argparse and debug options
Version 1.0 - first version 

optional arguments:
  -h, --help       Show this help message and exit
  -d, --debug      Print debug info
  -k, --skipone    Skip taxa with one gene
  -t, --trimname   Use shortname with genes and gb removed from end
  -p, --printtable Print gene occupancy table

Usage: 
    seqcat.py *.fta > mergedfile.fasta 2> partitiontable.txt
"""
	
import mybio as mb
import sys
import re
import argparse

def get_options():
	parser = argparse.ArgumentParser(usage = __doc__)
	parser.add_argument("-b", "-d", "--debug",    action="store_true", dest="DEBUG", default=False, help="Print debug info")
	parser.add_argument("-k", "--skipone",    action="store_true", dest="Skipone", default=False, help="Skip taxa with one gene")
	parser.add_argument("-t", "--trimname",    action="store_true", dest="trimname", default=False, help="Trim _ gene and -gb from names")
	
	parser.add_argument("-p", "--printtable",    action="store_true", dest="Printtable", default=False, help="Print a gene occupancy matrix")
	parser.add_argument("FileList", nargs='+')
	# options = parser.parse_args()
	options = parser.parse_args()
	return options


def printmatrix(MasterDictionary,GeneList,NameList):
	sys.stderr.write("Species\t{}\n".format("\t".join(GeneList)) )
	MasterKeys = MasterDictionary.keys()
	for Name in NameList:
		sys.stderr.write(Name)
		for Gene in GeneList:
			sys.stderr.write("\t{}".format(["0","1"][(Name,Gene) in MasterKeys]))
		sys.stderr.write("\n")

	
	
if len(sys.argv)<2:
	sys.stderr.write(__doc__)
else: 
	Options = get_options()
	DEBUG=Options.DEBUG
	FileList = Options.FileList
	MasterDict = {}
	LengthDict = {}
	MasterNameCount={}
	MasterGeneCount={}
	NameList = []
	SkipList = ["Urchin"]
	KOGskip = []
	GeneList = []
	LengthList = []
	""" First, build up a master list of unique names, and show their counts and lengths
	Then loop through and create a dictionary either appending the sample or n*'-'
	"""
	# for genbank names use this:
	rs = r'\1|GB'
	ss = r'([\w\.]+)\|.*\-(gb|GB).*'
	#TODO: add checking of length
	for FileName in FileList:
		# Get the gene from the last _Name.fta
		# Option, get it from the first sequence with __
		# This works for 18S and for KOGFiles!, etc...
		# Example KogName: aln_prune_KOG4655.faa"
		# Genename = FileName.split(".")[0].split("_")[-1]
		Genename = FileName.split(".")[0].split("_")[-1]
		GeneList.append(Genename)
		Names,Seqs = mb.readfasta_list(FileName)
		Seqs = [S.replace(" ","") for S in Seqs]
		SSize = len(Seqs[0])
		LengthDict[Genename] = SSize
		LengthList.append(SSize)
		
		for Name,Sequence in zip(Names,Seqs):
			if Name.count("_")>1 and Options.trimname:
				ShortName = "_".join(Name.split("_")[:-1])
			else:
				ShortName = Name
				if DEBUG:
					print >> sys.stderr, "## No gene name in {} - File: {}".format(Name,FileName)
				
			if Options.trimname and (('-gb' in ShortName) or ('-GB' in ShortName)):
				ShortName = re.sub(ss,rs,ShortName)
			MasterNameCount[ShortName] = MasterNameCount.get(ShortName,0) + 1
			MasterGeneCount[Genename] = MasterGeneCount.get(Genename,0) + 1
			
			if len(Sequence) != LengthDict[Genename]:
				print >> sys.stderr, "## Gene from {} is length {} not {}".format(Name,\
				                                     len(Sequence),LengthDict[Genename])
			MasterDict[(ShortName,Genename)] = Sequence
			NameList.append(ShortName)
		
	print >> sys.stderr, "GENELIST",GeneList
	print >> sys.stderr, "GeneCount",MasterGeneCount
	
	
	UniqueNames = sorted(set(NameList))
	# print  >> sys.stderr, "MK", Mk
	for k in UniqueNames:
		if k not in SkipList:
			Outstr=''
			for gene in sorted(GeneList):
				ExtraStr = '-' * LengthDict.get(gene)
				Outstr += MasterDict.get((k,gene), ExtraStr)
				# print "{}\t{}\t{}".format(MasterNameCount[k],k,MasterGeneCount[k])
				# print "{}\t{}".format(k,gene)
			
			
			# Skip singletons
			if not (Options.Skipone and MasterNameCount[k] < 2) : 
				if MasterNameCount[k] <2:
					k = "#"+k
				print ">{}".format(k)
				print mb.stringblock(Outstr,85).rstrip('\n')
	
	
	
	if DEBUG:
		for k in UniqueNames:
			DebugOut = ''
			if not (Options.Skipone and MasterNameCount[k] < 2) : 
				for gene in sorted(GeneList):
					# print "\t{}".format(len(MasterDict.get((k,gene),'')))
					DebugOut += "{:4d}\t".format(len(MasterDict.get((k,gene),'').replace("-","")))
				if MasterNameCount[k] <2:
					k = "*"+k
				DebugOut += k
				print >> sys.stderr, DebugOut
	# Output length table
	
	TL=0
	for n in GeneList:
		# for DNA use DNA
		model='CAT'
		if n[0:2] in ('16','18','28'):
			model = 'DNA'
		print >> sys.stderr, "%s, %s = %d-%d" % (model, n, TL+1,TL+LengthDict[n])
		TL+=LengthDict[n]
	if Options.Printtable:
		printmatrix(MasterDict,GeneList,UniqueNames)
	# for Name in Ordernames:
	# 	if Name not in SkipList:
	# 		print ">%s\n%s" % (Name,mb.stringblock(MasterDict[Name],80).rstrip())
	# position = 1
	# for KOG in MasterDict:
	# 	sys.stderr.write("%s\t%d\t%d\n" %(KOG, position, position+LengthDict[KOG]-1) )
	# 	position += LengthDict[KOG]
#	sys.stderr.write("Total Length:\t%d\n" % len(MasterDict[Name]) )
