###MBARI PUBLIC SOFTWARE REPO - DATABASE FOLDER

>All software is provided with the provision that the user will assume risks of its operation. Bugs exist! Use with care and use with files that have been backed up.
>Contact Steve Haddock at MBARI with questions about most of these scripts

# *File descriptions:

* **crossref_parse.py**
 	- Using a flat list of refs, try searching for crossref biblio records.
* **exif_and_xattr_set.py**
 	- Using the parent folders, set searchable EXIF and xattr tags for files.
* **flickr_search2.py**
	- search flickr for images with certain tags and in certain bounding boxes
* **flickrapi2.py**
    - required for `flickr_search2`
* **genbank_accession.py**
    - Get GenBank record for each accession number in a list
* **lucidconvert.py**
 	- Convert Lucid Builder (taxonomy program) XML files to NEXUS (Mesquite) format
* **vars_image_move.py** 
	- takes images retrieved in bulk by VARS query and reorganizes them into folders
* **vars_retrieve.py**
 	- search MBARI database for dive summaries, samples, and annotations
* **photoscale.py**
 	- calculate field of view of an image based on EXIF metadata stored by Nikon cameras
* **videotable.py**
 	- generate a text or HTML table summarizing the properties of video files

# *Notes on installation

To use `vars_retrieve.py` under OSX, follow instructions in the help section of that file. SHort version below:

 * For Yosemite, go to command line and type `gcc`.
 * For older OSX versions, Get the **Command Line Tools** from https://developer.apple.com/downloads/ 
 * Install HomeBrew by pasting this command in a Terminal window: 
 *     `ruby -e "$(curl -fsSL https://raw.github.com/mxcl/homebrew/go)"`
 * Install pip with this exact command:
 *     `sudo easy_install pip`
 * `brew install unixodbc`
 * `brew install --with-unixodbc freetds`
 * `sudo pip install pyodbc` 

 # Docker

 You can run a docker container without installing anything other than [Docker](http://www.docker.com). At a terminal run:

 `docker run -it -v $HOME/Downloads:/data mbari/py-database`

