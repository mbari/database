#!/usr/bin/env python
"""
Convert tab-delimited decimat lat/lon with header to kml

Usage:
   latlon2kml.py mylatlon.txt
   
Can get proper file using vars_unique.py myfile | cut -f 3,5,6 > mylatlon.txt

That includes depth info. 

To include field names also, use cut -f 3,4,5,6

# NOTE: by default, skips one header line...

"""

import sys

if len(sys.argv) < 2:
	print >> sys.stderr, __doc__
else:
	AlreadySkipped = False
	inFile = open(sys.argv[1], 'rU')
	print '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<kml xmlns=\"http://earth.google.com/kml/2.2\">\n<Document>'
	placestring = '''<Placemark>\n  <name>{3}</name>
    <Point>\n <coordinates>{0},{1},-{2}</coordinates>
	</Point>\n</Placemark>'''


	for line in inFile:
		if AlreadySkipped:
			Fields = line.rstrip("\n").split("\t")
			Label = "Waypoint"
			if len(Fields)>3:
				Elevation,Label,lat,lon = Fields
			elif len(Fields)>2:
				Elevation,lat,lon = Fields
			else:
				lat,lon = Fields
				Elevation = '0'
			print placestring.format(lon,lat,Elevation,Label)
		else:
			AlreadySkipped = True
			
	print'</Document>\n</kml>'
