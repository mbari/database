#! /usr/bin/env python
# -*- coding: utf-8 -*-
""" Calculate size in cm of long width of photo from EXIF data 
"""
import math as m
import sys
import re
import os
	
def	getexifdata(filename):
	""" 
'DOF: 0.013 m (0.231 - 0.244)\n', 
'FOV: 25.3 deg (0.11 m)\n', 
'FocusDistance: 0.24 m\n'
"""

	SearchStr = r"(\w+): ([\d\.]+) ?.*"
	FOVSearch = r"(\w+): ([\d\.]+) .*\(([\d\.]+)"

	# Regex1 = re.compile(SearchStr)
	# Regex2 = re.compile(FOVSearch)
	#
	exifcommand = 'exiftool -v0 -S -v0 -DOF -FOV -FocusDistance -FocalLengthIn35mmFormat -ImageWidth -ImageHeight "{}" '.format(filename)

	expipe = os.popen(exifcommand)
	exdata = expipe.readlines()
	# print "EXDATA,",exdata
	if len(exdata) > 4:
		D={}
		D['name'] = filename
		for Li in exdata:
			g = re.search(SearchStr,Li)
			if g:
				# print g.groups()
				try:
					D[g.group(1)] = float(g.group(2))
				except ValueError:
					print >> sys.stderr, "OOPS:",g.groups()
		# to get the camera value from the second line
		D['CamFOV'] = float(re.search(FOVSearch,exdata[1]).group(3))
	
		# print D	
	else:
		D=None
	return D
		
		
# ThIs is the same number as the camera records () in the FOV Exif parameter	
def sizefromdist(FOV,Dist):
	x = 2*Dist * m.tan(m.radians(FOV)/2.0)
	return x


if len(sys.argv) < 2:
	sys.stderr.write(__doc__)
else:
	Extended = False

	# Empirically determined regression is about 1:1 but has an offset of these values (distance to sensor?)
	
	CorrFact = {60:4.1, 90:3.1}
	FileList = sys.argv[1:]
	if Extended:
		print"Name	Dist	Dev	Deg	Lens	Width	FOV_cam	FOV_calc	Corrected"
	else:
		print"Name\tViewSize(cm)"
		
	for FileName in FileList:
		DV = getexifdata(FileName)
		if DV:
			viewsize = sizefromdist(DV['FOV'],DV['FocusDistance']) * 100
			if Extended:
				print "{}	{:.0f}	{:.1f}	{}	{:.0f}	{:.0f}	{}	{:.1f}	{:.1f}".format( 
				      DV['name'],DV['FocusDistance']*100, 100*DV['DOF'], DV['FOV'],
					  DV['FocalLengthIn35mmFormat'],
					  DV['ImageWidth'], DV['CamFOV']*100, viewsize, 
					  viewsize - CorrFact.get(int(DV['FocalLengthIn35mmFormat']),-10000) )
			else:
				print "{}\t{:.1f}".format(DV['name'],viewsize - CorrFact.get(int(DV['FocalLengthIn35mmFormat']),-10000) )
		else:
			print "{}	NA".format(FileName)

