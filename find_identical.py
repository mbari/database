#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import re
import mybio as mb
import jellyfish
import numpy as np
import argparse
"""
Find identical sequences and remove them...

Uses the NumPy and the "Jellyfish" python module (not my name!)
  Install jellyfish with 'sudo easy_install jellyfish'

"""

#TODO: check revcomp also

def identindex(names):
	nl = len(names)
	namearr = np.array(names)
	namediag = np.array([])
	for na in names:
	    namediag=np.append(namediag, na==namearr)
		
	namediag = namediag.reshape(nl,nl)
	# lower triangle, minus 1 makes it zero out the diagonal
	lowervals = np.tril(namediag,-1)
	# print lowervals
	delind = np.unique(np.where(lowervals == 1)[0])	
	return sorted(delind,reverse=True)

def get_options():
	parser = argparse.ArgumentParser(usage = __doc__)
	parser.add_argument('input_file', default = '-', help="fasta format file")
	parser.add_argument("-n", "--name",     action="store_true", dest="UseName",  default=False,help="Check full name")
	parser.add_argument("-q", "--seq",      action="store_true", dest="UseSeq",   default=True,help="Check sequence only")	
	parser.add_argument("-b", "--both",     action="store_true", dest="UseBoth",  default=False,help="Check combined shortname and sequence")	
	parser.add_argument("-s", "--shortname",action="store_true", dest="Shortname",default=False,help="Check shortname")
	parser.add_argument("-d", "--dist",     action="store_true", dest="UseDist", default=False, help="Use distance")
	parser.add_argument("-p", "--print",    action="store_false", dest="YesPrint",default=True, help="Print the sequences at the end")
	options = parser.parse_args()
	return options
	

if len(sys.argv) < 2:
	print >> sys.stderr, __doc__
else:
	Options = get_options()
	UseDist = False
	infile = Options.input_file
	names,seqs = mb.readfasta_list(infile)
	seq2 = [s.upper().replace('-','') for s in seqs]
	both = zip(names,seq2)
	
	# print >> sys.stderr, "BOTH",both
	
	# metric can either be name, shortname, sequence, combined shortname+seq, combined name+seq
	# within each of those (mainly sequence), can use identity or distance
	
	### FIX ME ###
	shortname = []
	for n in names:
		if "|" in n:
			shortname.append(n.split("|")[0])
		elif n.count('_') > 1:
			shortname.append("_".join(n.split('_')[0:2]))
		elif n.count(' ') > 1:
			shortname.append("_".join(n.split()[0:2]))
		else:
			shortname.append(n)
			
	# print >> sys.stderr, sorted(shortname)
	combined = [ '|'.join([x,y.upper().replace('-','').replace(' ','')]) for x,y in zip(shortname,seqs) ]
	# print combined

	### THE KEY PART
	delind = []
	
	if Options.UseBoth and not(Options.UseName or Options.UseSeq or Options.Shortname):
		resultind = identindex(names)
		delind += resultind
		print >> sys.stderr, "## Removing {} combined short names and seqs...".format(len(resultind))

	else:
		if Options.UseName:
			resultind = identindex(names)
			delind += resultind
			print >> sys.stderr, "## Removing {} identical long names...".format(len(resultind))
		if Options.UseSeq:
			resultind = identindex(seq2)
			delind += resultind
			print >> sys.stderr, "## Removing {} identical sequences...".format(len(resultind))
		if Options.Shortname:
			resultind = identindex(shortname)
			delind += resultind
			print >> sys.stderr, "## Removing {} identical shortnames...".format(len(resultind))
		
	delind = sorted(set(delind),reverse=True)
	
	print >> sys.stderr, "delete:", delind
	
	# for n,s in sorted(both):
	# 	print ">"+n
	# # 	print mb.stringblock(s)
	# print len(both)
	
	# NOT YET FINISHED/IMPLEMENTED
	if Options.UseDist:
		TotalList = np.array([])
		cl = len(combined)
		TotalList = np.zeros(shape=(cl,cl))
		TotalList = TotalList - 1
		for first in range(cl-1):
			for second in range(first+1,cl):
				# TotalList[second,first] = jellyfish.levenshtein_distance(combined[first],combined[second])
				TotalList[second,first] = jellyfish.hamming_distance(combined[first],combined[second])
		lowerTotal = np.tril(TotalList,-1)
		print lowerTotal
		DelDist = np.unique(np.where(0 < lowerTotal < 2)[0])	
		print sorted(DelDist,reverse=True)

	for di in delind:
		del both[di]
	
	if Options.YesPrint:
		for n,s in both:
			print ">"+n
		 	print mb.stringblock(s).rstrip()
	else:
		print >> sys.stderr, "NOT PRINTING. (Hope you are not expecting a redirect.)"
	print >> sys.stderr, "Deleted {} sequence(s) from {}:\n".format(len(delind),infile), [names[ni] for ni in delind[::-1] ]
	
	
	
	# for n,s in both:
	# 	print ">"+n
	# 	print mb.stringblock(s)
		
	# dmat = np.array()
	# print dmat
	

'''	
	for n,s in both:
		# first time
		if not(nameone):
			nameone = n.split()[0]
			seqone = s
		else:
			checkname = n.split()[0]
			if checkname == nameone:
				
				# checkseq here
				# reverse complement 
				# jaro_distance, damerau_levenshtein_distance, jaro_winkler, levenshtein_distance, hamming_distance
				diff = jellyfish.levenshtein_distance(seqone.replace('-','').upper(),s.replace('-','').upper())
				# print "levenshtein", diff, s,'=',seqone
				print checkname, diff, s,'=',seqone
				if diff < threshold:
					deletelist.append(n)
			else:
				nameone = checkname
				seqone = s

	print deletelist
	
	'''

"""print jellyfish.damerau_levenshtein_distance(a,b), 'jellyfish.damerau_levenshtein_distance(a,b)'
print jellyfish.hamming_distance(a,b), 'jellyfish.hamming_distance(a,b)'
print jellyfish.jaro_distance(a,b), 'jellyfish.jaro_distance(a,b)'
print jellyfish.jaro_winkler(a,b), 'jellyfish.jaro_winkler(a,b)'
print jellyfish.levenshtein_distance(a,b), 'jellyfish.levenshtein_distance(a,b)'
print jellyfish.match_rating_comparison(a,b), 'jellyfish.match_rating_comparison(a,b)'

"""