#! /usr/bin/env python

"""

Finds all frame-grab files in three vehicle-named subdirectories 
of the current directory, and renames them by adding
Taxon_Vehicle_directory to the front, moving them to the 
<Destination> folder. This folder should be at the same level
as the vehicle-named folders.

Usage: 

* CD to the folder that has the vehicle folders, 
* Create a folder at that same level to serve as the destination

* Run:
     vars_image_move.py TaxonName DestinationName


Version 2.1 - Aug 2013 - added parameters for TaxonName and DestinationName
Version 2.0 - June 2013
"""

import os, sys, glob


if len(sys.argv) < 3:
	print __doc__
else:
	Taxon = sys.argv[1]
	Destination = sys.argv[2].rstrip("/")
# Name of folder in current directory at same level as vehicles
# Destination = 'Periphyllopsis'
# Taxon = "Periphyllopsis"
	DEBUG = False

	Extension="jpg"

	BaseDir = os.getcwd()
	VehList = ["Doc Ricketts", "Tiburon", "Ventana"]

	for Veh in VehList:
		NewPath = os.path.join(BaseDir,Veh)
		if not os.path.exists(NewPath):
			sys.stderr.write("####\n### No folder for vehicle {}\n####\n".format(Veh))
		else:
			os.chdir(NewPath)
			# sys.stderr.write("IN {} DIRECTORY...\n".format(os.getcwd()))
			Vehicle = Veh[0:3]
			MyCwd = os.getcwd().split("/")[-1]
			# e.g. Ventana
			if DEBUG:
				print MyCwd


			ImageDirList = os.popen('ls -F  | grep \/','r').read().split()

			# e.g. images
			if DEBUG:
				print "ImageDir:",ImageDirList


			for SubDir in ImageDirList:
				#e.g. 3602
				if SubDir.startswith("stills"):
					# sys.stderr.write("IN STILLS DIRECTORY...\n")
					DirList = [o.replace("stills/","") for o in glob.glob('stills/*/*') if os.path.isdir(o)]
				else:
					DirList = os.popen('ls -F  %s | grep \/' % SubDir,'r').read().split()
				if DEBUG:
					print "** In SubDir: DirList:",DirList
			
				for Direct in DirList:
					# 	InDirList = os.popen('ls -F %s/%s/*/*| grep \/' % (SubDir, Direct),'r').read().split()
					# else: 
					# 	InDirList = os.popen('ls -F  %s/%s/* | grep \/' % (SubDir, Direct),'r').read().split()
					# if DEBUG:
					# 	print "InDirList:",InDirList
					if SubDir.startswith("stills"):
						Direct = Direct.replace(":","/").replace("//","/")
						# print >> sys.stderr, "SUBDIR: {}".format(SubDir)
						# print >> sys.stderr, "DIRECT",Direct
					
					print "Directory", Direct,'-----------------'
					ListCommand = 'ls ' + SubDir + Direct +'/*.'+Extension
					if DEBUG:
						print >> sys.stderr, "LIST COMMAND: ", ListCommand
					FileList=os.popen(ListCommand).read().split()
					if len(FileList)==0:
						sys.stderr.write( "No files found in " + ListCommand + "\n")
					else:
						if DEBUG:
							print "yes", FileList
						for PathName in FileList:
							PathName = PathName.replace("//","/")
							if SubDir.startswith("stills"):
								FileName= PathName.split('/')[-1]
								Directs = "_".join(PathName.split('/')[1:3] )
								EndName = Directs + '-' + FileName
								if DEBUG:
									print >> sys.stderr, "ListCommand: ", ListCommand
								# EndName = PathName.replace("/","_").replace(":","_")	
							else:
								if DEBUG:
									print "Pathname", PathName
								FileName=PathName.split('/')[-1]
								if (FileName) and FileName[0]==('_'):
									FileName=FileName[1:]
								#print FileName
								EndName = Direct[:-1] + '-' + FileName
							NewName= '../' + Destination + '/' + Taxon+'_' + Vehicle + '_' + EndName
							command = 'cp ' + PathName + " " + NewName
							print command
							if not DEBUG:
								os.popen(command,'r')
						if not DEBUG:
							os.popen('chmod 644 ../' + Destination + '/*.'+ Extension ,'r')
	os.chdir(BaseDir)		

