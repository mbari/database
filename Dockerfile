FROM ubuntu:18.04
LABEL AUTHOR="Brian Schlining <brian@mbari.org"

RUN apt-get update -y \
    && apt-get install -y \
      build-essential \
      curl \
      freetds-bin \
      freetds-dev \
      net-tools \
      python \
      python-dev \
      python-pip \
      tdsodbc \
      unixodbc

ENV APP_DIR /opt/database

COPY . ${APP_DIR}

WORKDIR ${APP_DIR}

RUN pip install --upgrade pip \
    && pip install -r requirements.txt

ENV PATH "${PATH}:."

ENTRYPOINT [ "/bin/bash" ]