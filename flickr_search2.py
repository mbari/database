#!/usr/bin/python
"""
(Based on a search script by Tamara Berg and James Hays)
Generates an HTML table of images, link, location from flickr 
on search terms and within a bounding box. Use -term to exclude
certain terms.

Very preliminary version w/o user interface...
Uses flickrapi2.py which is also available here:
    https://bitbucket.org/beroe/mbari-public/src/master/database/flickrapi2.py?at=master

Usage: 
1. Add your flickr API key information
2. Edit the query string and mybb variables
3. Run and capture output:

  flickr_search2.py > myresults.html

Version 0.9 - Steve Haddock [haddock at mbari dot org]

"""

import sys, time, socket
from flickrapi2 import FlickrAPI
from datetime import datetime

socket.setdefaulttimeout(30)  #30 second time out on sockets before they throw
#an exception.  I've been having trouble with urllib.urlopen hanging in the 
#flickr API.  This will show up as exceptions.IOError.

#the time out needs to be pretty long, it seems, because the flickr servers can be slow
#to respond to our big searches.

############################################################
####  CHANGE THESE VALUES FOR SEARCH TERMS AND BOUNDING BOX
############################################################
#form the query string.

query_string = "jellyfish -aquarium"

# bounding box in lat/lon
# west, south, east, north
mybb = "73, -13, 126, 13" #indonesia
# mybb = "60, 0, 100, 10" #


###########################################################################
# flickr auth information:
# change these to your flickr api keys and secret
# you can request one here:
# http://www.flickr.com/services/apps/create/apply
flickrAPIKey = "INSERTAPIKEYHERE"  # API key
flickrSecret = "INSERTSECRETKEYHERE"                  # shared "secret"


# make a new FlickrAPI instance
fapi = FlickrAPI(flickrAPIKey, flickrSecret)

try:
	rsp = fapi.photos_search(api_key=flickrAPIKey,
							ispublic="1",
							media="photos",
							per_page="250", 
							page="1",
							has_geo = "1", 
							bbox=mybb,
							extras = "tags, original_format, license, geo, date_taken, date_upload, o_dims, views",
							text=query_string
							#accuracy="6", #6 is region level.  most things seem 10 or better.
							#min_upload_date=str(mintime),
							#max_upload_date=str(maxtime))
							##min_taken_date=str(datetime.fromtimestamp(mintime)),
							##max_taken_date=str(datetime.fromtimestamp(maxtime))
							)
	#we want to catch these failures somehow and keep going.
	time.sleep(1)
	fapi.testFailure(rsp)
	total_images = rsp.photos[0]['total']
	# sys.stderr.write("Found {0} images\n".format(total_images)) 

except KeyboardInterrupt:
	sys.stderr.write('Keyboard exception while querying for images, exiting\n')
	raise
except:
	print rsp
	raise
	#print type(inst)     # the exception instance
	#print inst.args      # arguments stored in .args
	#print inst           # __str__ allows args to printed directly
	sys.stderr.write ('Exception encountered while querying for images\n')
	
i = getattr(rsp,'photos',None)
current_image_num = 0

### Print info in tabular format

# do you want to print the table?
printout = False


if printout:
	for b in rsp.photos[0].photo:
		if b!=None:
			outstring =  b['latitude'].encode("ascii","replace") 
			outstring += '\t'+  b['longitude'].encode("ascii","replace") 
			outstring += '\t'+  b['datetaken'].encode("ascii","replace")
			outstring += '\t'+ b['title'].encode("ascii","replace")
			MyURL = 'http://farm3.static.flickr.com/' + b['server'] + '/'+ b['id'] + "_" + b['secret'] + '.jpg'
			outstring += '\t' + MyURL
			print outstring
			current_image_num = current_image_num + 1;
		

#### Print the html format

print '''


<html>
<body>
<code>
<table border="1">
  <tr>
    <th>Lat</th>
    <th>Lon</th>
    <th>Date</th>
    <th>Img</th>
  </tr>
'''
current_image_num = 0;

for b in rsp.photos[0].photo:
	if b!=None:
		MyURL = 'http://farm3.static.flickr.com/' + b['server'] + '/'+ b['id'] + "_" + b['secret'] + '.jpg'
		outline = '\t<tr><td>%s</td><td>%s</td><td>%s</td><td><img src="%s" width = "300"></td></tr>' \
  		%(b['latitude'],b['longitude'],b['datetaken'],MyURL)
		print outline
		current_image_num = current_image_num + 1;
		
print '''</table>
</code>
</body>
</html>'''
sys.stderr.write("Showing {0} out of {1} images...\n".format(str(current_image_num),total_images)) 



